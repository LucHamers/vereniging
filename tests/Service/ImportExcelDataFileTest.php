<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Service;

use App\Entity\AddressType;
use App\Entity\Committee;
use App\Entity\CommitteeFunction;
use App\Entity\CommitteeFunctionMap;
use App\Entity\LogfileEntry;
use App\Entity\MailingList;
use App\Entity\MemberEntry;
use App\Entity\MembershipType;
use App\Entity\PhoneNumber;
use App\Entity\PhoneNumberType;
use App\Entity\Status;
use App\Entity\Title;
use App\Service\ImportExcelDataFile;
use App\Service\LogEventsDisabler;
use App\Service\LogMessageCreator;
use App\Tests\TestCase;
use DateTime;
use Exception;

/**
 * Test service class import excel data file.
 *
 * Class ImportExcelDataFileTest
 */
class ImportExcelDataFileTest extends TestCase
{
    /**
     * Setup the test environment, i.e. load fixture data.
     *
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->setLoggedInUserForLogging();
    }


    /**
     * Test if the template excel file offered on the web page is there and valid.
     */
    public function testExcelTemplateFile(): void
    {
        $testObject = $this->generateTestObject();

        // Check if error in row 1 of the group members heading if found
        try {
            $testObject->checkExcelFileForImport(dirname(__FILE__)."/../../public/import/default_import_file.xlsx");
        } catch (Exception $e) {
            $message = "Test for excel template file failed: ".$e->getMessage();
            $this->fail($message);
        }
        // No exception, as expected. The following assert just removes the warning for a risky test without assertions.
        $this->assertTrue(true);
    }


    /**
     * Test file checks
     */
    public function testImportExcelFileWithFileErrors(): void
    {
        $testObject = $this->generateTestObject();

        $exceptionOccurred = false;
        try {
            $testObject->checkExcelFileForImport('', '');
        } catch (Exception $e) {
            $message  = 'File  not found.';
            $this->assertEquals($message, $e->getMessage());
            $this->assertEquals($message, $testObject->getErrorMessage());
            $exceptionOccurred = true;
        }
        $this->assertTrue($exceptionOccurred, "Expected exception did not occur.");

        // File check: wrong type
        $exceptionOccurred = false;
        try {
            $testObject->checkExcelFileForImport(dirname(__FILE__).'/ImportExcelFiles/empty.txt', 'e.txt');
        } catch (Exception $e) {
            $message  = 'File e.txt is no excel file.';
            $this->assertEquals($message, $e->getMessage());
            $this->assertEquals($message, $testObject->getErrorMessage());
            $exceptionOccurred = true;
        }
        $this->assertTrue($exceptionOccurred, "Expected exception did not occur.");

        // Check setting original name via file check.
        $exceptionOccurred = false;
        try {
            $testObject->checkExcelFileForImport(dirname(__FILE__).'/ImportExcelFiles/empty.txt');
        } catch (Exception $e) {
            $message  = 'File empty.txt is no excel file.';
            $this->assertEquals($message, $e->getMessage());
            $this->assertEquals($message, $testObject->getErrorMessage());
            $exceptionOccurred = true;
        }
        $this->assertTrue($exceptionOccurred, "Expected exception did not occur.");
    }


    /**
     * Test static header checks
     */
    public function testImportExcelFileWithStaticHeaderErrors(): void
    {
        $this->clearAllLogFiles();
        // Check header where in cell A1 there is no name text
        $this->checkFileForError('empty.xlsx', 'Header error: Name not found in cell A1.');
        // Check header where in cell R1 there is no user role text
        $this->checkFileForError('static_header_error.xlsx', 'Header error: User role not found in cell N1.');

        // Check if the correct log messages were written
        $logMessages = $this->getAllLogs();
        $this->assertEquals(2, count($logMessages));
        $this->assertEquals('Import action', $logMessages[1]->getChangeType()->getChangeType());
        $this->assertEquals('Import action', $logMessages[0]->getChangeType()->getChangeType());
        $this->assertEquals('File check for importing file empty.xlsx failed with the following error: Header error: Name not found in cell A1.', $logMessages[1]->getLogentry());
        $this->assertEquals('File check for importing file static_header_error.xlsx failed with the following error: Header error: User role not found in cell N1.', $logMessages[0]->getLogentry());
    }


    /**
     * Test phone number header checks
     */
    public function testImportExcelFileWithDynamicHeaderErrors(): void
    {
        // Check if error in row 1 of the company information heading if found
        $this->checkFileForError('dynamic_header_company_row_1_error.xlsx', 'Header error: Company information not found in cell O1.');
        // Check if error in row 2 of the company information heading is found
        $this->checkFileForError('dynamic_header_company_row_2_error.xlsx', 'Header error: Company description not found in cell Q2.');
        // Check if error in row 1 of the phone number heading if found
        $this->checkFileForError('dynamic_header_phone_number_row_1_error.xlsx', 'Header error: Phone number 2 not found in cell V1.');
        // Check if error in row 2 of the phone number heading is found
        $this->checkFileForError('dynamic_header_phone_number_row_2_error.xlsx', 'Header error: Phone number type not found in cell X2.');
        // Check if error in row 1 of the address heading if found
        $this->checkFileForError('dynamic_header_address_row_1_error.xlsx', 'Header error: Address 1 not found in cell AB1.');
        // Check if error in row 2 of the address heading is found
        $this->checkFileForError('dynamic_header_address_row_2_error.xlsx', 'Header error: Address type not found in cell AI2.');
        // Check if error in row 1 of the committee function heading if found
        $this->checkFileForError('dynamic_header_committee_row_1_error.xlsx', 'Header error: Committee function 2 not found in cell AP1.');
        // Check if error in row 2 of the committee function heading is found
        $this->checkFileForError('dynamic_header_committee_row_2_error.xlsx', 'Header error: Committee not found in cell AV2.');
        // Check if error in row 1 of the group members heading if found
        $this->checkFileForError('dynamic_header_group_row_1_error.xlsx', 'Header error: Group member 1 not found in cell AX1.');
        // Check if error in row 2 of the group members heading is found
        $this->checkFileForError('dynamic_header_group_row_2_error.xlsx', 'Header error: First name not found in cell AY2.');
        // Check if error in row 1 of the log file entry heading is found
        $this->checkFileForError('dynamic_header_logentry_row_1_error.xlsx', 'Header error: Log file entry 2 not found in cell BM1.');
        // Check if error in row 2 of the log file entry heading is found
        $this->checkFileForError('dynamic_header_logentry_row_2_error.xlsx', 'Header error: Editable not found in cell BL2.');
    }


    /**
     * Test group header checks
     */
    public function testImportExcelFileWithStaticFieldDataErrors(): void
    {
        // Check if missing name data is found
        $this->checkFileForError('static_data_required_name_missing_error.xlsx', 'Field Name cannot be empty in cell A5.');
        // Check if missing first name data is found
        $this->checkFileForError('static_data_required_first_name_missing_error.xlsx', 'Field First name cannot be empty in cell B10.');
        // Check if missing gender data is found
        $this->checkFileForError('static_data_required_gender_missing_error.xlsx', 'Field Gender cannot be empty in cell C8.');
        // Check if wrong gender data is found
        $this->checkFileForError('static_data_required_gender_wrong_error.xlsx', 'Field Gender must be m or f in cell C8.');
        // Check if missing preferred language data is found
        $this->checkFileForError('static_data_required_preferred_language_missing_error.xlsx', 'Field Preferred language cannot be empty in cell F6.');
    }


    /**
     * Test group header checks
     */
    public function testImportExcelFileWithDynamicFieldDataErrors(): void
    {
        // Check if missing company name data is found
        $this->checkFileForError('dynamic_data_required_company_name_missing_error.xlsx', 'Field Company name cannot be empty in cell O3.');
        // Check if missing phone number data is found
        $this->checkFileForError('dynamic_data_required_phone_number_missing_error.xlsx', 'Field Phone number cannot be empty in cell W3.');
        // Check if missing address type data is found
        $this->checkFileForError('dynamic_data_required_address_type_missing_error.xlsx', 'Field Address type cannot be empty in cell AI3.');
        // Check if missing committee function data is found
        $this->checkFileForError('dynamic_data_required_committee_function_missing_error.xlsx', 'Field Function cannot be empty in cell AQ4.');
        // Check if missing editable data is found
        $this->checkFileForError('dynamic_data_required_logentry_editable_nonbool_error.xlsx', 'Field Editable must be true or false in cell BL3.');
        // Check if missing editable data is found
        $this->checkFileForError('dynamic_data_required_logentry_editable_nonbool_text_error.xlsx', 'Field Editable must be true or false in cell BL3.');
    }


    /**
     * Check importing a correct file where there are some new select values.
     */
    public function testImportExcelFileWithNewSelectValues(): void
    {
        $testObject = $this->generateTestObject();
        $this->clearAllLogFiles();

        try {
            $testObject->checkExcelFileForImport(dirname(__FILE__)."/ImportExcelFiles/new_select_values.xlsx");
        } catch (Exception $e) {
            $this->fail("Unexpected exception: ".$e->getMessage().$e->getFile().$e->getLine());
        }

        // Check the numbers found during import
        $numbers = $testObject->getNumbers();
        $this->assertEquals(2, $numbers['addresses']);
        $this->assertEquals(5, $numbers['committeeFunctions']);
        $this->assertEquals(1, $numbers['companyInformation']);
        $this->assertEquals(3, $numbers['groupMembers']);
        $this->assertEquals(12, $numbers['memberEntries']);
        $this->assertEquals(4, $numbers['phoneNumbers']);
        $this->assertEquals(4, $numbers['mailingLists']);
        $this->assertEquals(2, $numbers['logFileEntries']);

        // Check the select values found during import
        $selectValues = $testObject->getSelectValues();
        // Check titles
        $this->assertEquals(4, count($selectValues['Title']['usedValues']));
        $this->assertContains('Dipl.-Ing.', $selectValues['Title']['usedValues']);
        $this->assertContains('M.A.', $selectValues['Title']['usedValues']);
        $this->assertContains('Dr.-Ing.', $selectValues['Title']['usedValues']);
        $this->assertContains('Prof.-Dr.', $selectValues['Title']['usedValues']);
        $this->assertEquals(2, count($selectValues['Title']['newValues']));
        $this->assertContains('Dr.-Ing.', $selectValues['Title']['newValues']);
        $this->assertContains('Prof.-Dr.', $selectValues['Title']['newValues']);
        // Check user status
        $this->assertEquals(3, count($selectValues['User status']['usedValues']));
        $this->assertContains('member', $selectValues['User status']['usedValues']);
        $this->assertContains('new_member', $selectValues['User status']['usedValues']);
        $this->assertContains('future_member', $selectValues['User status']['usedValues']);
        $this->assertEquals(1, count($selectValues['User status']['newValues']));
        $this->assertContains('new_member', $selectValues['User status']['newValues']);
        // Check membership type
        $this->assertEquals(3, count($selectValues['Membership type']['usedValues']));
        $this->assertContains('regular member', $selectValues['Membership type']['usedValues']);
        $this->assertContains('special member', $selectValues['Membership type']['usedValues']);
        $this->assertContains('Extra-special member', $selectValues['Membership type']['usedValues']);
        $this->assertEquals(2, count($selectValues['Membership type']['newValues']));
        $this->assertContains('special member', $selectValues['Membership type']['newValues']);
        $this->assertContains('Extra-special member', $selectValues['Membership type']['newValues']);
        // Check phone number type
        $this->assertEquals(5, count($selectValues['Phone number type']['usedValues']));
        $this->assertContains('home', $selectValues['Phone number type']['usedValues']);
        $this->assertContains('company', $selectValues['Phone number type']['usedValues']);
        $this->assertContains('private', $selectValues['Phone number type']['usedValues']);
        $this->assertContains('company-mobile', $selectValues['Phone number type']['usedValues']);
        $this->assertContains('mobile', $selectValues['Phone number type']['usedValues']);
        $this->assertEquals(2, count($selectValues['Phone number type']['newValues']));
        $this->assertContains('private', $selectValues['Phone number type']['newValues']);
        $this->assertContains('company-mobile', $selectValues['Phone number type']['newValues']);
        // Check address type
        $this->assertEquals(3, count($selectValues['Address type']['usedValues']));
        $this->assertContains('Home address', $selectValues['Address type']['usedValues']);
        $this->assertContains('Week address', $selectValues['Address type']['usedValues']);
        $this->assertContains('Company address', $selectValues['Address type']['usedValues']);
        $this->assertEquals(1, count($selectValues['Address type']['newValues']));
        $this->assertContains('Week address', $selectValues['Address type']['newValues']);
        // Check committee
        $this->assertEquals(4, count($selectValues['Committee']['usedValues']));
        $this->assertContains('board', $selectValues['Committee']['usedValues']);
        $this->assertContains('activities', $selectValues['Committee']['usedValues']);
        $this->assertContains('sports', $selectValues['Committee']['usedValues']);
        $this->assertContains('public relations', $selectValues['Committee']['usedValues']);
        $this->assertEquals(1, count($selectValues['Committee']['newValues']));
        $this->assertContains('sports', $selectValues['Committee']['newValues']);
        // Check function
        $this->assertEquals(3, count($selectValues['Function']['usedValues']));
        $this->assertContains('chairman', $selectValues['Function']['usedValues']);
        $this->assertContains('vice chairman', $selectValues['Function']['usedValues']);
        $this->assertContains('member', $selectValues['Function']['usedValues']);
        $this->assertEquals(0, count($selectValues['Function']['newValues']));
        // Check committee function
        $this->assertEquals(9, count($selectValues['Committee function']['usedValues']));
        $this->assertContains('activities - chairman', $selectValues['Committee function']['usedValues']);
        $this->assertContains('activities - member', $selectValues['Committee function']['usedValues']);
        $this->assertContains('activities - vice chairman', $selectValues['Committee function']['usedValues']);
        $this->assertContains('board - chairman', $selectValues['Committee function']['usedValues']);
        $this->assertContains('board - vice chairman', $selectValues['Committee function']['usedValues']);
        $this->assertContains('board - member', $selectValues['Committee function']['usedValues']);
        $this->assertContains('public relations - member', $selectValues['Committee function']['usedValues']);
        $this->assertContains('sports - chairman', $selectValues['Committee function']['usedValues']);
        $this->assertContains('sports - member', $selectValues['Committee function']['usedValues']);
        $this->assertEquals(3, count($selectValues['Committee function']['newValues']));
        $this->assertContains('activities - vice chairman', $selectValues['Committee function']['newValues']);
        $this->assertContains('sports - chairman', $selectValues['Committee function']['newValues']);
        $this->assertContains('sports - member', $selectValues['Committee function']['newValues']);
        // Check country
        $this->assertEquals(2, count($selectValues['Country']['usedValues']));
        $this->assertContains('Germany', $selectValues['Country']['usedValues']);
        $this->assertContains('Belgium', $selectValues['Country']['usedValues']);
        $this->assertEquals(1, count($selectValues['Committee']['newValues']));
        $this->assertContains('Belgium', $selectValues['Country']['newValues']);
        // Check mailinglists
        $this->assertEquals(3, count($selectValues['Mailing list']['usedValues']));
        $this->assertContains('MailingList_3', $selectValues['Mailing list']['usedValues']);
        $this->assertContains('MailingList_4', $selectValues['Mailing list']['usedValues']);
        $this->assertContains('MailingList_5', $selectValues['Mailing list']['usedValues']);
        $this->assertEquals(2, count($selectValues['Mailing list']['newValues']));
        $this->assertContains('MailingList_4', $selectValues['Mailing list']['newValues']);
        $this->assertContains('MailingList_5', $selectValues['Mailing list']['newValues']);

        // Check if the correct log message is written
        $logMessages = $this->getAllLogs();
        $this->assertEquals(1, count($logMessages));
        $this->assertEquals('Import action', $logMessages[0]->getChangeType()->getChangeType());
        $this->assertEquals('File check for importing file new_select_values.xlsx was successful, 12 member entries found.', $logMessages[0]->getLogentry());
    }


    /**
     * Test importing data with errors.
     */
    public function testImportDataWithErrors(): void
    {
        $testObject = $this->generateTestObject();

        $exceptionOccurred = false;
        try {
            $testObject->importExcelFile(dirname(__FILE__)."/ImportExcelFiles/static_header_error.xlsx");
        } catch (Exception $e) {
            $this->assertEquals('Header error: User role not found in cell N1.', $e->getMessage());
            $exceptionOccurred = true;
        }
        $this->assertTrue($exceptionOccurred, "Expected exception did not occur.");
    }


    /**
     * Test if all needed select values are created on import
     */
    public function testImportDataSelectValues(): void
    {
        $testObject = $this->generateTestObject();
        $this->clearAllLogFiles();

        try {
            $testObject->importExcelFile(dirname(__FILE__)."/ImportExcelFiles/all_select_values.xlsx");
        } catch (Exception $e) {
            $this->fail("Unexpected exception: ".$e->getMessage());
        }

        /** @var Title $newTitle */
        $newTitle = $this->getEntityManager()->getRepository(Title::class)->findOneBy(['title' => 'Dr.-Ing.']);
        $this->assertEquals('Dr.-Ing.', $newTitle);
        $newTitle = $this->getEntityManager()->getRepository(Title::class)->findOneBy(['title' => 'Prof.-Dr.']);
        $this->assertEquals('Prof.-Dr.', $newTitle);

        /** @var Status $newStatus */
        $newStatus = $this->getEntityManager()->getRepository(Status::class)->findOneBy(['status' => 'new_member']);
        $this->assertEquals('new_member', $newStatus);

        /** @var MembershipType $newMembershipType */
        $newMembershipType = $this->getEntityManager()->getRepository(MembershipType::class)
                                           ->findOneBy(['type' => 'special member']);
        $this->assertEquals('special member', $newMembershipType);

        /** @var PhoneNumberType $newPhoneNumberType */
        $newPhoneNumberType = $this->getEntityManager()->getRepository(PhoneNUmberType::class)
                                            ->findOneBy(['phoneNumberType' => 'private']);
        $this->assertEquals('private', $newPhoneNumberType);

        /** @var AddressType $newAddressType */
        $newAddressType = $this->getEntityManager()->getRepository(AddressType::class)
                                        ->findOneBy(['addressType' => 'Week address']);
        $this->assertEquals('Week address', $newAddressType);

        /** @var Committee $newCommittee */
        $newCommittee = $this->getEntityManager()->getRepository(Committee::class)
                                      ->findOneBy(['committeeName' => 'sports']);
        $this->assertEquals('sports', $newCommittee);

        /** @var CommitteeFunction $newFunction */
        $newFunction = $this->getEntityManager()->getRepository(CommitteeFunction::class)
                                     ->findOneBy(['committeeFunctionName' => 'chief']);
        $this->assertEquals('chief', $newFunction);

        /** @var CommitteeFunctionMap $newCommitteeFunction */
        $newCommitteeFunction = $this->getEntityManager()->getRepository(CommitteeFunctionMap::class)
                                              ->findOneByCommitteeFunction('sports - member');
        $this->assertEquals('sports - Member', $newCommitteeFunction);
        $newCommitteeFunction = $this->getEntityManager()->getRepository(CommitteeFunctionMap::class)
                                              ->findOneByCommitteeFunction('sports - chief');
        $this->assertEquals('sports - chief', $newCommitteeFunction);

        // Check if the correct log message is written
        /** @var array|LogfileEntry[] $logMessages */
        $logMessages = $this->getAllLogs();
        $this->assertEquals(14, count($logMessages));
        $this->assertEquals('Import action', $logMessages[0]->getChangeType()->getChangeType());
        $expected = '{"numberOfDataFields":1,"mainMessage":"Imported the following select values:",';
        $expected .= '"dataArray":{"Title":["Dr.-Ing., Prof.-Dr."],"User status":["new_member"],';
        $expected .= '"Membership type":["special member"],"Phone number type":["private"],';
        $expected .= '"Address type":["Week address"],"Country":["Belgium"],"Committee":["sports"],';
        $expected .= '"Function":["chief"],';
        $expected .= '"Mailing list":["MailingList_4, MailingList_5"],';
        $expected .= '"Committee function":["activities - vice chairman, sports - chief, sports - member"]}}';
        $this->assertEquals($expected, $logMessages[13]->getLogentry());
    }


    /**
     * Test if all needed select values are created on import
     */
    public function testImportMemberData(): void
    {
        $testObject = $this->generateTestObject();
        $this->clearAllLogFiles();

        $memberRepo = $this->getEntityManager()->getRepository(MemberEntry::class);
        $numberOfMembers = count($memberRepo->findAll());
        $this->assertEquals(12, $numberOfMembers);

        try {
            $testObject->importExcelFile(dirname(__FILE__)."/ImportExcelFiles/new_select_and_member_values.xlsx");
        } catch (Exception $e) {
            $this->fail("Unexpected exception: ".$e->getMessage());
        }

        $numberOfMembers = count($memberRepo->findAll());
        $this->assertEquals(24, $numberOfMembers);
        // Test some samples of the inserted data
        $member21 = $memberRepo->findOneBy(['name' => 'Lastname21']);
        $this->assertEquals('m', $member21->getGender());
        $this->assertEquals('Lastname21', $member21->getName());
        $this->assertEquals('Firstname21', $member21->getFirstName());
        $this->assertEquals(new DateTime("1974-02-07"), $member21->getBirthday());
        $this->assertEquals('firstname21@email.com', $member21->getEmail());
        $this->assertEquals(2016, $member21->getMemberSince());
        $this->assertEquals(false, $member21->getMembershipEndRequested());
        $this->assertEquals('Dipl.-Ing.', $member21->getTitle());
        $this->assertEquals('German', $member21->getPreferredLanguage());
        $this->assertEquals('Member', $member21->getStatus());
        $this->assertEquals('Regular member', $member21->getMembershipType());
        $this->assertEquals('System administrator', $member21->getUserRole());
        $this->assertEquals(21, $member21->getMembershipNumber()->getMembershipNumber());
        $this->assertEquals(true, $member21->getMembershipNumber()->getUseDirectDebit());
        $logMessages = $this->getEntityManager()->getRepository(LogfileEntry::class)
                                           ->findBy(['changesOnMember' => $member21]);
        $this->assertCount(2, $logMessages);

        // Ampersands in the company must be replaced by &amp; to prevent errors in the patex serial letter
        $this->assertEquals('My Company1 &amp; My Company2', $member21->getCompanyInformation()->getName());

        /** @var PhoneNumber[] $phoneNumbers */
        $phoneNumbers = $member21->getPhoneNumbersAsArray();
        $keys = array_keys($phoneNumbers);
        $this->assertEquals(4, count($phoneNumbers));
        $this->assertEquals('Home', $phoneNumbers[$keys[0]]['type']);
        $this->assertEquals('123', $phoneNumbers[$keys[0]]['phone_number']);
        $this->assertEquals('Company', $phoneNumbers[$keys[1]]['type']);
        $this->assertEquals('456', $phoneNumbers[$keys[1]]['phone_number']);
        $this->assertEquals('Mobile', $phoneNumbers[$keys[2]]['type']);
        $this->assertEquals('789', $phoneNumbers[$keys[2]]['phone_number']);
        $this->assertEquals('Company', $phoneNumbers[$keys[3]]['type']);
        $this->assertEquals('012', $phoneNumbers[$keys[3]]['phone_number']);

        $addresses = $member21->getAddressesAsArray();
        $this->assertEquals(2, count($addresses));
        $this->assertEquals('Home address', $addresses[0]['type']);
        $this->assertEquals('MyAddress', $addresses[0]['address']);
        $this->assertEquals(true, $addresses[0]['is_main_address']);
        $this->assertEquals('Company address', $addresses[1]['type']);
        $this->assertEquals('MyAddress2', $addresses[1]['address']);
        $this->assertEquals(false, $addresses[1]['is_main_address']);

        $committees = $member21->getCommitteeFunctionMaps();
        $this->assertEquals(5, count($committees));
        $this->assertEquals('sports - chief', $committees[0]);
        $this->assertEquals('Board - Vice chairman', $committees[1]);
        $this->assertEquals('Board - Member', $committees[2]);
        $this->assertEquals('Activities - Member', $committees[3]);
        $this->assertEquals('Public relations - Member', $committees[4]);

        /** @var MemberEntry[] $groupMembers */
        $groupMembers = $member21->getGroupMembers();
        $this->assertEquals(3, count($groupMembers));
        $this->assertEquals('Firstname29', $groupMembers[0]->getFirstName());
        $this->assertEquals('Firstname30', $groupMembers[1]->getFirstName());
        $this->assertEquals('Firstname31', $groupMembers[2]->getFirstName());

        $mailingLists = $member21->getMailingLists();
        $this->assertEquals(1, count($mailingLists));
        $this->assertEquals('MailingList_3', $mailingLists[0]);
        /** @var MailingList[] $mailingLists */
        $mailingLists = $memberRepo->findOneBy(['name' => 'Lastname22'])->getMailingLists();
        $this->assertEquals(2, count($mailingLists));
        $this->assertEquals('MailingList_5', $mailingLists[0]->getListName());
        $this->assertEquals('MailingList_4', $mailingLists[1]->getListName());

        $member27 = $memberRepo->findOneBy(['name' => 'Lastname27']);
        $this->assertEquals('f', $member27->getGender());
        $this->assertEquals('Lastname27', $member27->getName());
        $this->assertEquals('Firstname27', $member27->getFirstName());
        $this->assertEquals('firstname27@email.com', $member27->getEmail());
        $this->assertEquals('Prof.-Dr.', $member27->getTitle());
        $this->assertEquals(26, $member27->getMembershipNumber()->getMembershipNumber());
        $this->assertEquals(false, $member27->getMembershipNumber()->getUseDirectDebit());

        /** @var MemberEntry $member29 */
        $member29 = $memberRepo->findOneBy(['firstName' => 'Firstname29']);
        $this->assertEquals(1, count($member29->getGroupMembers()));
        $this->assertEquals('Firstname21', $member29->getGroupMembers()[0]->getFirstName());

        /** @var MemberEntry $member30 */
        $member30 = $memberRepo->findOneBy(['firstName' => 'Firstname30']);
        $this->assertEquals(1, count($member30->getGroupMembers()));
        $this->assertEquals('Firstname21', $member30->getGroupMembers()[0]->getFirstName());

        /** @var MemberEntry $member31 */
        $member31 = $memberRepo->findOneBy(['firstName' => 'Firstname31']);
        $this->assertEquals(1, count($member31->getGroupMembers()));
        $this->assertEquals('Firstname21', $member31->getGroupMembers()[0]->getFirstName());
        $this->assertEquals(21, $member31->getMembershipNumber()->getMembershipNumber());
        $this->assertEquals(true, $member31->getMembershipNumber()->getUseDirectDebit());


        // Check if the correct log message is written
        /** @var array|LogfileEntry[] $logMessages */
        $logMessages = $this->getEntityManager()->getRepository(LogfileEntry::class)->findBy([], ['id' => 'DESC']);
        $this->assertEquals(16, count($logMessages));
        $this->assertEquals('Import action', $logMessages[0]->getChangeType()->getChangeType());
        $expected = 'Importing file new_select_and_member_values.xlsx was successful, 12 member entries were imported.';
        $this->assertEquals($expected, $logMessages[0]->getLogentry());
        $this->assertEquals('Log entry 2', $logMessages[12]->getLogentry());
        $member22 = $memberRepo->findOneBy(['firstName' => 'Firstname22']);
        $this->assertEquals($member22->getId(), $logMessages[12]->getChangesOnMember()->getId());
        $this->assertEquals('Log entry 1', $logMessages[14]->getLogentry());
        $this->assertEquals($member21->getId(), $logMessages[14]->getChangesOnMember()->getId());
    }


    /**
     * Test if all needed select values are created on import
     */
    public function testImportMemberDataWithDoubleEntries(): void
    {
        $testObject = $this->generateTestObject();
        $this->clearAllLogFiles();

        $memberRepo = $this->getEntityManager()->getRepository(MemberEntry::class);
        $numberOfMembers = count($memberRepo->findAll());
        $this->assertEquals(12, $numberOfMembers);

        try {
            $testObject->importExcelFile(dirname(__FILE__)."/ImportExcelFiles/double_member_entry.xlsx");
        } catch (Exception $e) {
            $this->fail("Unexpected exception: ".$e->getMessage());
        }

        $numberOfMembers = count($memberRepo->findAll());
        $this->assertEquals(12, $numberOfMembers);
        /** @var MemberEntry $member */
        $member = $memberRepo->findOneBy(['firstName' => 'Firstname1']);
        $this->assertEquals('m', $member->getGender());
        $this->assertEquals('Lastname1', $member->getName());
        $this->assertEquals('Firstname1', $member->getFirstName());
        $this->assertEquals(new DateTime("1974-02-07"), $member->getBirthday());
        $this->assertEquals('firstname1@email.com', $member->getEmail());
        $this->assertEquals(2016, $member->getMemberSince());
        $this->assertEquals(false, $member->getMembershipEndRequested());
        $this->assertEquals('Dipl.-Ing.', $member->getTitle());
        $this->assertEquals('German', $member->getPreferredLanguage());
        $this->assertEquals('Member', $member->getStatus());
        $this->assertEquals('Regular member', $member->getMembershipType());
        $this->assertEquals('System administrator', $member->getUserRole());
        $this->assertEquals(35, $member->getMembershipNumber()->getMembershipNumber());

        $this->assertEquals('My Company2', $member->getCompanyInformation()->getName());

        $phoneNumbers = $member->getPhoneNumbersAsArray();
        $keys = array_keys($phoneNumbers);
        $this->assertEquals(3, count($phoneNumbers));
        $this->assertEquals('private', $phoneNumbers[$keys[0]]['type']);
        $this->assertEquals('321', $phoneNumbers[$keys[0]]['phone_number']);
        $this->assertEquals('Company', $phoneNumbers[$keys[1]]['type']);
        $this->assertEquals('456', $phoneNumbers[$keys[1]]['phone_number']);
        $this->assertEquals('Mobile', $phoneNumbers[$keys[2]]['type']);
        $this->assertEquals('789', $phoneNumbers[$keys[2]]['phone_number']);

        $addresses = $member->getAddressesAsArray();
        $this->assertEquals(2, count($addresses));
        $this->assertEquals('Week address', $addresses[0]['type']);
        $this->assertEquals('MyAddress new', $addresses[0]['address']);
        $this->assertEquals(true, $addresses[0]['is_main_address']);
        $this->assertEquals('Company address', $addresses[1]['type']);
        $this->assertEquals('MyAddress2', $addresses[1]['address']);
        $this->assertEquals(false, $addresses[1]['is_main_address']);

        $committees = $member->getCommitteeFunctionMaps();
        $this->assertEquals(1, count($committees));
        $this->assertEquals('Activities - Chairman', $committees->first());

        /** @var MemberEntry[] $groupMembers */
        $groupMembers = $member->getGroupMembers();
        $this->assertEquals(0, count($groupMembers));
    }


    /**
     * Test if the service is registered correctly in the service container
     */
    public function testServiceRegistration(): void
    {
        $this->checkContainerRegistration(ImportExcelDataFile::class);
    }


    /**
     * Pass a file to the import excel data class and check if the expected error message is returned.
     *
     * @param string $fileName File name in the directory ImportExcelFiles
     * @param string $expected Expected error message.
     */
    private function checkFileForError(string $fileName, string $expected): void
    {
        $testObject = $this->generateTestObject();

        // Check if error in row 1 of the group members heading if found
        $exceptionOccurred = false;
        try {
            $testObject->checkExcelFileForImport(dirname(__FILE__)."/ImportExcelFiles/$fileName");
        } catch (Exception $e) {
            $this->assertEquals($expected, $e->getMessage());
            $this->assertEquals($expected, $testObject->getErrorMessage());
            $exceptionOccurred = true;
        }
        $this->assertTrue($exceptionOccurred, "Expected exception did not occur for file $fileName.");
    }


    /**
     * @return ImportExcelDataFile
     */
    private function generateTestObject(): ImportExcelDataFile
    {
        /** @var LogEventsDisabler $logEventDisabler */
        $logEventDisabler = $this->getContainer()->get(LogEventsDisabler::class);
        /** @var LogMessageCreator $logMessageCreator */
        $logMessageCreator = $this->getContainer()->get(LogMessageCreator::class);

        return new ImportExcelDataFile($this->getEntityManager(), $logMessageCreator, $logEventDisabler);
    }
}
