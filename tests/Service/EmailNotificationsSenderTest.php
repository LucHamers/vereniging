<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Service;

use App\Entity\ChangeType;
use App\Entity\ComplexLogEntryDoubleDataField;
use App\Entity\LogfileEntry;
use App\Entity\MemberEntry;
use App\Service\EmailNotificationSender;
use App\Tests\TestCase;
use DateTime;
use Exception;
use Symfony\Component\Mailer\Exception\TransportException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Header\MailboxListHeader;
use Symfony\Component\Security\Core\Security;

/**
 * Class EmailNotificationsSenderTest
 */
class EmailNotificationsSenderTest extends TestCase
{
    private ?MemberEntry $member1;
    private ?MemberEntry $member3;


    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();

        $this->member1 = $this->getFixtureReference('member_entry_last_name1');
        $this->member3 = $this->getFixtureReference('member_entry_last_name3');
        $member5 = $this->getFixtureReference('member_entry_last_name5');

        // Initialize member notifications for this test (none are defined before):
        // Firstname1: export, error, member data change
        // Firstname3: export
        // Firstname5: export
        /** @var ChangeType $changeTypeMemberData */
        $changeTypeMemberData = $this->getFixtureReference('change_type_member_data_change');
        /** @var ChangeType $changeTypeError */
        $changeTypeError = $this->getFixtureReference('change_type_error');
        /** @var ChangeType $changeTypeExport */
        $changeTypeExport = $this->getFixtureReference('change_type_export_action');
        $this->member1->addEmailNotification($changeTypeMemberData);
        $this->member1->addEmailNotification($changeTypeError);
        $this->member1->addEmailNotification($changeTypeExport);
        $this->member3->addEmailNotification($changeTypeExport);
        $member5->addEmailNotification($changeTypeExport);
        $this->getEntityManager()->flush();
    }


    /**
     * Test if email notifications are sent to the correct email addresses.
     *
     * @throws Exception
     */
    public function testSendEmailNotification(): void
    {
        list($mailerMock, $notificationSender) = $this->createTestClass();

        $mailerMock->expects($this->once())
            ->method('send')
            ->with($this->callback(function (Email $message) {
                $expected = [
                    'from' => ['sender-change-en@email.com'],
                    'to' => ['firstname1@email.com'],
                    'Subject' => 'Change with change type "Error"',
                    'HtmlBody' => "change content: <p>Firstname1 Lastname1 did the following changes on Firstname3 Lastname3:</p>\n\n<table><tr><td colspan=\"4\">testmessage</td></tr></table>",
                    ];

                return $this->checkMailMessage($expected, $message);
            }));

        $logEntry = new LogfileEntry();
        $logEntry->setDate(new DateTime('2019-01-01'));
        $logEntry->setChangedByMember($this->member1);
        $logEntry->setChangesOnMember($this->member3);
        $logEntry->setChangeType($this->getFixtureReference('change_type_error'));
        $logEntry->setLogentry('testmessage');

        $notificationSender->sendEmailNotification($logEntry);
        $this->assertNotNull($notificationSender);
        $this->assertInstanceOf(EmailNotificationSender::class, $notificationSender);
    }


    /**
     * Test if email notifications are translated correctly.
     *
     * @throws Exception
     */
    public function testSendEmailNotificationTranslations(): void
    {
        list($mailerMock, $notificationSender) = $this->createTestClass();

        $mailerMock->expects($this->exactly(2))
            ->method('send')
            ->withConsecutive(
                [
                    $this->callback(function (Email $message) {
                        $expected = [
                            'from' => ['sender-change-en@email.com'],
                            'to' => ['firstname1@email.com'],
                            'Subject' => 'Change with change type "Export action"',
                            'HtmlBody' => "change content: <p>Firstname1 Lastname1 did the following changes on Firstname3 Lastname3:</p>\n\n<table><tr><td colspan=\"4\">testmessage</td></tr></table>",
                        ];

                        return $this->checkMailMessage($expected, $message);
                    }), ],
                [
                    $this->callback(function (Email $message) {
                        $expected = [
                            'from' => ['sender-change-nl@email.com'],
                            'to' => ['firstname3@email.com', 'firstname5@email.com'],
                            'Subject' => 'Verandering van het type "Exportactie"',
                            'HtmlBody' => "change content: <p>Firstname1 Lastname1 heeft de volgende veranderingen bij Firstname3 Lastname3 doorgevoerd:</p>\n\n<table><tr><td colspan=\"4\">testmessage</td></tr></table>",
                        ];

                        return $this->checkMailMessage($expected, $message);
                    }), ],
            );

        $logEntry = new LogfileEntry();
        $logEntry->setDate(new DateTime('2019-01-01'));
        $logEntry->setChangedByMember($this->member1);
        $logEntry->setChangesOnMember($this->member3);
        $logEntry->setChangeType($this->getFixtureReference('change_type_export_action'));
        $logEntry->setLogentry('testmessage');

        $notificationSender->sendEmailNotification($logEntry);
    }


    /**
     * Test if email notifications are send to the correct email addresses.
     *
     * @throws Exception
     */
    public function testSendEmailNotificationWithComplexLogMessage(): void
    {
        list($mailerMock, $notificationSender) = $this->createTestClass();

        $mailerMock->expects($this->once())
            ->method('send')
            ->with($this->callback(function (Email $message) {
                $body = <<<'EOD'
change content: <p>Firstname1 Lastname1 did the following changes on Firstname3 Lastname3:</p>

<table><tr><td colspan="4">Updated entry of type member :</td></tr>
                                                                                                                    <tr><td>Gender:</td><td>Male</td><td>&nbsp;&rarr;&nbsp;</td><td>Female</td></tr>
                                    </table>
EOD;

                $expected = [
                    'HtmlBody' => $body,
                ];

                return $this->checkMailMessage($expected, $message);
            }));

        $logEntry = new LogfileEntry();
        $logEntry->setDate(new DateTime('2019-01-01'));
        $logEntry->setChangedByMember($this->member1);
        $logEntry->setChangesOnMember($this->member3);
        $logEntry->setChangeType($this->getFixtureReference('change_type_member_data_change'));
        $complexLogContent = new ComplexLogEntryDoubleDataField();
        $complexLogContent->setMainMessage("Updated %classname% entry %stringIdentifier%|member,");
        $complexLogContent->addDataField('gender', ['Male', 'Female']);
        $logEntry->setLogentry($complexLogContent);

        $notificationSender->sendEmailNotification($logEntry);
    }


    /**
     *
     * @throws Exception
     */
    public function testSendEmailNotificationWithoutChangesOn(): void
    {
        list($mailerMock, $notificationSender) = $this->createTestClass();

        $mailerMock->expects($this->once())
            ->method('send')
            ->with($this->callback(function (Email $message) {
                $expected = [
                    'HtmlBody' => "change content: <p>Firstname1 Lastname1 did the following changes:</p>\n\n<table><tr><td colspan=\"4\"></td></tr></table>",
                ];

                return $this->checkMailMessage($expected, $message);
            }));

        $logEntry = new LogfileEntry();
        $logEntry->setDate(new DateTime('2019-01-01'));
        $logEntry->setChangedByMember($this->member1);
        $logEntry->setChangeType($this->getFixtureReference('change_type_member_data_change'));

        $notificationSender->sendEmailNotification($logEntry);
    }


    /**
     *
     * @throws Exception
     */
    public function testSendEmailNotificationWithoutSubscribers(): void
    {
        list($mailerMock, $notificationSender) = $this->createTestClass();

        $mailerMock->expects($this->never())
                   ->method('send');

        $logEntry = new LogfileEntry();
        $logEntry->setDate(new DateTime('2019-01-01'));
        $logEntry->setChangedByMember($this->member1);
        $logEntry->setChangeType($this->getFixtureReference('change_type_system_settings_change'));

        $notificationSender->sendEmailNotification($logEntry);
    }


    /**
     *
     * @throws Exception
     */
    public function testSetSiteName(): void
    {
        list($mailerMock, $notificationSender) = $this->createTestClass('{"en":"My site"}');

        $mailerMock->expects($this->once())
            ->method('send')
            ->with($this->callback(function (Email $message) {
                $expected = [
                   'To' => ['firstname1@email.com'],
                   'Subject' => 'Change to My site with change type "Member data change"',
                ];

                return $this->checkMailMessage($expected, $message);
            }));

        $logEntry = new LogfileEntry();
        $logEntry->setDate(new DateTime('2019-01-01'));
        $logEntry->setChangedByMember($this->member1);
        $logEntry->setChangesOnMember($this->member3);
        $logEntry->setChangeType($this->getFixtureReference('change_type_member_data_change'));

        $notificationSender->sendEmailNotification($logEntry);
    }


    /**
     *
     * @throws Exception
     */
    public function testSetSiteNameWithException(): void
    {
        try {
            $this->createTestClass('{"en:"My site"}');

            $this->fail('Expected exception has not been thrown!');
        } catch (Exception $e) {
            $this->assertEquals('Exception in class App\Service\EmailNotificationSender in line 74: Malformed site name json string: {"en:"My site"}', $e->getMessage());
        }
    }


    /**
     * Test if an exception from the mailer is handled correctly
     *
     * @throws Exception
     */
    public function testEmailSendException(): void
    {
        list($mailerMock, $notificationSender) = $this->createTestClass();

        $mailerMock->expects($this->any())
            ->method('send')
            ->will($this->throwException(new TransportException('sender exception')));

        $this->setLoggedInUserForLogging(true);

        $logEntry = new LogfileEntry();
        $logEntry->setChangeType($this->getFixtureReference('change_type_member_data_change'));
        $logEntry->setChangedByMember($this->member1);

        $notificationSender->sendEmailNotification($logEntry);

        $lastLogMessage = $this->getAllLogs()[0];
        $this->assertNotNull($lastLogMessage);
        $this->assertFalse($lastLogMessage->isComplexLogentry());
        $this->assertEquals('Exception in class App\Service\EmailNotificationSender in line 132: sender exception', $lastLogMessage->getLogentry());
    }


    /**
     * Test if the service is registered correctly in the service container
     */
    public function testServiceRegistration(): void
    {
        $this->checkContainerRegistration(EmailNotificationSender::class);
    }


    /**
     * Check the content of a mail message.
     *
     * @param array $expected Array keys can be "To", "From", "Subject", "Body"
     * @param Email $message  Message to compare to
     *
     * @return bool true when the message is correct
     */
    private function checkMailMessage(array $expected, Email $message): bool
    {
        // Iterate over all expected values. Since the getTo and getFrom both return an array, they must be handled
        // separately
        foreach ($expected as $function => $expectedValue) {
            // Check body and subject
            if (('HtmlBody' === $function) || ('Subject' === $function)) {
                if ($message->{"get$function"}() !== $expectedValue) {
                    echo "$function: ".$message->{"get$function"}()."!=$expectedValue\n";

                    return false;
                }
            } else {
                // Check to and from
                /** @var MailboxListHeader $headerContent */
                $headerContent = $message->getHeaders()->get($function);
                if (count($headerContent->getAddresses()) !== count($expectedValue)) {
                    echo "$function count: ".count($headerContent->getAddresses())."!=".count($expectedValue)."\n";

                    return false;
                }
                foreach ($headerContent->getAddressStrings() as $index => $value) {
                    if ($value !== $expectedValue[$index]) {
                        echo "To $index: $value!=".$expectedValue[$index]."\n";

                        return false;
                    }
                }
            }
        }

        return true;
    }


    /**
     *
     * @param string|null $siteName
     *
     * @return array
     *
     * @throws Exception
     */
    private function createTestClass(string $siteName = null): array
    {
        // Create a mock for the mailer.
        $mailerMock = $this->createMock(MailerInterface::class);
        $notificationSender = new EmailNotificationSender(
            $this->getEntityManager(),
            $mailerMock,
            $this->getContainer()->get('twig'),
            $this->getTranslator(),
            $this->createMock(Security::class),
            $siteName
        );

        return array($mailerMock, $notificationSender);
    }
}
