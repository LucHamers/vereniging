<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Form;

use App\Entity\MembershipType;
use App\Form\FormCollection;
use App\Tests\TestCase;

/**
 * Class FormCollectionTest
 */
class FormCollectionTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->setLoggedInUserForLogging();
    }


    /**
     * Test if the member selection by status and role form can be submitted.
     */
    public function testConstruct(): void
    {
        $members = ['test1', 'test2'];

        $formCollection = new FormCollection($members, $this->getEntityManager());
        $this->assertEquals(2, $formCollection->getMembers()->count());
    }


    /**
     * Test the addType and removeType methods
     */
    public function testAddRemove(): void
    {
        $entityManager = $this->getEntityManager();

        $newType = new MembershipType();
        $newType->setType("Test type");
        $newType->setMembershipFee(24);
        $newType->setGroupMembership(false);
        $newType->setDefaultGroupMembershipType(false);
        $newType->setDefaultNewMembershipType(false);

        $formCollection = new FormCollection([], $entityManager);
        $this->assertEquals(0, $formCollection->getMembers()->count());

        // Check if the element is added to the internal list
        $formCollection->addMember($newType);
        $this->assertEquals(1, $formCollection->getMembers()->count());

        // Check that the element cannot be added twice
        $formCollection->addMember($newType);
        $this->assertEquals(1, $formCollection->getMembers()->count());

        // Check if the added member is also written to the database
        $typesInDb = $entityManager->getRepository(MembershipType::class)->findAll();
        $startTypeCountInDb = count($typesInDb);
        $entityManager->flush();
        $typesInDb = $entityManager->getRepository(MembershipType::class)->findAll();
        $typesCountInDb = count($typesInDb);
        $this->assertEquals($startTypeCountInDb + 1, $typesCountInDb);
        $this->assertContains($newType, $typesInDb);

        // Check if the element is removed from the internal list
        $formCollection->removeMember($newType);
        $this->assertEquals(0, $formCollection->getMembers()->count());

        // Check that the element cannot be removed twice
        $formCollection->removeMember($newType);
        $this->assertEquals(0, $formCollection->getMembers()->count());

        // Check if the removed member is also removed from the database
        $entityManager->flush();
        $typesInDb = $entityManager->getRepository(MembershipType::class)->findAll();
        $typesCountInDb = count($typesInDb);
        $this->assertEquals($startTypeCountInDb, $typesCountInDb);
        $this->assertNotContains($newType, $typesInDb);

        // Check that a member with an empty type name is not added
        $emptyType = new MembershipType();
        $formCollection->addMember($emptyType);
        $this->assertEquals(0, $formCollection->getMembers()->count());

        // Check that a member with an empty membership fee is added and the membership fee is set to 0.
        $emptyType->setType('some type');
        $formCollection->addMember($emptyType);
        $this->assertEquals(1, $formCollection->getMembers()->count());
        $this->assertEquals(0, $emptyType->getMembershipFee());
    }
}
