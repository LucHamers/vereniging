<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\Country;
use App\Tests\TestCase;

/**
 * Class CountryTest
 */
class CountryTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->setLoggedInUserForLogging();
    }


    /**
     * Test if the id is set when the country is saved.
     */
    public function testGetId(): void
    {
        $country = new Country();
        $country->setCountry('new country');
        $this->assertNull($country->getId());
        $this->getEntityManager()->persist($country);
        $this->getEntityManager()->flush();
        $this->assertNotNull($country->getId());
    }


    /**
     * Test the inUse method.
     */
    public function testIsInUse(): void
    {
        $country = $this->getEntityManager()->getRepository(Country::class)->findOneBy(['country' => 'Germany']);
        $country = $this->getEntityManager()->getRepository(Country::class)->findOneBy(['country' => 'Germany']);
        $this->assertTrue($country->isInUse());
        $country = $this->getEntityManager()->getRepository(Country::class)->findOneBy(['country' => 'Netherlands']);
        $this->assertTrue($country->isInUse());
    }


    /**
     * Test if the findAll method in the repository class has been overloaded to return the values ordered ascending.
     */
    public function testFindAll(): void
    {
        $countries = $this->getEntityManager()->getRepository(Country::class)->findAll();
        $this->assertEquals("Germany", $countries[0]->getCountry());
        $this->assertEquals("Netherlands", $countries[1]->getCountry());
    }


    /**
     * Check if translations for the country table work correctly
     */
    public function testTranslation(): void
    {
        $country = $this->getEntityManager()->getRepository(Country::class)->findOneBy(['country' => 'Germany']);

        $this->assertEquals("Germany", $country->getCountry());
        $this->assertEquals("Germany", $country);
        $country->setCurrentLocale('de');
        $this->assertEquals("Deutschland", $country);
        $this->assertEquals("Germany", $country->translate('en')->getCountryTranslated());
        $this->assertEquals("Duitsland", $country->translate('nl')->getCountryTranslated());
        $this->assertEquals("Deutschland", $country->translate('de')->getCountryTranslated());

        $country->addNewTranslation('es', 'Germany in Spanish');
        $this->getEntityManager()->flush();
        $this->getEntityManager()->refresh($country);
        $this->assertEquals('Germany in Spanish', $country->translate('es')->getCountryTranslated());
    }
}
