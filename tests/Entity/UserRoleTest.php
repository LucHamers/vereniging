<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\Status;
use App\Entity\UserRole;
use App\Tests\TestCase;

/**
 * Class UserRole
 */
class UserRoleTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test __toString method.
     */
    public function testToString(): void
    {
        $role = $this->getFixtureReference('user_role_role_user');
        $this->assertEquals('User', $role);
        $role = $this->getFixtureReference('user_role_role_member_administrator');
        $this->assertEquals('Member administrator', $role);
        $role = $this->getFixtureReference('user_role_role_system_administrator');
        $this->assertEquals('System administrator', $role);
    }


    /**
     * Test getSystemRoles method.
     */
    public function testGetSetSystemRoles(): void
    {
        $newRole = new UserRole();

        // Check if the view dashboard role is returned when no other roles are set.
        $this->assertContains('ROLE_VIEW_DASHBOARD', $newRole->getSystemRoles());

        // Check if the view dashboard role can be added.
        $newRole->setSystemRoles(['ROLE_VIEW_DASHBOARD']);
        $this->assertContains('ROLE_VIEW_DASHBOARD', $newRole->getSystemRoles());
        $this->assertEquals(1, count($newRole->getSystemRoles()));

        // Check if other roles can be added.
        $newRole->setSystemRoles(['ROLE_VIEW_SOMETHING']);
        $this->assertContains('ROLE_VIEW_DASHBOARD', $newRole->getSystemRoles());
        $this->assertContains('ROLE_VIEW_SOMETHING', $newRole->getSystemRoles());
        $this->assertEquals(2, count($newRole->getSystemRoles()));
    }


    /**
     * Check if translations for the status table work correctly
     */
    public function testTranslation(): void
    {
        /** @var UserRole $userRole */
        $userRole = $this->getFixtureReference('user_role_role_system_administrator');

        $this->assertEquals("System administrator", $userRole->getUserRole());
        $this->assertEquals("System administrator", $userRole);
        $this->assertEquals("System administrator", $userRole->translate('en')->getUserRoleTranslated());
        $this->assertEquals("Systeemadministrator", $userRole->translate('nl')->getUserRoleTranslated());
        $this->assertEquals("Systemadministrator", $userRole->translate('de')->getUserRoleTranslated());
    }


    /**
     * Test if a default translation is created when a new object is created without one.
     */
    public function testDefaultTranslation(): void
    {
        $userRole = new UserRole();
        $userRole->setUserRole('Test role');
        $userRole->setDefaultRole(false);
        $this->getEntityManager()->persist($userRole);
        $this->getEntityManager()->flush();
        $id = $userRole->getId();
        $this->assertEquals('Test role', $userRole->getUserRole());

        // Check if the translations are persisted correctly when reloading the object from the database
        $this->getEntityManager()->clear();
        $newType = $this->getEntityManager()->getRepository(Status::class)->findOneBy(['id' => $id]);
        $this->assertEquals('Test role', $userRole->getUserRole());
    }


    /**
     * Test the createFindAllOrderedQueryBuilder method
     */
    public function testCreateFindAllOrderedQueryBuilder(): void
    {
        $userRoles = $this->getEntityManager()->getRepository(UserRole::class)
            ->createFindAllOrderedQueryBuilder('en')
            ->getQuery()->execute();

        /** @var UserRole[] $userRoles */
        $this->assertCount(6, $userRoles);
        $this->assertEquals('Member administrator', $userRoles[0]->getUserRole());
        $this->assertEquals('Serial letter author', $userRoles[1]->getUserRole());
        $this->assertEquals('Serial letter signer', $userRoles[2]->getUserRole());
        $this->assertEquals('System administrator', $userRoles[3]->getUserRole());
        $this->assertEquals('Treasurer', $userRoles[4]->getUserRole());
        $this->assertEquals('User', $userRoles[5]->getUserRole());
    }


    /**
     * Test ordering by role count
     */
    public function testFindAllOrderByRoleCount(): void
    {
        // First add a new role which has the same system roles as the serial letter signer role
        $newRole = new UserRole();
        $newRole->setUserRole('Serial letter signer');
        $newRole->setDefaultRole(false);
        /** @var UserRole $letterSignerRole */
        $letterSignerRole = $this->getFixtureReference('user_role_role_serial_letter_signer');
        $newRole->setSystemRoles($letterSignerRole->getSystemRoles());
        $newRole->translate('en')->setUserRoleTranslated('New role');
        // Ensure that the German translation is ordered BEHIND "Serial Letter signer"
        $newRole->translate('de')->setUserRoleTranslated('X New role');
        $this->getEntityManager()->persist($newRole);
        $this->getEntityManager()->flush();

        /** @var UserRole[] $userRoles */
        $userRoles = $this->getEntityManager()->getRepository(UserRole::class)->findAllOrderedByRoleCount('en');

        $expectedRoles = ['User', 'New role', 'Serial letter signer', 'Serial letter author', 'Member administrator', 'Treasurer', 'System administrator'];

        $this->assertCount(count($expectedRoles), $userRoles);
        for ($i = 0; $i < count($expectedRoles); $i++) {
            $this->assertEquals($expectedRoles[$i], $userRoles[$i]->getUserRole(), "Index $i");
        }

        $userRoles = $this->getEntityManager()->getRepository(UserRole::class)->findAllOrderedByRoleCount('de');

        // In the German translation, new role is behind serial letter signer
        $expectedRoles[1] = 'Serial letter signer';
        $expectedRoles[2] = 'New role';

        $this->assertCount(count($expectedRoles), $userRoles);
        for ($i = 0; $i < count($expectedRoles); $i++) {
            $this->assertEquals($expectedRoles[$i], $userRoles[$i]->getUserRole(), "Index $i");
        }
    }
}
