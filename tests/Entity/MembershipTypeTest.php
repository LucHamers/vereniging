<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\MembershipType;
use App\Service\LogEventsDisabler;
use App\Tests\TestCase;

/**
 * Class MailingListTest
 */
class MembershipTypeTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->getContainer()->get(LogEventsDisabler::class)->disableLifecycleEvents();
        $this->loadFixtures([]);
    }


    /**
     * Test is the default flags are set correctly when the first entry is added
     */
    public function testSettingDefaultMembershipTypesOnFirstCreate(): void
    {
        // Add the first entry to the list. Both default flags must be set on flush.
        $this->createMembershipType("new type 1", false, false);
        $membershipTypes = $this->getEntityManager()->getRepository(MembershipType::class)->findAll();
        $this->assertEquals(1, count($membershipTypes));
        $this->assertTrue($membershipTypes[0]->getDefaultNewMembershipType());
        $this->assertTrue($membershipTypes[0]->getDefaultGroupMembershipType());
    }


    /**
     * Check if adding new membership types preserves the one flag per default type.
     */
    public function testSettingDefaultMembershipTypesOnAdditionalCreates(): void
    {
        // Add first membership type with both flags set.
        $this->createMembershipType("new type 1", true, true);

        // Add membership of which no flags are set. The flags of the first object should remain the same.
        $this->createMembershipType("new type 2", false, false);
        $membershipTypes = $this->getEntityManager()->getRepository(MembershipType::class)->findAll();
        $this->assertEquals(2, count($membershipTypes));
        $this->assertTrue($membershipTypes[0]->getDefaultNewMembershipType());
        $this->assertTrue($membershipTypes[0]->getDefaultGroupMembershipType());
        $this->assertFalse($membershipTypes[1]->getDefaultNewMembershipType());
        $this->assertFalse($membershipTypes[1]->getDefaultGroupMembershipType());

        // Add membership where the default group membership type flag is set. The new object should keep this flag, the
        // first object in the list should have a cleared flag.
        $this->createMembershipType("new type 3", true, false);
        $membershipTypes = $this->getEntityManager()->getRepository(MembershipType::class)->findAll();
        $this->assertEquals(3, count($membershipTypes));
        $this->assertTrue($membershipTypes[0]->getDefaultNewMembershipType());
        $this->assertFalse($membershipTypes[0]->getDefaultGroupMembershipType());
        $this->assertFalse($membershipTypes[1]->getDefaultNewMembershipType());
        $this->assertFalse($membershipTypes[1]->getDefaultGroupMembershipType());
        $this->assertFalse($membershipTypes[2]->getDefaultNewMembershipType());
        $this->assertTrue($membershipTypes[2]->getDefaultGroupMembershipType());

        // Add membership where the default new membership type flag is set. The new object should keep this flag, the
        // first object in the list should have a cleared flag.
        $this->createMembershipType("new type 4", false, true);
        $membershipTypes = $this->getEntityManager()->getRepository(MembershipType::class)->findAll();
        $this->assertEquals(4, count($membershipTypes));
        $this->assertFalse($membershipTypes[0]->getDefaultNewMembershipType());
        $this->assertFalse($membershipTypes[0]->getDefaultGroupMembershipType());
        $this->assertFalse($membershipTypes[1]->getDefaultNewMembershipType());
        $this->assertFalse($membershipTypes[1]->getDefaultGroupMembershipType());
        $this->assertFalse($membershipTypes[2]->getDefaultNewMembershipType());
        $this->assertTrue($membershipTypes[2]->getDefaultGroupMembershipType());
        $this->assertTrue($membershipTypes[3]->getDefaultNewMembershipType());
        $this->assertFalse($membershipTypes[3]->getDefaultGroupMembershipType());
    }


    /**
     * Check if changing existing membership types preserves the one flag per default type.
     */
    public function testSettingDefaultMembershipTypesOnUpdating(): void
    {
        $this->createMembershipType("new type 1", false, false);
        $this->createMembershipType("new type 2", false, false);
        $this->createMembershipType("new type 3", true, false);
        $this->createMembershipType("new type 4", false, true);
        $membershipTypes = $this->getEntityManager()->getRepository(MembershipType::class)->findAll();
        $this->assertEquals(4, count($membershipTypes));

        // Set default group membership type on the first type. This should remove the flag on the third one
        $membershipTypes[0]->setDefaultGroupMembershipType(true);
        $this->getEntityManager()->flush();
        $this->getEntityManager()->refresh($membershipTypes[0]);
        $this->assertFalse($membershipTypes[0]->getDefaultNewMembershipType());
        $this->assertTrue($membershipTypes[0]->getDefaultGroupMembershipType());
        $this->assertFalse($membershipTypes[1]->getDefaultNewMembershipType());
        $this->assertFalse($membershipTypes[1]->getDefaultGroupMembershipType());
        $this->assertFalse($membershipTypes[2]->getDefaultNewMembershipType());
        $this->assertFalse($membershipTypes[2]->getDefaultGroupMembershipType());
        $this->assertTrue($membershipTypes[3]->getDefaultNewMembershipType());
        $this->assertFalse($membershipTypes[3]->getDefaultGroupMembershipType());

        // Set default new membership type on the second type. This should remove the flag on the fourth one
        $membershipTypes[1]->setDefaultNewMembershipType(true);
        $this->getEntityManager()->flush();
        $this->getEntityManager()->refresh($membershipTypes[1]);
        $this->assertFalse($membershipTypes[0]->getDefaultNewMembershipType());
        $this->assertTrue($membershipTypes[0]->getDefaultGroupMembershipType());
        $this->assertTrue($membershipTypes[1]->getDefaultNewMembershipType());
        $this->assertFalse($membershipTypes[1]->getDefaultGroupMembershipType());
        $this->assertFalse($membershipTypes[2]->getDefaultNewMembershipType());
        $this->assertFalse($membershipTypes[2]->getDefaultGroupMembershipType());
        $this->assertFalse($membershipTypes[3]->getDefaultNewMembershipType());
        $this->assertFalse($membershipTypes[3]->getDefaultGroupMembershipType());
    }


    /**
     * Check if removing membership types preserves the one flag per default type.
     */
    public function testSettingDefaultMembershipTypesOnRemove(): void
    {
        $this->createMembershipType("new type 1", true, false);
        $this->createMembershipType("new type 2", false, true);
        $this->createMembershipType("new type 3", false, false);
        $this->createMembershipType("new type 4", false, false);

        $membershipTypes = $this->getEntityManager()->getRepository(MembershipType::class)->findAll();
        $this->assertEquals(4, count($membershipTypes));

        // Remove a membership type in which non of the default flags is set. This should not change any of the other
        // entities.
        $this->getEntityManager()->remove($membershipTypes[2]);
        $this->getEntityManager()->flush();
        $this->getEntityManager()->refresh($membershipTypes[0]);
        $this->getEntityManager()->refresh($membershipTypes[1]);
        $this->getEntityManager()->refresh($membershipTypes[3]);
        $membershipTypes = $this->getEntityManager()->getRepository(MembershipType::class)->findAll();
        $this->assertEquals(3, count($membershipTypes));
        $this->assertFalse($membershipTypes[0]->getDefaultNewMembershipType());
        $this->assertTrue($membershipTypes[0]->getDefaultGroupMembershipType());
        $this->assertTrue($membershipTypes[1]->getDefaultNewMembershipType());
        $this->assertFalse($membershipTypes[1]->getDefaultGroupMembershipType());
        $this->assertFalse($membershipTypes[2]->getDefaultNewMembershipType());
        $this->assertFalse($membershipTypes[2]->getDefaultGroupMembershipType());

        // Remove a membership type in which the default group membership type is set. This should move the flag to the
        // first entry in the list.
        $this->getEntityManager()->remove($membershipTypes[1]);
        $this->getEntityManager()->flush();
        $this->getEntityManager()->refresh($membershipTypes[0]);
        $this->getEntityManager()->refresh($membershipTypes[2]);
        $membershipTypes = $this->getEntityManager()->getRepository(MembershipType::class)->findAll();
        $this->assertEquals(2, count($membershipTypes));
        $this->assertTrue($membershipTypes[0]->getDefaultNewMembershipType());
        $this->assertTrue($membershipTypes[0]->getDefaultGroupMembershipType());
        $this->assertFalse($membershipTypes[1]->getDefaultNewMembershipType());
        $this->assertFalse($membershipTypes[1]->getDefaultGroupMembershipType());

        // Remove a membership type in which both default membership types are set. This should move the default new
        // membership flag to the first entry in the list, but not the default group membership type, because the
        // remaining membership type is no group membership type.
        $this->getEntityManager()->remove($membershipTypes[0]);
        $this->getEntityManager()->flush();
        $this->getEntityManager()->refresh($membershipTypes[1]);
        $membershipTypes = $this->getEntityManager()->getRepository(MembershipType::class)->findAll();
        $this->assertEquals(1, count($membershipTypes));
        $this->assertTrue($membershipTypes[0]->getDefaultNewMembershipType());
        $this->assertFalse($membershipTypes[0]->getDefaultGroupMembershipType());

        // Remove the last membership type from the list. This should do nothing.
        $this->getEntityManager()->remove($membershipTypes[0]);
        $this->getEntityManager()->flush();
        $membershipTypes = $this->getEntityManager()->getRepository(MembershipType::class)->findAll();
        $this->assertEquals(0, count($membershipTypes));
    }


    /**
     * There was a bug when adding multiple new entries at the same time. This test checks if this is fixed.
     */
    public function testSettingDefaultMembershipTypesOnMultipleFirstCreateCorrectSettings(): void
    {
        // Add the first entry to the list. Both default flags must be set on flush.
        $membershipType1 = $this->createMembershipType("new type 1", false, false, false);
        $membershipType2 = $this->createMembershipType("new type 2", true, false, false);
        $membershipType3 = $this->createMembershipType("new type 3", false, true, false);
        $this->getEntityManager()->persist($membershipType1);
        $this->getEntityManager()->persist($membershipType2);
        $this->getEntityManager()->persist($membershipType3);
        $this->getEntityManager()->flush();
        $membershipTypes = $this->getEntityManager()->getRepository(MembershipType::class)->findAll();
        $this->assertEquals(3, count($membershipTypes));
        $this->assertFalse($membershipTypes[0]->getDefaultGroupMembershipType());
        $this->assertFalse($membershipTypes[0]->getDefaultNewMembershipType());
        $this->assertTrue($membershipTypes[1]->getDefaultGroupMembershipType());
        $this->assertFalse($membershipTypes[1]->getDefaultNewMembershipType());
        $this->assertfalse($membershipTypes[2]->getDefaultGroupMembershipType());
        $this->assertTrue($membershipTypes[2]->getDefaultNewMembershipType());
    }


    /**
     * There was a bug when adding multiple new entries at the same time. This test checks if this is fixed.
     */
    public function testSettingDefaultMembershipTypesOnMultipleFirstCreateDoubleSettings(): void
    {
        // Add the first entry to the list. Both default flags must be set on flush.
        $this->createMembershipType("new type 1", false, false);
        $this->createMembershipType("new type 2", true, false);
        $this->createMembershipType("new type 3", false, true);
        $membershipType1 = $this->createMembershipType("new type 4", false, false, false);
        $membershipType2 = $this->createMembershipType("new type 5", true, false, false);
        $membershipType3 = $this->createMembershipType("new type 6", true, true, false);
        $this->getEntityManager()->persist($membershipType1);
        $this->getEntityManager()->persist($membershipType2);
        $this->getEntityManager()->persist($membershipType3);
        $this->getEntityManager()->flush();
        $membershipTypes = $this->getEntityManager()->getRepository(MembershipType::class)->findAll();
        $this->assertEquals(6, count($membershipTypes));
        $this->assertFalse($membershipTypes[3]->getDefaultGroupMembershipType());
        $this->assertFalse($membershipTypes[3]->getDefaultNewMembershipType());
        $this->assertFalse($membershipTypes[4]->getDefaultGroupMembershipType());
        $this->assertFalse($membershipTypes[4]->getDefaultNewMembershipType());
        $this->assertTrue($membershipTypes[5]->getDefaultGroupMembershipType());
        $this->assertTrue($membershipTypes[5]->getDefaultNewMembershipType());
    }


    /**
     * Check if translations for the phone number type table work correctly
     */
    public function testTranslation(): void
    {
        $this->loadAllFixtures();

        $membershipType = $this->getFixtureReference('membership_type_regular');

        $this->assertEquals("Regular member", $membershipType->getType());
        $this->assertEquals("Regular member", $membershipType);
        $this->assertEquals("Regular member", $membershipType->translate('en')->getTypeTranslated());
        $this->assertEquals("Regulier lid", $membershipType->translate('nl')->getTypeTranslated());
        $this->assertEquals("Reguläres Mitglied", $membershipType->translate('de')->getTypeTranslated());
    }


    /**
    * Test the createFindByGroupMembershipQueryBuilder method
    */
    public function testCreateFindByGroupMembershipQueryBuilder(): void
    {
        $this->loadAllFixtures();

        // Add company membership type to test ordering in different langauges
        $companyMembership = $this->createMembershipType('Compnay member', false, false, false);
        $companyMembership->translate('en')->setTypeTranslated('Company member');
        $companyMembership->translate('nl')->setTypeTranslated('Bedrijfslid');
        $companyMembership->translate('de')->setTypeTranslated('Firmenmitglied');
        $companyMembership->setGroupMembership(true);
        $this->getEntityManager()->persist($companyMembership);
        $this->getEntityManager()->flush();

        // Test the order for English translations
        /** @var MembershipType[] $membershipTypes */
        $membershipTypes = $this->getEntityManager()->getRepository(MembershipType::class)
                                               ->createFindByGroupMembershipQueryBuilder('en', false)
                                               ->getQuery()->getResult();

        $this->assertCount(5, $membershipTypes);
        $this->assertEquals('Company member', $membershipTypes[0]->getType());
        $this->assertEquals('Family member', $membershipTypes[1]->getType());
        $this->assertEquals('Group member', $membershipTypes[2]->getType());
        $this->assertEquals('Regular member', $membershipTypes[3]->getType());
        $this->assertEquals('Student member', $membershipTypes[4]->getType());

        // Test the order for German translations
        /** @var MembershipType[] $membershipTypes */
        $membershipTypes = $this->getEntityManager()->getRepository(MembershipType::class)
                                ->createFindByGroupMembershipQueryBuilder('de', false)
                                ->getQuery()->getResult();

        $this->assertCount(5, $membershipTypes);
        $this->assertEquals('Familienmitglied', $membershipTypes[0]->translate('de')->getTypeTranslated());
        $this->assertEquals('Firmenmitglied', $membershipTypes[1]->translate('de')->getTypeTranslated());
        $this->assertEquals('Gruppenmitglied', $membershipTypes[2]->translate('de')->getTypeTranslated());
        $this->assertEquals('Reguläres Mitglied', $membershipTypes[3]->translate('de')->getTypeTranslated());
        $this->assertEquals('Studentisches Mitglied', $membershipTypes[4]->translate('de')->getTypeTranslated());

        // Test group membership types only, for English translations
        /** @var MembershipType[] $membershipTypes */
        $membershipTypes = $this->getEntityManager()->getRepository(MembershipType::class)
                                ->createFindByGroupMembershipQueryBuilder('en', true)
                                ->getQuery()->getResult();

        $this->assertCount(3, $membershipTypes);
        $this->assertEquals('Company member', $membershipTypes[0]->getType());
        $this->assertEquals('Family member', $membershipTypes[1]->getType());
        $this->assertEquals('Group member', $membershipTypes[2]->getType());

        // Test group membership types only, for English translations
        /** @var MembershipType[] $membershipTypes */
        $membershipTypes = $this->getEntityManager()->getRepository(MembershipType::class)
                                ->createFindByGroupMembershipQueryBuilder('de', true)
                                ->getQuery()->getResult();

        $this->assertCount(3, $membershipTypes);

        $this->assertEquals('Familienmitglied', $membershipTypes[0]->translate('de')->getTypeTranslated());
        $this->assertEquals('Firmenmitglied', $membershipTypes[1]->translate('de')->getTypeTranslated());
        $this->assertEquals('Gruppenmitglied', $membershipTypes[2]->translate('de')->getTypeTranslated());
    }


    /**
     * Test the findByGroupMembershipAsJson method
     */
    public function testFindByGroupMembershipAsJson(): void
    {
        $this->loadAllFixtures();

        // Test the method for English translations
        $membershipTypesJson = $this->getEntityManager()->getRepository(MembershipType::class)
                                               ->findByGroupMembershipAsJson('en', false);
        $this->assertJson($membershipTypesJson);
        $membershipTypes = json_decode($membershipTypesJson, true);

        $this->assertCount(4, $membershipTypes);

        $this->assertEquals('Family member', $membershipTypes[0]['type']);
        $this->assertGreaterThan(0, $membershipTypes[0]['id']);
        $this->assertTrue($membershipTypes[0]['isDefaultGroupMembershipType']);
        $this->assertFalse($membershipTypes[0]['isDefaultNewMembershipType']);
        $this->asserttrue($membershipTypes[0]['isGroupMembershipType']);

        $membershipTypesJson = $this->getEntityManager()->getRepository(MembershipType::class)
                                    ->findByGroupMembershipAsJson('de', true);
        $this->assertJson($membershipTypesJson);
        $membershipTypes = json_decode($membershipTypesJson, true);

        $this->assertCount(2, $membershipTypes);
        $this->assertEquals('Gruppenmitglied', $membershipTypes[1]['type']);
        $this->assertGreaterThan(0, $membershipTypes[1]['id']);
        $this->assertFalse($membershipTypes[1]['isDefaultGroupMembershipType']);
        $this->assertFalse($membershipTypes[1]['isDefaultNewMembershipType']);
        $this->asserttrue($membershipTypes[1]['isGroupMembershipType']);
    }


    /**
     * Test if a default translation is created when a new object is created without one.
     */
    public function testDefaultTranslation(): void
    {
        $membershipType = $this->createMembershipType('Testtype', false, false);
        $id = $membershipType->getId();
        $this->assertEquals('Testtype', $membershipType->getType());

        // Check if the translations are persisted correctly when reloading the object from the database
        $this->getEntityManager()->clear();
        $newType = $this->getEntityManager()->getRepository(MembershipType::class)->findOneBy(['id' => $id]);
        $this->assertEquals('Testtype', $newType->getType());
    }


    /**
     * Test the inUse method.
     */
    public function testIsInUse(): void
    {
        $this->loadAllFixtures();

        $type = $this->getFixtureReference('membership_type_regular');
        $this->assertTrue($type->isInUse());
        $type = $this->getFixtureReference('membership_type_student');
        $this->assertFalse($type->isInUse());
        $type = $this->getFixtureReference('membership_type_family');
        $this->assertTrue($type->isInUse());
        // The following test ensures that inUse also works when the object has no initialised member entries (test of a
        // bugfix)
        $type = new MembershipType();
        $this->assertFalse($type->isInUse());
    }


    /**
     * Create a new membership type
     *
     * @param string $typeName
     * @param bool   $defaultGroupMembershipType
     * @param bool   $defaultNewMembershipType
     * @param bool   $save
     *
     * @return MembershipType
     */
    private function createMembershipType(string $typeName, bool $defaultGroupMembershipType, bool $defaultNewMembershipType, bool $save = true): MembershipType
    {
        $membershipType = new MembershipType();
        $membershipType->setType($typeName);
        $membershipType->setMembershipFee(0);
        $membershipType->setGroupMembership(false);
        $membershipType->setDefaultGroupMembershipType($defaultGroupMembershipType);
        $membershipType->setDefaultNewMembershipType($defaultNewMembershipType);
        if ($save) {
            $this->getEntityManager()->persist($membershipType);
            $this->getEntityManager()->flush();
            // This is needed because flush can change the flags
            $this->getEntityManager()->refresh($membershipType);
        }

        return $membershipType;
    }
}
