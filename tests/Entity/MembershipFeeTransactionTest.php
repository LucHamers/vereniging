<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\MembershipFeeTransaction;
use App\Tests\TestCase;
use DateTime;
use DateTimeImmutable;

/**
 * Class MembershipFeeTransactionTest
 */
class MembershipFeeTransactionTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->setLoggedInUserForLogging();
    }


    /**
     * Test if the overloaded method findAll sorts the entries correctly
     */
    public function testFindAllByMember(): void
    {
        /** @var MembershipFeeTransaction[] $entries */
        $entries = $this->getEntityManager()->getRepository(MembershipFeeTransaction::class)->findAllByMember($this->getFixtureReference('member_entry_last_name1'));
        $this->assertCount(3, $entries);
        // The newest entry has to be on top
        $this->assertLessThan((new DateTimeImmutable("now"))->modify("- 1 day"), $entries[0]->getCreateDate());
        $this->assertLessThan((new DateTimeImmutable("now"))->modify("- 2 day"), $entries[1]->getCreateDate());
        $this->assertLessThan((new DateTimeImmutable("now"))->modify("- 30 day"), $entries[2]->getCreateDate());
        $this->assertNull($entries[0]->getCloseDate());
        $this->assertLessThan((new DateTimeImmutable("now"))->modify("- 20 day"), $entries[2]->getCloseDate());

        // Check that we get the same data for another group member
        $entries = $this->getEntityManager()->getRepository(MembershipFeeTransaction::class)->findAllByMember($this->getFixtureReference('member_entry_last_name12'));
        $this->assertCount(3, $entries);
        // The newest entry has to be on top
        $this->assertLessThan((new DateTimeImmutable("now"))->modify("- 1 day"), $entries[0]->getCreateDate());
        $this->assertLessThan((new DateTimeImmutable("now"))->modify("- 2 day"), $entries[1]->getCreateDate());
        $this->assertLessThan((new DateTimeImmutable("now"))->modify("- 30 day"), $entries[2]->getCreateDate());
        $this->assertNull($entries[0]->getCloseDate());
        $this->assertLessThan((new DateTimeImmutable("now"))->modify("- 20 day"), $entries[2]->getCloseDate());

        // Check that we get an empty list when loading from a member without transactions
        $entries = $this->getEntityManager()->getRepository(MembershipFeeTransaction::class)->findAllByMember($this->getFixtureReference('member_entry_last_name5'));
        $this->assertCount(0, $entries);

        // Check that we get an empty list when loading from a member without a membership number
        $entries = $this->getEntityManager()->getRepository(MembershipFeeTransaction::class)->findAllByMember($this->getFixtureReference('member_entry_last_name4'));
        $this->assertCount(0, $entries);
    }


    /**
     * Test the toArray method
     */
    public function testToArray(): void
    {
        $fee = new MembershipFeeTransaction();
        $fee->setCreateDate(new DateTime('2019-01-01'));
        $fee->setCloseDate(new DateTime('2020-02-02'));
        $fee->setAmount(12.34);

        $data = $fee->toArray();
        $this->assertEquals('Jan 1, 2019', $data['createDate']);
        $this->assertEquals('Feb 2, 2020', $data['closeDate']);
        $this->assertNull($data['membershipNumber']);
        $this->assertEqualsWithDelta(12.34, $data['amount'], 0.001);

        $fee->setMembershipNumber($this->getFixtureReference('membership_number_3'));
        $data = $fee->toArray();
        $this->assertEquals(3, $data['membershipNumber']->getMembershipNumber());
    }


    /**
     * Test reading the statistics for membership fees
     */
    public function testGetMembershipFeeStatistics(): void
    {
        // First add some more transactions to make it more interesting
        $membershipNumber = $this->getFixtureReference('membership_number_1');
        $membershipFee1 = new MembershipFeeTransaction();
        $membershipFee1->setMembershipNumber($membershipNumber);
        // Use an amount with cents to check if the calculation uses floats and rounds correctly
        $membershipFee1->setAmount(12.23);
        $membershipFee1->setCreateDate(new DateTime('2019-01-01'));
        // Close in next year. The value should be added to the 2019 fees
        $membershipFee1->setCloseDate(new DateTime('2020-02-01'));

        // Add an open transaction in 2019
        $membershipFee2 = new MembershipFeeTransaction();
        $membershipFee2->setMembershipNumber($membershipNumber);
        $membershipFee2->setAmount(24);
        $membershipFee2->setCreateDate(new DateTime('2019-12-31'));

        $this->getEntityManager()->persist($membershipFee1);
        $this->getEntityManager()->persist($membershipFee2);
        $this->getEntityManager()->flush();

        $repo = $this->getEntityManager()->getRepository(MembershipFeeTransaction::class);

        $result = $repo->getMembershipFeeStatistics();

        $expected = [
            '2020' => ['expected' => 192.0, 'paid' => 60.0, 'outstanding' => 132.0],
            '2019' => ['expected' => 36.23, 'paid' => 12.23, 'outstanding' => 24.0],
        ];
        $this->assertEquals($expected, $result);
    }
}
