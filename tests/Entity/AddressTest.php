<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\Address;
use App\Tests\TestCase;

/**
 * Test for the address type entity.
 *
 */
class AddressTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test toArray method.
     */
    public function testToArray(): void
    {
        $address1 = $this->getEntityManager()->getRepository(Address::class)->findOneBy(['address' => "MyAddress1"]);
        $address2 = $this->getEntityManager()->getRepository(Address::class)->findOneBy(['address' => "MyAddress2"]);
        $addressArray1 = $address1->toArray();
        $addressArray2 = $address2->toArray();
        $this->assertArrayHasKey("address", $addressArray1);
        $this->assertArrayHasKey("address", $addressArray2);
        $this->assertArrayHasKey("zip", $addressArray1);
        $this->assertArrayHasKey("zip", $addressArray2);
        $this->assertArrayHasKey("city", $addressArray1);
        $this->assertArrayHasKey("city", $addressArray2);
        $this->assertArrayHasKey("country", $addressArray1);
        $this->assertArrayHasKey("country", $addressArray2);
        $this->assertArrayHasKey("type", $addressArray1);
        $this->assertArrayHasKey("type", $addressArray2);

        $this->assertEquals("MyAddress1", $addressArray1["address"]);
        $this->assertEquals("MyAddress2", $addressArray2["address"]);
        $this->assertEquals("MyZip1", $addressArray1["zip"]);
        $this->assertEquals("MyZip2", $addressArray2["zip"]);
        $this->assertEquals("MyCity1", $addressArray1["city"]);
        $this->assertEquals("MyCity2", $addressArray2["city"]);
        $this->assertEquals("Germany", $addressArray1["country"]);
        $this->assertEquals("Netherlands", $addressArray2["country"]);
        $this->assertEquals("Home address", $addressArray1["type"]);
        $this->assertEquals("Company address", $addressArray2["type"]);
    }
}
