<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\SerialLetterTemplate;
use App\Tests\TestCase;

/**
 * Class SerialLetterTemplateTest
 */
class SerialLetterTemplateTest extends TestCase
{
    /**
     * Test the method getAllAsArray
     */
    public function testGetAllAsArray(): void
    {
        $templates = $this->getEntityManager()->getRepository(SerialLetterTemplate::class)->findAllAsArray();
        $this->assertCount(3, $templates);
        $this->assertCount(3, $templates['en']);
        $this->assertCount(3, $templates['de']);
        $this->assertCount(3, $templates['nl']);
        $this->assertEquals('Activity list', $templates['en'][0]->getTemplateName());
        $this->assertEquals('Day program', $templates['en'][1]->getTemplateName());
        $this->assertEquals('Invitation', $templates['en'][2]->getTemplateName());
        $this->assertEquals('Einladung', $templates['de'][0]->getTemplateName());
        $this->assertEquals('Tagesprogramm', $templates['de'][1]->getTemplateName());
        $this->assertEquals('Veranstaltungsliste', $templates['de'][2]->getTemplateName());
    }
}
