<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\AddressType;
use App\Entity\AddressTypeTranslation;
use App\Tests\TestCase;

/**
 * Test for the address type entity.
 *
 */
class AddressTypeTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->setLoggedInUserForLogging();
    }


    /**
     * Test the inUse method.
     */
    public function testIsInUse(): void
    {
        $addressType = $this->getEntityManager()->getRepository(AddressType::class)
                                                ->findOneBy(['addressType' => "Home address"]);
        $this->assertTrue($addressType->isInUse());
        $addressType = $this->getEntityManager()->getRepository(AddressType::class)
                                                ->findOneBy(['addressType' => "Company address"]);
        $this->assertTrue($addressType->isInUse());
    }


    /**
     * Test if the findAll method in the repository class has been overloaded to return the values ordered ascending.
     */
    public function testFindAll(): void
    {
        $addressTypes = $this->getEntityManager()->getRepository(AddressType::class)->findAll();
        $this->assertEquals("Company address", $addressTypes[0]->getAddressType());
        $this->assertEquals("Home address", $addressTypes[1]->getAddressType());
    }


    /**
     * Test the createOrderByTypeQueryBuilder method in the repository class
     */
    public function testCreateOrderByTypeQueryBuilder(): void
    {
        // Test with the default locale
        /** @var AddressType[] $addressTypes */
        $addressTypes = $this->getEntityManager()->getRepository(AddressType::class)
                             ->createOrderByTypeQueryBuilder()->getQuery()->getResult();
        $this->assertEquals("Company address", $addressTypes[0]->getAddressType());
        $this->assertEquals("Home address", $addressTypes[1]->getAddressType());

        // Test with the English locale
        /** @var AddressType[] $addressTypes */
        $addressTypes = $this->getEntityManager()->getRepository(AddressType::class)
                             ->createOrderByTypeQueryBuilder('en')->getQuery()->getResult();
        $this->assertEquals("Company address", $addressTypes[0]->getAddressType());
        $this->assertEquals("Home address", $addressTypes[1]->getAddressType());

        // Test with the Dutch locale --> the values still have to be in alphabetical order, in Dutch! Change the Dutch
        // translation for company address to test this.
        $translation = $this->getEntityManager()->getRepository(AddressTypeTranslation::class)
                                                ->findOneBy(['addressTypeTranslated' => 'Firma-adres']);
        $translation->setAddressTypeTranslated('Z-bedrijfsadress');
        $this->getEntityManager()->flush();
        $addressTypes = $this->getEntityManager()->getRepository(AddressType::class)
                             ->createOrderByTypeQueryBuilder('nl')->getQuery()->getResult();
        $this->assertEquals("Home address", $addressTypes[0]->getAddressType());
        $this->assertEquals("Company address", $addressTypes[1]->getAddressType());
    }


    /**
     * Test for checking if the use company name flag is stored and read correctly.
     */
    public function testUserCompanyName(): void
    {
        $addressType = new AddressType();
        $addressType->setUseCompanyName(false);
        $this->assertFalse($addressType->getUseCompanyName());
        $addressType->setUseCompanyName(true);
        $this->assertTrue($addressType->getUseCompanyName());
    }


    /**
     * Test if the id is set correctly
     */
    public function testGetId(): void
    {
        $addressType = new AddressType();
        $this->assertEquals(null, $addressType->getId());
        $addressType->setAddressType("test type");
        $addressType->setUseCompanyName(false);
        $this->getEntityManager()->persist($addressType);
        $this->getEntityManager()->flush();
        $this->assertNotNull($addressType->getId());
    }


    /**
     * Check if translations for the phone number type table work correctly
     */
    public function testTranslation(): void
    {
        $addressType = $this->getEntityManager()->getRepository(AddressType::class)
                                           ->findOneBy(['addressType' => "Home address"]);

        $this->assertEquals("Home address", $addressType->getAddressType());
        $this->assertEquals("Home address", $addressType);
        $this->assertEquals("Home address", $addressType->translate('en')->getAddressTypeTranslated());
        $this->assertEquals("Thuisadres", $addressType->translate('nl')->getAddressTypeTranslated());
        $this->assertEquals("Privatadresse", $addressType->translate('de')->getAddressTypeTranslated());

        $addressType->addNewTranslation('es', 'Home address in Spanish');
        $this->getEntityManager()->flush();
        $this->getEntityManager()->refresh($addressType);
        $this->assertEquals('Home address in Spanish', $addressType->translate('es')->getAddressTypeTranslated());
    }


    /**
     * Test if a default translation is created when a new object is created without one.
     */
    public function testDefaultTranslation(): void
    {
        $addressType = new AddressType();
        $addressType->setAddressType('Testtype');
        $addressType->setUseCompanyName(false);
        $this->getEntityManager()->persist($addressType);
        $this->getEntityManager()->flush();
        $id = $addressType->getId();
        $this->assertEquals('Testtype', $addressType->getAddressType());

        // Check if the translations are persisted correctly when reloading the object from the database
        $this->getEntityManager()->clear();
        $newType = $this->getEntityManager()->getRepository(AddressType::class)->findOneBy(['id' => $id]);
        $this->assertEquals('Testtype', $newType->getAddressType());
    }
}
