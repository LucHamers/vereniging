<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Entity;

use App\Entity\PhoneNumberType;
use App\Tests\TestCase;

/**
 * Class PhoneNumberTypeTest
 */
class PhoneNumberTypeTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->setLoggedInUserForLogging();
    }


    /**
     * Test the inUse method.
     */
    public function testIsInUse(): void
    {
        $phoneNumberType = $this->getFixtureReference('phone_number_type_home');
        $this->assertTrue($phoneNumberType->isInUse());
        $phoneNumberType = $this->getFixtureReference('phone_number_type_company_mobile');
        $this->assertFalse($phoneNumberType->isInUse());
    }


    /**
     * Test if the findAll method in the repository class has been overloaded to return the values ordered ascending.
     */
    public function testFindAll(): void
    {
        $phoneNumberTypes = $this->getEntityManager()->getRepository(PhoneNumberType::class)->findAll();
        $this->assertEquals("Company", $phoneNumberTypes[0]->getPhoneNumberType());
        $this->assertEquals("Company mobile", $phoneNumberTypes[1]->getPhoneNumberType());
        $this->assertEquals("Home", $phoneNumberTypes[2]->getPhoneNumberType());
        $this->assertEquals("Mobile", $phoneNumberTypes[3]->getPhoneNumberType());
        $this->assertGreaterThan(0, $phoneNumberTypes[0]->getId());
        $this->assertGreaterThan(0, $phoneNumberTypes[1]->getId());
        $this->assertGreaterThan(0, $phoneNumberTypes[2]->getId());
        $this->assertGreaterThan(0, $phoneNumberTypes[3]->getId());
    }


    /**
     * Test the createOrderByTypeQueryBuilder method in the repository class
     */
    public function testCreateOrderByTypeQueryBuilder(): void
    {
        // Test with the default locale
        /** @var PhoneNumberType[] $phoneNumberTypes */
        $phoneNumberTypes = $this->getEntityManager()->getRepository(PhoneNumberType::class)
                                                ->createOrderByTypeQueryBuilder()
                                                ->getQuery()->getResult();
        $this->assertEquals("Company", $phoneNumberTypes[0]->getPhoneNumberType());
        $this->assertEquals("Company mobile", $phoneNumberTypes[1]->getPhoneNumberType());
        $this->assertEquals("Home", $phoneNumberTypes[2]->getPhoneNumberType());
        $this->assertEquals("Mobile", $phoneNumberTypes[3]->getPhoneNumberType());

        // Test with the English locale
        /** @var PhoneNumberType[] $phoneNumberTypes */
        $phoneNumberTypes = $this->getEntityManager()->getRepository(PhoneNumberType::class)
                                                ->createOrderByTypeQueryBuilder('en')
                                                ->getQuery()->getResult();
        $this->assertEquals("Company", $phoneNumberTypes[0]->getPhoneNumberType());
        $this->assertEquals("Company mobile", $phoneNumberTypes[1]->getPhoneNumberType());
        $this->assertEquals("Home", $phoneNumberTypes[2]->getPhoneNumberType());
        $this->assertEquals("Mobile", $phoneNumberTypes[3]->getPhoneNumberType());

        // Test with the English locale
        /** @var PhoneNumberType[] $phoneNumberTypes */
        $phoneNumberTypes = $this->getEntityManager()->getRepository(PhoneNumberType::class)
                                                ->createOrderByTypeQueryBuilder('de')
                                                ->getQuery()->getResult();
        $this->assertEquals("Company", $phoneNumberTypes[0]->getPhoneNumberType());
        $this->assertEquals("Company mobile", $phoneNumberTypes[1]->getPhoneNumberType());
        $this->assertEquals("Mobile", $phoneNumberTypes[2]->getPhoneNumberType());
        $this->assertEquals("Home", $phoneNumberTypes[3]->getPhoneNumberType());
    }


    /**
     * Check if translations for the phone number type table work correctly
     */
    public function testTranslation(): void
    {
        $phoneNumberType = $this->getEntityManager()->getRepository(PhoneNumberType::class)
                                               ->findOneBy(['phoneNumberType' => "home"]);

        $this->assertEquals("Home", $phoneNumberType->getPhoneNumberType());
        $this->assertEquals("Home", $phoneNumberType);
        $this->assertEquals("Home", $phoneNumberType->translate('en')->getPhoneNumberTypeTranslated());
        $this->assertEquals("Thuis", $phoneNumberType->translate('nl')->getPhoneNumberTypeTranslated());
        $this->assertEquals("Zuhause", $phoneNumberType->translate('de')->getPhoneNumberTypeTranslated());

        $phoneNumberType->addNewTranslation('es', 'Germany in Spanish');
        $this->getEntityManager()->flush();
        $this->getEntityManager()->refresh($phoneNumberType);
        $this->assertEquals('Germany in Spanish', $phoneNumberType->translate('es')->getPhoneNumberTypeTranslated());
    }


    /**
     * Test if a default translation is created when a new object is created without one.
     */
    public function testDefaultTranslation(): void
    {
        $phoneNumberType = new PhoneNumberType();
        $phoneNumberType->setPhoneNumberType('Testtype');
        $this->getEntityManager()->persist($phoneNumberType);
        $this->getEntityManager()->flush();
        $id = $phoneNumberType->getId();
        $this->assertEquals('Testtype', $phoneNumberType->getPhoneNumberType());

        // Check if the translations are persisted correctly when reloading the object from the database
        $this->getEntityManager()->clear();
        $newType = $this->getEntityManager()->getRepository(PhoneNumberType::class)->findOneBy(['id' => $id]);
        $this->assertEquals('Testtype', $newType->getPhoneNumberType());
    }
}
