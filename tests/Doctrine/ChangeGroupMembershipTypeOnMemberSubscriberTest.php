<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Doctrine;

use App\Entity\LogfileEntry;
use App\Entity\MemberEntry;
use App\Entity\MembershipType;
use App\Tests\TestCase;

/**
 * Class ChangeGroupMembershipTypeOnMemberSubscriberTest
 */
class ChangeGroupMembershipTypeOnMemberSubscriberTest extends TestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->setLoggedInUserForLogging();
        LogfileEntry::prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'en');
    }


    /**
     * Test if the membership type is set for all members of the group
     */
    public function testSetMembershipType(): void
    {
        $this->clearAllLogFiles();

        /** @var MemberEntry $member1 */
        $member1 = $this->getFixtureReference('member_entry_last_name1');
        foreach ($member1->getGroupMembers() as $groupMember) {
            $this->assertEquals('Family member', $groupMember->getMembershipType()->getType());
        }
        $this->assertEquals('Family member', $member1->getMembershipType()->getType());

        /** @var MembershipType $groupMemberType */
        $groupMemberType = $this->getFixtureReference('membership_type_group');
        $member1->setMembershipType($groupMemberType);
        $this->getEntityManager()->flush();

        foreach ($member1->getGroupMembers() as $groupMember) {
            $this->assertEquals('Group member', $groupMember->getMembershipType()->getType());
        }
        $this->assertEquals('Group member', $member1->getMembershipType()->getType());

        $logMessages = $this->getAllLogs();
        $this->assertCount(4, $logMessages);
        $message = 'Updated entry of type member ';
        $change = ["Membership type" => ["Family member", "Group member"]];
        $this->checkComplexLogMessage($logMessages[3], $message, $change, $member1->getId(), $member1->getId());
        $this->checkComplexLogMessage($logMessages[2], $message, $change, $member1->getId(), $member1->getGroupMembers()[0]->getId());
        $this->checkComplexLogMessage($logMessages[1], $message, $change, $member1->getId(), $member1->getGroupMembers()[1]->getId());
        $this->checkComplexLogMessage($logMessages[0], $message, $change, $member1->getId(), $member1->getGroupMembers()[2]->getId());
    }


    /**
     * @param LogfileEntry $logEntry
     * @param string       $expectedMessage
     * @param array        $expectedData
     * @param int          $changedBy
     * @param int          $changesOn
     */
    private function checkComplexLogMessage(LogfileEntry $logEntry, string $expectedMessage, array $expectedData, int $changedBy, int $changesOn): void
    {
        $complexLogEntry = $logEntry->getComplexLogEntry();
        $this->assertEquals($expectedMessage, $complexLogEntry->getMainMessage());
        $this->assertEquals($expectedData, $complexLogEntry->getDataAsArray());
        $this->assertEquals($changedBy, $logEntry->getChangedByMember()->getId());
        $this->assertEquals($changesOn, $logEntry->getChangesOnMember()->getId());
    }
}
