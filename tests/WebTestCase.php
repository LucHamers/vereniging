<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests;

use App\Entity\MemberEntry;
use Exception;
use Liip\FunctionalTestBundle\Test\WebTestCase as LiipWebTestCase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\DomCrawler\Crawler;
use Throwable;

/**
 * This extends the Liip WebTestCase with some functions needed by all functional tests using a web client.
 */
class WebTestCase extends LiipWebTestCase
{
    use TestCaseTrait;

    protected ?KernelBrowser $client;


    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->client = null;
        $this->getMyClient();
        $this->setupMutual();
    }


    /**
     * @inheritDoc
     */
    public function tearDown():void
    {
        $this->tearDownMutual();
        parent::tearDown();
    }


    /**
     * Check if a link on the dashboard page is there and works.
     *
     * @param WebTestCase $testCase
     * @param string      $linkText
     * @param string      $title
     * @param string      $path
     *
     * @throws Exception
     */
    protected function checkLinkOnDashboardPage(WebTestCase $testCase, string $linkText, string $title, string $path): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', $path);

        // The following doesn't work, unclear why. That is why the test is done "by hand"
        // $testCase->assertSelectorTextContains('a', $linkText, "Link '$linkText' not found on dashboard page.");
        $testCase->assertCount(1, $crawler->filter("a:contains('$linkText')"), "Link '$linkText' not found on dashboard page.");
        $link = $crawler->selectLink($linkText)->link();
        $client->click($link);
        $testCase->assertSelectorTextContains('title', $title, "Link on dashboard page points to wrong page, title '$title' expected, but not found.");
    }


    /**
     * Creates a client. It searches for the passed user name in the database and logs in that user.
     *
     * @param string $userName
     *
     * @return KernelBrowser

     * @throws Exception
     */
    protected function getMyClient(string $userName = 'firstname1.lastname1'): KernelBrowser
    {
        if (!$this->client) {
            $this->client = static::createClient();
        }

        $user = $this->getEntityManager()->getRepository(MemberEntry::class)->findOneBy(['username' => $userName]);

        // When the user is found in the database, then login this user.
        if ($user) {
            $this->client->loginUser($user);
        }


        return $this->client;
    }


    /**
     * Check if the headings of a table are as expected.
     *
     * @param string[] $expectedHeaders String list of expected header texts
     * @param Crawler  $crawler         Crawler where to search
     * @param string   $id              Optional css id in element around the table
     */
    protected function checkTableHeaders(array $expectedHeaders, Crawler $crawler, $id = ''): void
    {
        $this->checkContentAsArray($expectedHeaders, $crawler, $id.' table tr > th');
    }


    /**
     * Filter the content of a page via a css selector and check the text content as if it were an array. This works for
     * tables lines, div rows, ...
     *
     * @param string[] $expectedContents Expected text elements
     * @param Crawler  $crawler          Crawler where to search
     * @param string   $cssFilter        CSS selector to filter before searching
     * @param string   $errorMessage     Additional error message to add when the check fails
     */
    protected function checkContentAsArray(array $expectedContents, Crawler $crawler, string $cssFilter, string $errorMessage = ''): void
    {
        $widgets = $crawler->filter($cssFilter);
        $this->assertCount(count($expectedContents), $widgets);
        $column = 0;
        foreach ($widgets as $widget) {
            $element = new Crawler($widget);
            $this->assertEquals(
                $expectedContents[$column],
                $element->text(),
                "Element not found in column ".($column + 1).$errorMessage
            );
            $column++;
        }
    }


    /**
     * Check if each line of a table contains certain text elements. It searches through the html content of each line,
     * so we can also search for certain html elements.
     *
     * @param string[][] $expectedContents Expected text elements. Per line, an array of strings which all have to be
     *                                     present in a table line.
     * @param Crawler    $crawler          Crawler where to search
     * @param string     $cssFilter        CSS selector for the table
     */
    protected function checkTableForTexts(array $expectedContents, Crawler $crawler, string $cssFilter): void
    {
        $tableRows = $crawler->filter($cssFilter)->filter(' table tr');
        $this->assertCount(count($expectedContents), $tableRows);
        $lineCounter = 0;
        foreach ($tableRows as $row) {
            $row = new Crawler($row);
            foreach ($expectedContents[$lineCounter] as $element) {
                $this->assertStringContainsString($element, $row->html(), "Element \"$element\" not found in line $lineCounter");
            }
            $lineCounter++;
        }
    }


    /**
     * @inheritDoc
     *
     * @param Throwable $e
     *
     * @throws Throwable
     */
    protected function onNotSuccessfulTest(Throwable $t): void
    {
        $this->writeErrorFile($t->getMessage(), $t->getTraceAsString());

        parent::onNotSuccessfulTest($t);
    }


    /**
     * @param string $errorMessage
     * @param string $trace
     */
    protected function writeErrorFile(string $errorMessage, string $trace): void
    {
        $testClass = get_class($this);
        $testName = $this->getName();
        $response = '';
        try {
            if (($this->client) && ($this->client->getResponse())) {
                $response = $this->client->getResponse()->getContent();
            }
        } catch (Exception $e) {
            // Left intentionally blank.
        }

        // Generate a file name containing the test file name and the test name, e.g.
        // App_Tests_Controller_MyControllerTest___testDefault.html
        $fileName = str_replace('\\', '_', "$testClass"."___$testName.html");
        $content = "$response <pre>Error message: $errorMessage\nFailing test: $testClass::$testName\nStacktrace:\n$trace</pre>";
        if (false === strpos($content, '<html')) {
            $content = "<html>$content</html>";
        }
        file_put_contents(self::$logDir."//$fileName", $content);
    }
}
