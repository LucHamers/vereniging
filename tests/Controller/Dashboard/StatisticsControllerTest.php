<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Dashboard;

use App\Entity\MembershipFeeTransaction;
use App\Tests\WebTestCase;
use DateTime;
use Exception;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Test the statistics controller.
 *
 */
class StatisticsControllerTest extends WebTestCase
{
    /**
     * Setup the test environment, i.e. load fixture data.
     *
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test if there is a link on the dashboard page to the statistics page.
     *
     * @throws Exception
     */
    public function testLinkOnDashboardPage(): void
    {
        $this->checkLinkOnDashboardPage($this, 'Statistics', 'Statistics', '/dashboard/statistics');
    }


    /**
     * Test the statistics page.
     *
     * @throws Exception
     */
    public function testIndexGeneralStatistics(): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/dashboard/statistics');

        // Check the page title
        $this->assertEquals(1, $crawler->filter('title:contains("Statistics")')->count());

        // Check the navigation links
        $this->assertCount(1, $crawler->filter('a.navbar-brand:contains("the Vereniging member system dashboard")'));
        $this->assertCount(1, $crawler->filter('a.nav-link:contains("Statistics")'));

        $this->assertCount(1, $crawler->filter('h6:contains("General statistics")'));

        $expectedLines = [
            ['User status', 'Member entries'],
            ['Member', 11],
            ['Future member', 1],
        ];
        $this->checkTable('Members per status', $expectedLines, $crawler);

        $expectedLines = [
            ['Preferred language', 'Member entries'],
            ['Dutch', 10],
            ['English', 1],
            ['German', 1],
        ];
        $this->checkTable('Members per language', $expectedLines, $crawler);

        $expectedLines = [
            ['Member since', 'Member entries'],
            ['2015', 9],
            ['2016', 1],
            ['Unknown', 1],
        ];
        $this->checkTable('Members per membership start', $expectedLines, $crawler);

        $expectedLines = [
            ['Use direct debit', 'Member entries'],
            ['No', 7],
            ['Yes', 4],
        ];
        $this->checkTable('Members per use direct debit', $expectedLines, $crawler);
    }


    /**
     * Test the statistics page.
     *
     * @throws Exception
     */
    public function testIndexFinancialStatistics(): void
    {
        // First add some more transactions to make it more interesting
        $membershipNumber = $this->getFixtureReference('membership_number_1');
        $membershipFee1 = new MembershipFeeTransaction();
        $membershipFee1->setMembershipNumber($membershipNumber);
        // Use an amount with cents to check if the calculation uses floats
        $membershipFee1->setAmount(32);
        $membershipFee1->setCreateDate(new DateTime('2019-01-01'));
        // Close in next year. The value should be added to the 2019 fees
        $membershipFee1->setCloseDate(new DateTime('2020-02-01'));

        // Add an open transaction in 2019
        $membershipFee2 = new MembershipFeeTransaction();
        $membershipFee2->setMembershipNumber($membershipNumber);
        $membershipFee2->setAmount(64.124999);
        $membershipFee2->setCreateDate(new DateTime('2019-12-31'));

        $this->getEntityManager()->persist($membershipFee1);
        $this->getEntityManager()->persist($membershipFee2);
        $this->getEntityManager()->flush();

        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/dashboard/statistics');
        $this->assertCount(1, $crawler->filter('h6:contains("General statistics")'));

        $expectedLines = [
            ['Year', 'Expected contribution', 'Paid', 'Outstanding'],
            ['2020', '€192.00', '€60.00', '€132.00'],
            ['2019',  '€96.12', '€32.00',  '€64.12'],
        ];
        $this->checkTableContent('#financial-statistics', $expectedLines, $crawler);
    }


    /**
     * Check one of the tables containing statistics information.
     *
     * @param string       $title         Title text above the table
     * @param array        $expectedLines Array to compare the table with
     * @param Crawler|null $crawler
     */
    private function checkTable(string $title, array $expectedLines, ?Crawler $crawler): void
    {
        $id = '#'.str_replace(' ', '-', strtolower($title));
        $this->assertCount(1, $crawler->filter("$id div:contains('$title')"));
        $this->checkTableContent($id, $expectedLines, $crawler);
    }


    /**
     * Check one of the tables containing statistics information.
     *
     * @param string       $id            CSS id in element above the table
     * @param array        $expectedLines Array to compare the table with
     * @param Crawler|null $crawler
     */
    private function checkTableContent(string $id, array $expectedLines, ?Crawler $crawler): void
    {
        $this->checkTableHeaders($expectedLines[0], $crawler, $id);
        for ($i = 1; $i < count($expectedLines); $i++) {
            $line = $i + 1;
            $this->checkContentAsArray($expectedLines[$i], $crawler, "$id table tr:nth-child($line) td", " in line $line");
        }
    }
}
