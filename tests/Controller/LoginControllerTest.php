<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller;

use App\Entity\LogfileEntry;
use App\Tests\WebTestCase;
use Exception;

/**
 * Test the login controller.
 *
 */
class LoginControllerTest extends WebTestCase
{
    /**
     * Setup the test environment, i.e. load fixture data.
     *
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test the login form when giving it the correct credentials.
     */
    public function testLoginOK()
    {
        $client = $this->makeClient();
        $this->clearAllLogFiles();
        $client->followRedirects();
        $crawler = $client->request('GET', '/');

        $this->assertEquals(1, $crawler->filter('h3:contains("Login at the Vereniging member system")')->count());

        // Check login form fields
        $this->assertEquals(1, $crawler->filter('html:contains("Username")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Password")')->count());
        $this->assertEquals(1, $crawler->filter('button:contains("Login")')->count());

        // Check if there is a link to the about page and the version number is shown
        $this->assertEquals('About this software', $crawler->filter('a')->text());
        $this->assertEquals(1, $crawler->filter('p:contains("Version")')->count());
        $link = $crawler->filter('a')->attr('href');
        $crawler = $client->request('GET', $link);
        $this->assertEquals(1, $crawler->filter('h6:contains("Development history")')->count());

        // Check if the user can login and is redirected to the dashboard page
        $crawler = $client->request('GET', '/');
        $form = $crawler->filter('form')->form();
        $crawler = $client->submit($form, [
            "login_form[_username]" => "firstname1.lastname1",
            "login_form[_password]" => "secret1",
        ]);

        $this->assertEquals(1, $crawler->filter('html:contains("dashboard")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("My data")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Currently logged in user: firstname1.lastname1")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("role System administrator")')->count());

        $logMessages = $this->getAllLogs();
        $this->assertEquals(1, count($logMessages));
        $this->checkLogMessage($logMessages[0], 'Login', 'Login succeeded');

        // Logout by selecting the logout link and verify that we are back on the login page
        $link = $crawler->selectLink('Logout')->link();
        $crawler = $client->click($link);
        $this->assertEquals(1, $crawler->filter('html:contains("Username")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Password")')->count());
        $this->assertEquals(1, $crawler->filter('button:contains("Login")')->count());

        $logMessages = $this->getAllLogs();
        $this->assertEquals(2, count($logMessages));
        $this->checkLogMessage($logMessages[0], 'Login', 'Logout succeeded');
    }


    /**
     * Test the login form with a bad username or password.
     */
    public function testLoginFail()
    {
        $client = $this->makeClient();
        $this->clearAllLogFiles();
        $client->followRedirects();
        $this->setLoggedInUserForLogging(true);
        $crawler = $client->request('GET', '/');

        // Check login form fields
        $this->assertEquals(1, $crawler->filter('html:contains("Username")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Password")')->count());
        $this->assertEquals(1, $crawler->filter('button:contains("Login")')->count());

        // Check error message when the user name is wrong.
        $form = $crawler->filter('form')->form();
        $crawler = $client->submit($form, [
            "login_form[_username]" => "firstname1.false",
            "login_form[_password]" => "secret1",
        ]);
        $this->assertEquals(1, $crawler->filter('html:contains("Username")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Password")')->count());
        $this->assertEquals(1, $crawler->filter('button:contains("Login")')->count());
        $this->assertEquals(1, $crawler->filter('div.alert-danger:contains("User name or password is wrong!")')->count());
        // Check if the entered user name is put back into the form
        $this->assertEquals('firstname1.false', $crawler->filter('input')->attr('value'));

        // Check error message when the user name is wrong.
        $form = $crawler->filter('form')->form();
        $crawler = $client->submit($form, [
            "login_form[_username]" => "firstname1.lastname1",
            "login_form[_password]" => "bad_password",
        ]);
        $this->assertEquals(1, $crawler->filter('html:contains("Username")')->count());
        $this->assertEquals(1, $crawler->filter('html:contains("Password")')->count());
        $this->assertEquals(1, $crawler->filter('button:contains("Login")')->count());
        $this->assertEquals(1, $crawler->filter('div.alert-danger:contains("User name or password is wrong!")')->count());
        // Check if the entered user name is put back into the form
        $this->assertEquals('firstname1.lastname1', $crawler->filter('input')->attr('value'));

        LogfileEntry::prepareTranslation($this->getEntityManager(), $this->getTranslator(), 'en');
        $logMessages = $this->getEntityManager()->getRepository(LogfileEntry::class)->findBy([], ['id' => 'DESC']);
        $this->assertEquals(2, count($logMessages));
        $this->assertEquals('Login error', $logMessages[0]->getChangeType()->getChangeType());
        $this->assertEquals('Login error', $logMessages[1]->getChangeType()->getChangeType());
        $this->assertEquals('Login failure with message "The presented password is invalid." and user name "firstname1.lastname1"', $logMessages[0]->getLogEntry());
        $this->assertEquals('Login failure with message "Bad credentials." and user name "firstname1.false"', $logMessages[1]->getLogEntry());
    }


    /**
     * Check the content of a log message
     *
     * @param LogfileEntry $logMessage Log message to check
     * @param string       $changeType Expected change type
     * @param string       $message    Expected string content
     */
    private function checkLogMessage(LogfileEntry $logMessage, string $changeType, string $message)
    {
        $this->assertEquals($changeType, $logMessage->getChangeType()->getChangeType());
        $this->assertEquals('Firstname1 Lastname1', $logMessage->getChangedByMember()->getCompleteName());
        $this->assertEquals('Firstname1 Lastname1', $logMessage->getChangesOnMember()->getCompleteName());
        $this->assertEquals($message, $logMessage->getLogEntry());
    }
}
