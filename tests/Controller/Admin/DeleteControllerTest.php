<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Admin;

use Exception;

/**
 * Test class for deleting users controller.
 *
 */
class DeleteControllerTest extends TestWithMemberSelection
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test if there is a link on the dashboard page to the new member page.
     *
     * @throws Exception
     */
    public function testLinkOnDashboardPage(): void
    {
        $this->checkLinkOnDashboardPage($this, 'Delete users', 'Delete users', '/administration');
    }


    /**
     * Test if the selection form on this page works correctly
     *
     * @throws Exception
     */
    public function testSelectionForm(): void
    {
        $this->checkMemberSelectionForm('/administration/delete_users');
    }


    /**
     * Test the index controller.
     *
     * @throws Exception
     */
    public function testIndex(): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/administration/delete_users');

        // Check the page title
        $this->assertEquals(1, $crawler->filter('title:contains("Administration")')->count());
        $this->assertEquals(1, $crawler->filter('title:contains("Delete users")')->count());

        // Check the navigation links
        $this->assertEquals(1, $crawler->filter('a.navbar-brand:contains("the Vereniging member system dashboard")')->count());
        $this->assertEquals(2, $crawler->filter('a.nav-link:contains("Administration")')->count());
        $this->assertEquals(1, $crawler->filter('a.nav-link:contains("Delete users")')->count());

        // Check the table headings
        $headings = ["Last name", "First name", "Email address", ""];
        $this->checkTableHeaders($headings, $crawler, '#member_table');

        $expectedLines = [
            ['Lastname1', 'Firstname1', 'firstname1@email.com', ''],
            ['Lastname1', 'Firstname11', 'firstname11@email.com', 'Delete user'],
            ['Lastname10', 'Firstname10', 'firstname10@email.com', 'Delete user'],
            ['Lastname12', 'Firstname12', 'firstname12@email.com', 'Delete user'],
            ['Lastname2', 'Firstname2', '', 'Delete user'],
            ['Lastname3', 'Firstname3', 'firstname3@email.com', 'Delete user'],
            ['Lastname5', 'Firstname5', 'firstname5@email.com', 'Delete user'],
            ['Lastname6', 'Firstname6', 'firstname6@email.com', 'Delete user'],
            ['Lastname7', 'Firstname7', 'firstname7@email.com', 'Delete user'],
            ['Lastname8', 'Firstname8', 'firstname8@email.com', 'Delete user'],
            ['Lastname9', 'Firstname9', 'firstname9@email.com', 'Delete user'],
        ];
        $lineCounter = 2;
        foreach ($expectedLines as $expectedLine) {
            $this->checkContentAsArray(
                $expectedLine,
                $crawler,
                "#member_table table tr:nth-child($lineCounter) td",
                " in line $lineCounter"
            );
            $lineCounter++;
        }
    }


    /**
     * This method tests if the deleted user button works correctly
     */
    public function testDeleteButtons(): void
    {
        $client = $this->getMyClient();
        $crawler = $client->request('GET', '/administration/delete_users');
        $buttonLink = $crawler->filter('#js-user-item-2 a')->link();
        $crawler = $client->click($buttonLink);
        $this->assertEquals('Confirm deletion of user', $crawler->filter('#confirmTitle')->html());
        $this->checkContentAsArray(['Name:Lastname2', 'First name:Firstname2', 'Email address:', 'User name:firstname2.lastname2'], $crawler, '.modal-body > table > tr');
        $this->assertCount(3, $crawler->filter('button'));
        $this->assertCount(2, $crawler->filter('.modal-footer button'));
        $this->assertCount(1, $crawler->filter('.modal-footer button:contains("Cancel")'));
        $this->assertCount(1, $crawler->filter('.modal-footer button:contains("Delete user")'));
        $this->assertCount(1, $crawler->filter('#js-delete_button[href*="/delete_users/delete/2"]'));
    }
}
