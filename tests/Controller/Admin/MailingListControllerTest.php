<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Admin;

use App\Entity\LogfileEntry;
use App\Entity\MailingList;
use App\Entity\MemberEntry;
use App\Service\MailingListUpdateInterface;
use App\Service\MailManUpdater;
use App\Tests\WebTestCase;
use Exception;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Test class for the mailing lists controller.
 *
 */
class MailingListControllerTest extends WebTestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
    }


    /**
     * Test if there is a link on the dashboard page to the new member page.
     *
     * @throws Exception
     */
    public function testLinkOnDashboardPage(): void
    {
        $this->checkLinkOnDashboardPage($this, 'Manage mailing lists', 'Manage mailing lists', '/administration');
    }


    /**
     * Test the index controller with the default service for unit tests, which is majordomo.
     *
     * @throws Exception
     */
    public function testIndexWithMajorDomo(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_mailing_lists');

        // Check the page title
        $this->assertEquals(1, $crawler->filter('title:contains("Administration")')->count());
        $this->assertEquals(1, $crawler->filter('title:contains("Manage mailing lists")')->count());

        // Check the navigation links
        $this->assertEquals(1, $crawler->filter('a.navbar-brand:contains("the Vereniging member system dashboard")')->count());
        $this->assertEquals(2, $crawler->filter('a.nav-link:contains("Administration")')->count());
        $this->assertEquals(1, $crawler->filter('a.nav-link:contains("Manage mailing lists")')->count());

        // Check the table headings
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Mailing lists")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("List name")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Management email")')->count());
        $this->assertEquals(0, $crawler->filter('th:contains("Server domain name")')->count());
        $this->assertEquals(0, $crawler->filter('th:contains("Management URL")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Management password")')->count());

        // Check if all the expected mailing lists are present
        $this->checkListPresence($crawler, [1, 2, 3], "testIndex");

        // Check some javascripts
        $this->assertEquals(1, $crawler->filter('script:contains("function doAjax")')->count());
        $this->assertEquals(1, $crawler->filter('script:contains("#edit_mailing_lists\').on(\'click")')->count());
        $this->assertEquals(1, $crawler->filter('script:contains("#reset_mailing_list_data\').on(\'click")')->count());

        // Check ajax anker
        $this->assertEquals(1, $crawler->filter('div#mailing_list_content')->count());
        // Check if the type is shown correctly
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("type: MajorDomo")')->count());
        // Check if the password is not shown
        $this->assertEquals(0, $crawler->filter('td:contains("password1")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("password2")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("password3")')->count());
        $this->assertEquals(3, $crawler->filter('td:contains("******")')->count());
    }


    /**
     * Test the index controller with the list updater for mailman. For this, the object in the service container has
     * to be changed to MailManUpdater.
     *
     * @throws Exception
     */
    public function testIndexWithMailMan(): void
    {
        $client = $this->getMyClient();
        $mailManUpdater = new MailManUpdater($this->getEntityManager());
        $client->getContainer()->set(MailingListUpdateInterface::class, $mailManUpdater);
        $crawler = $client->request('GET', '/administration/manage_mailing_lists');

        // Check the table headings: for mailman, the email field has a different heading
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Mailing lists")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("List name")')->count());
        $this->assertEquals(0, $crawler->filter('th:contains("Management email")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Server domain name")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Management URL")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Management password")')->count());

        // Check if the type is shown correctly
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("type: MailMan")')->count());
        // Check if the password is not shown
        $this->assertEquals(0, $crawler->filter('td:contains("password1")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("password2")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("password3")')->count());
        $this->assertEquals(3, $crawler->filter('td:contains("******")')->count());

        // Check the management URL (looks a bit strange here, because the management email address is interpreted here
        // as the domain)
        $this->assertEquals(1, $crawler->filter('td:contains("mailinglist1@mail.org/mailman/admin/MailingList_1/members/")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("mailinglist2@mail.org/mailman/admin/MailingList_2/members/")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("mailinglist3@mail.org/mailman/admin/MailingList_3/members/")')->count());
    }


    /**
     * Test the index controller when the showOnlyContent flag is set.
     *
     * @throws Exception
     */
    public function testIndexWithShowOnlyContentTable(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_mailing_lists/1');

        // Check the page title
        $this->assertEquals(0, $crawler->filter('title:contains("Administration")')->count());
        $this->assertEquals(0, $crawler->filter('title:contains("Manage mailing lists")')->count());

        // Check the navigation links
        $this->assertEquals(0, $crawler->filter('a.navbar-brand:contains("the Vereniging member system dashboard")')->count());
        $this->assertEquals(0, $crawler->filter('a.nav-link:contains("Administration")')->count());
        $this->assertEquals(0, $crawler->filter('a.nav-link:contains("Manage mailing lists")')->count());

        // Check the table headings
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Mailing lists")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("List name")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Management email")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("Management password")')->count());

        // Check if all the expected mailing lists are present
        $this->checkListPresence($crawler, [1, 2, 3], "testIndex");
    }


    /**
     * Test the edit action without submitting data.
     *
     * @throws Exception
     */
    public function testEditShow(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_mailing_lists_edit');
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Mailing lists")')->count());

        // Check if the javascript classes used for jquery are present
        $this->assertEquals(1, $crawler->filter('tbody.js-mailing-list-table')->count());
        $this->assertEquals(1, $crawler->filter('tbody[data-prototype]')->count());
        $this->assertEquals(1, $crawler->filter('tbody[data-index=3]')->count());
        $this->assertEquals(1, $crawler->filter('tbody.js-mailing-list-table')->count());
        $this->assertEquals(3, $crawler->filter('tr.js-mailing-list-item')->count());
        $this->assertEquals(3, $crawler->filter('td a.js-remove-mailing-list')->count());
        $this->assertEquals(1, $crawler->filter('td a.js-mailing-list-add')->count());

        // Check if both buttons are available
        $this->assertEquals(1, $crawler->filter('button#save_mailing_list_data[type="submit"]:contains("Save")')->count());
        $this->assertEquals(1, $crawler->filter('button#reset_mailing_list_data[type="reset"]:contains("Cancel")')->count());

        // Check if the input fields are present
        $this->assertEquals(3, $crawler->filter('input[type="text"][value*="MailingList"]')->count());
        $this->assertEquals(3, $crawler->filter('input[type="text"][value*="@mail.org"]')->count());
        $this->assertEquals(3, $crawler->filter('input[type="text"][value*="password"]')->count());
    }


    /**
     * Test the update action.
     *
     * @throws Exception
     */
    public function testEditCommit(): void
    {
        $client = $this->getMyClient();

        // Delete the third mailing list
        $crawler = $client->request('GET', '/administration/manage_mailing_lists_edit');
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        unset($form['form_collection[members][2]']);
        $crawler = $client->submit($form);
        $this->checkListPresence($crawler, [1, 2], "testEditCommit - remove");

        // Change password and check for log message
        $crawler = $client->request('GET', '/administration/manage_mailing_lists_edit');
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        $form['form_collection[members][1][managementPassword]'] = "changed password2";
        $crawler = $client->submit($form);
        $this->checkListPresence($crawler, [1, 2], "testEditCommit - changed password");
        // Check if special log message for changing the password is created.
        $logMessage = $this->getEntityManager()->getRepository(LogfileEntry::class)
                                               ->findOneBy([], ['id' => 'DESC']);
        $this->assertEquals('Changed management password for mailing list MailingList_2', $logMessage->getLogentry());

        // Add mailinglist 3 to the list
        $data = $form->getPhpValues();
        $data['form_collection']['members'][] = [
            "listName" => "MailingList_3",
            "managementEmail" => "mailinglist3@mail.org",
            "managementPassword" => "password3",
        ];

        $crawler = $client->request('POST', '/administration/manage_mailing_lists_edit', $data);
        $this->checkListPresence($crawler, [1, 2, 3], "testEditCommit - add");

        // Clear a mailing list name. This should be ignored and there should be no change to the database and the
        // output
        $data['form_collection']['members'][2]['listName'] = '';
        $crawler = $client->request('POST', '/administration/manage_mailing_lists_edit', $data);
        $this->checkListPresence($crawler, [1, 2, 3], "testEditCommit - list name cleared");

        // Add empty mailinglist to the list --> this should be ignored
        $data['form_collection']['members'][] = [
            "listName" => "",
            "managementEmail" => "",
            "managementPassword" => "",
        ];

        $crawler = $client->request('POST', '/administration/manage_mailing_lists_edit', $data);
        $this->checkListPresence($crawler, [1, 2, 3], "testEditCommit - empty line added");

        // Change email address to server address: http should be added.
        $crawler = $client->request('GET', '/administration/manage_mailing_lists_edit');
        $buttonCrawlerNode = $crawler->selectButton('Save');
        $form = $buttonCrawlerNode->form();
        $form['form_collection[members][1][managementEmail]'] = "myserver.com";
        $crawler = $client->submit($form);
        $this->assertEquals(1, $crawler->filter('td:contains("http://myserver.com")')->count());

        // The following is to check for a bug which prevented the use of https domains.
        $form['form_collection[members][1][managementEmail]'] = "https://myserver.com";
        $crawler = $client->submit($form);
        $this->assertEquals(1, $crawler->filter('td:contains("https://")')->count());
        $this->assertEquals(0, $crawler->filter('td:contains("http://")')->count());
    }


    /**
     * Test the sections for showing mailing list subscriberss
     *
     * @throws Exception
     */
    public function testShowSubscribers(): void
    {
        $client = $this->getMyClient();

        $crawler = $client->request('GET', '/administration/manage_mailing_lists');

        // Check if the subscribers frame is shown
        $this->assertEquals(1, $crawler->filter('h6.card-header:contains("Subscribers")')->count());
        $this->assertEquals(1, $crawler->filter('select:contains("MailingList_1")')->count());
        $this->assertEquals(1, $crawler->filter('select:contains("MailingList_2")')->count());
        $this->assertEquals(1, $crawler->filter('select:contains("MailingList_3")')->count());

        // Check some ids needed for the ajax calls
        $this->assertEquals(1, $crawler->filter('select[id="mailing_list_subscriber_mailinglist"]')->count());
        $this->assertEquals(1, $crawler->filter('div[id="mailing_list_subscribers"]')->count());

        // Check result when selecting the defaul (no mailing list) from the selection form
        $crawler = $client->request('GET', '/administration/manage_mailing_lists');
        $form = $crawler->filter('form[name="mailing_list_subscriber"]')->form();

        $crawler = $client->submit($form);
        // The returned page should contain an empty body frame without a table
        $this->assertEquals(1, $crawler->filter('div[class="card-body"]')->count());
        $this->assertEquals(0, $crawler->filter('table')->count());


        // Now select the first mailing list and check the results. To check if multiple members are shown, add a second
        // member here.
        $entityManager = $this->getEntityManager();
        /** @var MailingList $mailingList1 */
        $mailingList1 = $entityManager->getRepository(MailingList::class)->findOneBy([
            'listName' => 'MailingList_1',
        ]);

        $member3 = $entityManager->getRepository(MemberEntry::class)->findOneBy([
            'firstName' => 'Firstname3',
        ]);

        $member3->addMailingList($mailingList1);
        $entityManager->flush();

        $crawler = $client->request('GET', '/administration/manage_mailing_lists');
        $form = $crawler->filter('form[name="mailing_list_subscriber"]')->form();
        $form['mailing_list_subscriber[mailinglist]']->select($mailingList1->getId());

        $crawler = $client->submit($form);
        $this->assertEquals(1, $crawler->filter('table')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("MailingList_1")')->count());
        $this->assertEquals(1, $crawler->filter('th:contains("2 subscribers")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("firstname1@email.com")')->count());
        $this->assertEquals(1, $crawler->filter('td:contains("firstname3@email.com")')->count());
    }


    /**
     * @param Crawler $crawler
     * @param int[]   $expectedListNumbers
     * @param string  $testCase
     */
    private function checkListPresence(Crawler $crawler, array $expectedListNumbers, string $testCase): void
    {
        foreach ($expectedListNumbers as $listNumber) {
            $message = "$testCase - $listNumber";
            $this->assertEquals(1, $crawler->filter('td:contains("MailingList_'.$listNumber.'")')->count(), $message);
            $this->assertEquals(1, $crawler->filter('td:contains("mailinglist'.$listNumber.'@mail.org")')->count(), $message);
        }
    }
}
