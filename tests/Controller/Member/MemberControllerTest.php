<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Controller\Member;

use App\Entity\ComplexLogEntryDoubleDataField;
use App\Entity\MemberEntry;
use App\Service\LogMessageCreator;
use App\Tests\WebTestCase;
use Exception;

/**
 * Class MemberControllerTest
 */
class MemberControllerTest extends WebTestCase
{
    private ?MemberEntry $member1;


    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();

        $this->member1 = $this->getFixtureReference('member_entry_last_name1');
    }


    /**
     * Test if there is a link on the dashboard page to the new member page.
     *
     * @throws Exception
     */
    public function testLinkOnDashboardPage(): void
    {
        $this->checkLinkOnDashboardPage($this, 'My data', 'View and edit data', '/dashboard');
    }


    /**
     * Test if on the index page the main widgets are shown.
     *
     * @throws Exception
     */
    public function testIndex(): void
    {
        // There was a bug in showing the new complex log message format. To prevent regressions, a test is added here.
        $complexLogEntry = new ComplexLogEntryDoubleDataField();
        $complexLogEntry->setMainMessage('my message');
        $complexLogEntry->addDataField('status', ['old', 'new']);

        $this->setLoggedInUserForLogging();
        /** @var LogMessageCreator $logMessageCreator */
        $logMessageCreator = $this->getContainer()->get(LogMessageCreator::class);
        $logMessageCreator->createLogEntry('member data change', $complexLogEntry, $this->member1, $this->getEntityManager());
        $this->getEntityManager()->flush();

        $crawler = $this->client->request('GET', '/member/firstname1.lastname1');

        $this->assertCount(1, $crawler->filter('h6.card-header:contains("Personal data")'));
        $this->assertCount(1, $crawler->filter('h6.card-header:contains("Contact data")'));
        $this->assertCount(1, $crawler->filter('h6.card-header:contains("Company data")'));
        $this->assertCount(1, $crawler->filter('h6.card-header:contains("Club data")'));
        $this->assertCount(1, $crawler->filter('h6.card-header:contains("Financial data")'));
        $this->assertCount(1, $crawler->filter('h6.card-header:contains("Log entries")'));
        $this->assertCount(1, $crawler->filter('td:contains("my message")'));

        // Check financial data section
        $expectedHeadings = ['Type', 'Amount', 'Billing date', 'Paid'];
        $this->checkTableHeaders($expectedHeadings, $crawler, '#financial_data');

        $expectedLines = [
            ['Contribution claim', '€52.00', 'Nov 29, 2020', '-'],
            ['Contribution claim', '€36.00', 'Nov 28, 2020', '-'],
            ['Contribution claim', '€36.00', 'Nov 1, 2020', 'Nov 10, 2020'],
        ];
        $lineCounter = 2;
        foreach ($expectedLines as $expectedLine) {
            $this->checkContentAsArray($expectedLine, $crawler, "#financial_data table tr:nth-child($lineCounter) td", " in line $lineCounter");
            $lineCounter++;
        }
    }


    /**
     * Test calling the index page for a user which is not allowed to do this
     *
     * @throws Exception
     */
    public function testIndexWithoutAccessRight(): void
    {
        $this->getMyClient('firstname2.lastname2');

        $crawler = $this->client->request('GET', '/member/firstname1.lastname1');

        $this->assertEquals(1, $crawler->filter('title:contains("You are not allowed to edit other users data! (403 Forbidden)")')->count());
    }
}
