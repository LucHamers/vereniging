<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests\Security;

use App\Tests\WebTestCase;
use Exception;

/**
 * Class SessionIdleHandlerTest
 */
class SessionIdleHandlerTest extends WebTestCase
{
    private static $defaultTimeOut;

    /**
     * @inheritDoc
     */
    public static function setUpBeforeClass(): void
    {
        self::$defaultTimeOut = $_ENV['VERENIGING_SESSION_TIMEOUT_SECONDS'];
        parent::setUpBeforeClass();
    }


    /**
     * @inheritDoc
     */
    public static function tearDownAfterClass(): void
    {
        // Reset timout to the original setting
        $_ENV['VERENIGING_SESSION_TIMEOUT_SECONDS'] = self::$defaultTimeOut;
    }


    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAllFixtures();
        $this->setLoggedInUserForLogging();
    }

    /**
     * Test if the user is logged out automatically
     *
     * @throws Exception
     */
    public function testLogoutOnIdle(): void
    {
        // Change the session timeout to 2 seconds to be able to test the redirect to the login page.
        $_ENV['VERENIGING_SESSION_TIMEOUT_SECONDS'] = 2;
        $client = $this->getMyClient();
        $client->followRedirects(true);

        $crawler = $client->request('GET', '/dashboard');
        $link = $crawler->selectLink('Club structure')->link();
        $this->clearAllLogFiles();
        // Wait for 3 seconds to pass the idle time
        sleep(3);

        // This should redirect to the login page instead of to the club structure page.
        $crawler = $client->click($link);
        $this->assertSelectorTextContains('title', 'Login at the Vereniging member system', 'No redirect to login page after idle time.');

        $logs = $this->getAllLogs();
        $this->assertCount(1, $logs);
        $this->assertEquals('User logged off automatically', $logs[0]->getLogentry());
        $this->assertEquals('Firstname1', $logs[0]->getChangedByMember()->getFirstName());
    }


    /**
     * Test if the user is logged out outomatically
     *
     * @throws Exception
     */
    public function testExceptionOnWrongIdleTime(): void
    {
        // Set idle time to an illegal value
        $_ENV['VERENIGING_SESSION_TIMEOUT_SECONDS'] = 0;
        $client = $this->getMyClient();
        $this->expectExceptionMessage('maxIdleTime must be greater than zero, but "0" was passed!');

        $client->request('GET', '/dashboard');
    }
}
