<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Tests;

use App\Entity\Address;

/**
 * Test the methods in the special WebTestCase used for all functional unit tests
 *
 * Class WebTestCaseTest
 */
class WebTestCaseTest extends WebTestCase
{
    /**
     * @inheritDoc
     */
    public function setUp(): void
    {
        parent::setUp();
    }


    /**
     * Test load all fixtures method
     */
    public function testLoadAllFixtures(): void
    {
        $entityManager = $this->getEntityManager();

        // Load the fixtures with a valid group. The database should be initialised completely, checked here with the
        // addresses.
        $this->loadAllFixtures(['default']);
        $this->assertCount(6, $entityManager->getRepository(Address::class)->findAll());

        // Load the fixtures with an invalid group. The database should be empty.
        $this->loadAllFixtures(['bla']);
        $this->assertCount(0, $entityManager->getRepository(Address::class)->findAll());
    }
}
