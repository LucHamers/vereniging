<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Service;

use App\Entity\MailingList;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

/**
 * This class implements updating members of majordomo mailing lists.
 *
 * Class MajorDomoUpdater
 */
class MajorDomoUpdater implements MailingListUpdateInterface
{
    private string $senderAddress;
    private MailerInterface $mailer;


    /**
     * MajorDomoUpdater constructor.
     *
     * @param MailerInterface $mailer
     * @param string          $senderAddress Email address from which the mails to majordomo are send.
     */
    public function __construct(MailerInterface $mailer, string $senderAddress)
    {
        $this->senderAddress = $senderAddress;
        $this->mailer = $mailer;
    }


    /**
     * @inheritdoc
     *
     * @param MailingList $mailingList  Mailing list object to which the email address has to be added.
     * @param string      $emailAddress Email address which has to be added to the list.
     */
    public function addEmailAddress(MailingList $mailingList, string $emailAddress): void
    {
        $this->addOrRemoveEmailAddress($mailingList, $emailAddress, 'subscribe');
    }


    /**
     * @inheritdoc
     *
     * @param MailingList $mailingList  Mailing list object from which the email address has to be removed.
     * @param string      $emailAddress Email address which has to be removed from the list.
     */
    public function removeEmailAddress(MailingList $mailingList, string $emailAddress): void
    {
        $this->addOrRemoveEmailAddress($mailingList, $emailAddress, 'unsubscribe');
    }


    /**
     * @inheritdoc
     *
     * @param bool $edit When true, return the field names above the form. If false, return the field names above the
     *                   text columns.
     *
     * @return string[]|array
     */
    public function getFieldNames(bool $edit): array
    {
        return ['List name', 'Management email', 'Management password'];
    }


    /**
     * This returns the field texts matching the field names when not editing, i.e. when only showing the text
     *
     * @param MailingList $mailingList Mailing list object from which the email address has to be removed.
     *
     * @return string[]|array
     */
    public function getTextFields(MailingList $mailingList): array
    {
        return [$mailingList->getListName(), $mailingList->getManagementEmail(), '******'];
    }


    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getMailingListType(): string
    {
        return 'MajorDomo';
    }


    /**
     * @param MailingList $mailingList  Mailing list object from which the email address has to be removed.
     * @param string      $emailAddress Email address which has to be removed from the list.
     * @param string      $action       'subscribe' or 'unsubscribe'
     */
    private function addOrRemoveEmailAddress(MailingList $mailingList, string $emailAddress, string $action): void
    {
        $mailingListName = $mailingList->getListName();
        $body = 'approve '.$mailingList->getManagementPassword()." $action $mailingListName $emailAddress";

        $email = (new Email())
            ->from($this->senderAddress)
            ->to($mailingList->getManagementEmail())
            ->subject("Update mailinglist \"$mailingListName\"")
            ->text($body);

        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
        }
    }
}
