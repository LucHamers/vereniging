<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Service;

use App\Entity\CommitteeFunctionMap;
use App\Entity\MemberEntry;
use Exception;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

/**
 * This is a service class which generates excel files as an export of member entries.
 *
 * Class GenerateExcelDataFile
 */
class GenerateExcelDataFile
{
    private ?MemberEntry $loggedInUser;
    private int $numberOfCompanyInformation;
    private int $numberOfPhoneNumbers;
    private int $numberOfAddresses;
    private int $numberOfCommitteeFunctions;
    private int $numberOfGroupMembers;
    private int $indexOfCompanyInformation;
    private int $indexOfPhoneNumbers;
    private int $indexOfAddresses;
    private int $indexOfCommitteeFunctions;
    private int $indexOfGroupMembers;


    /**
     * GenerateExcelDataFile constructor.
     */
    public function __construct()
    {
        $this->loggedInUser = null;
        $this->numberOfCompanyInformation = 0;
        $this->numberOfPhoneNumbers = 0;
        $this->numberOfAddresses = 0;
        $this->numberOfCommitteeFunctions = 0;
        $this->numberOfGroupMembers = 0;
        $this->indexOfCompanyInformation = 0;
        $this->indexOfPhoneNumbers = 0;
        $this->indexOfAddresses = 0;
        $this->indexOfCommitteeFunctions = 0;
        $this->indexOfGroupMembers = 0;
    }


    /**
     * Set the user which is currently logged in. This is needed to set the changedByUser in the log message. Since this
     * user is not available on the creation of the service, it has to be set before the log file entry is created.
     *
     * @param MemberEntry $loggedInUser Currently logged in user.
     */
    public function setLoggedInUser(MemberEntry $loggedInUser): void
    {
        $this->loggedInUser = $loggedInUser;
    }



    /**
     * Create an Excel file for the member entries passed to the method.
     *
     * @param array|MemberEntry[] $memberEntries
     *
     * @throws Exception
     *
     * @return Spreadsheet
     */
    public function createExcelFile(array $memberEntries): Spreadsheet
    {
        if (!$this->loggedInUser) {
            throw new Exception("Logged in user is not set!");
        }

        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()
                    ->setCreator($this->loggedInUser->getCompleteName())
                    ->setLastModifiedBy($this->loggedInUser->getCompleteName())
                    ->setTitle('Vereniging data export')
                    ->setSubject('Vereniging data export')
                    ->setDescription('Vereniging data export');

        $spreadsheet->setActiveSheetIndex(0)
                    ->setTitle('Vereniging data export');


        $this->initializeFieldCounters($memberEntries);
        $this->addHeaders($spreadsheet);
        $this->addMemberData($memberEntries, $spreadsheet);

        return $spreadsheet;
    }


    /**
     * Search through all member entries and find out the maximum number of all dynamic fields
     *
     * @param array|MemberEntry[] $memberEntries
     */
    private function initializeFieldCounters(array $memberEntries): void
    {
        foreach ($memberEntries as $memberEntry) {
            if ($memberEntry->getCompanyInformation()) {
                $this->numberOfCompanyInformation = 1;
            }

            if (count($memberEntry->getMemberAddressMaps()) > $this->numberOfAddresses) {
                $this->numberOfAddresses = count($memberEntry->getMemberAddressMaps());
            }

            if (count($memberEntry->getPhoneNumbers()) > $this->numberOfPhoneNumbers) {
                $this->numberOfPhoneNumbers = count($memberEntry->getPhoneNumbers());
            }

            if (count($memberEntry->getCommitteeFunctionMaps()) > $this->numberOfCommitteeFunctions) {
                $this->numberOfCommitteeFunctions = count($memberEntry->getCommitteeFunctionMaps());
            }

            if (count($memberEntry->getGroupMembers()) > $this->numberOfGroupMembers) {
                $this->numberOfGroupMembers = count($memberEntry->getGroupMembers());
            }
        }
    }


    /**
     * Create the headers of the Excel file.
     *
     * @param Spreadsheet $spreadsheet
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function addHeaders(Spreadsheet $spreadsheet): void
    {
        $columnIndex = 1;
        // Add headers for the static fields
        foreach (ExcelDataFormats::STATIC_HEADERS as $header) {
            // Start writing from cell A1 (row = 1; column starts with 1)
            $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($columnIndex, 1, $header)
                                          ->mergeCellsByColumnAndRow($columnIndex, 1, $columnIndex, 2);
            ++$columnIndex;
        }

        // Add headers for the dynamic fields
        $columnIndex = $this->addCompanyHeaders($spreadsheet, $columnIndex);
        $columnIndex = $this->addPhoneNumberHeaders($spreadsheet, $columnIndex);
        $columnIndex = $this->addAddressesHeaders($spreadsheet, $columnIndex);
        $columnIndex = $this->addCommitteeFunctionHeaders($spreadsheet, $columnIndex);
        $columnIndex = $this->addGroupMembersHeaders($spreadsheet, $columnIndex);

        // Auto size all the header columns
        for ($column = 0; $column < $columnIndex; ++$column) {
            $spreadsheet->getActiveSheet()->getColumnDimensionByColumn($column + 1)->setAutoSize(true);
        }
    }


    /**
     * Create the headers of the Excel file.
     *
     * @param array       $memberEntries
     * @param Spreadsheet $spreadsheet
     */
    private function addMemberData(array $memberEntries, Spreadsheet $spreadsheet): void
    {
        // Row 1 and 2 are used for headers
        $rowIndex = 3;
        $sheet = $spreadsheet->getActiveSheet();
        /** @var MemberEntry $memberEntry */
        foreach ($memberEntries as $memberEntry) {
            $this->addStaticFieldData($sheet, $rowIndex, $memberEntry);
            $this->addCompanyData($sheet, $rowIndex, $memberEntry);
            $this->addPhoneNumberData($sheet, $rowIndex, $memberEntry);
            $this->addAddressData($sheet, $rowIndex, $memberEntry);
            $this->addCommitteeFunctions($sheet, $rowIndex, $memberEntry);
            $this->addGroupMembers($sheet, $rowIndex, $memberEntry);

            ++$rowIndex;
        }
    }


    /**
     * @param Spreadsheet $spreadsheet
     * @param int         $columnIndex
     *
     * @return int
     */
    private function addCompanyHeaders(Spreadsheet $spreadsheet, int $columnIndex): int
    {
        $this->indexOfCompanyInformation = $columnIndex;

        $entryCounter = $this->numberOfCompanyInformation;
        $headerInfo = ExcelDataFormats::DYNAMIC_HEADERS['companyInformation'];
        $columnIndex = $this->addDynamicHeaderEntry($spreadsheet, $columnIndex, $entryCounter, $headerInfo, true);

        return $columnIndex;
    }


    /**
     * @param Spreadsheet $spreadsheet
     * @param int         $columnIndex
     *
     * @return int
     */
    private function addPhoneNumberHeaders(Spreadsheet $spreadsheet, int $columnIndex): int
    {
        $this->indexOfPhoneNumbers = $columnIndex;

        $entryCounter = $this->numberOfPhoneNumbers;
        $headerInfo = ExcelDataFormats::DYNAMIC_HEADERS['phoneNumbers'];
        $columnIndex = $this->addDynamicHeaderEntry($spreadsheet, $columnIndex, $entryCounter, $headerInfo);

        return $columnIndex;
    }


    /**
     * @param Spreadsheet $spreadsheet
     * @param int         $columnIndex
     *
     * @return int
     */
    private function addAddressesHeaders(Spreadsheet $spreadsheet, int $columnIndex): int
    {
        $this->indexOfAddresses = $columnIndex;

        $entryCounter = $this->numberOfAddresses;
        $headerInfo = ExcelDataFormats::DYNAMIC_HEADERS['addresses'];
        $columnIndex = $this->addDynamicHeaderEntry($spreadsheet, $columnIndex, $entryCounter, $headerInfo);

        return $columnIndex;
    }


    /**
     * @param Spreadsheet $spreadsheet
     * @param int         $columnIndex
     *
     * @return int
     */
    private function addCommitteeFunctionHeaders(Spreadsheet $spreadsheet, int $columnIndex): int
    {
        $this->indexOfCommitteeFunctions = $columnIndex;

        $entryCounter = $this->numberOfCommitteeFunctions;
        $headerInfo = ExcelDataFormats::DYNAMIC_HEADERS['committees'];
        $columnIndex = $this->addDynamicHeaderEntry($spreadsheet, $columnIndex, $entryCounter, $headerInfo);

        return $columnIndex;
    }


    /**
     * @param Spreadsheet $spreadsheet
     * @param int         $columnIndex
     *
     * @return int
     */
    private function addGroupMembersHeaders(Spreadsheet $spreadsheet, int $columnIndex): int
    {
        $this->indexOfGroupMembers = $columnIndex;

        $entryCounter = $this->numberOfGroupMembers;
        $headerInfo = ExcelDataFormats::DYNAMIC_HEADERS['groupMembers'];
        $columnIndex = $this->addDynamicHeaderEntry($spreadsheet, $columnIndex, $entryCounter, $headerInfo);

        return $columnIndex;
    }


    /**
     * Add all fields to the work sheet line which are static, i.e. which the member entry always has.
     *
     * @param Worksheet   $sheet       Work sheet to add the data to
     * @param int         $rowIndex    Row to write the data to
     * @param MemberEntry $memberEntry Data source
     */
    private function addStaticFieldData(Worksheet $sheet, int $rowIndex, MemberEntry $memberEntry): void
    {
        $sheet->setCellValueByColumnAndRow(1, $rowIndex, $memberEntry->getName())
              ->setCellValueByColumnAndRow(2, $rowIndex, $memberEntry->getFirstName())
              ->setCellValueByColumnAndRow(3, $rowIndex, $memberEntry->getGender())
              ->setCellValueByColumnAndRow(4, $rowIndex, $memberEntry->getTitle())
              ->setCellValueByColumnAndRow(5, $rowIndex, $memberEntry->getBirthday())
              ->setCellValueByColumnAndRow(6, $rowIndex, $memberEntry->getPreferredLanguage())
              ->setCellValueByColumnAndRow(7, $rowIndex, $memberEntry->getEmail())
              ->setCellValueByColumnAndRow(8, $rowIndex, $memberEntry->getMembershipNumber()->getMembershipNumber())
              ->setCellValueByColumnAndRow(9, $rowIndex, $memberEntry->getMembershipNumber()->getUseDirectDebit())
              ->setCellValueByColumnAndRow(10, $rowIndex, $memberEntry->getMembershipEndRequested())
              ->setCellValueByColumnAndRow(11, $rowIndex, $memberEntry->getMemberSince())
              ->setCellValueByColumnAndRow(12, $rowIndex, $memberEntry->getStatus())
              ->setCellValueByColumnAndRow(13, $rowIndex, $memberEntry->getMembershipType())
              ->setCellValueByColumnAndRow(14, $rowIndex, $memberEntry->getUserRole());
    }


    /**
     * Add all company data fields to the work sheet line.
     *
     * @param Worksheet   $sheet       Work sheet to add the data to
     * @param int         $rowIndex    Row to write the data to
     * @param MemberEntry $memberEntry Data source
     */
    private function addCompanyData(Worksheet $sheet, int $rowIndex, MemberEntry $memberEntry): void
    {
        $company = $memberEntry->getCompanyInformation();
        if ($company) {
            $columnIndex = $this->indexOfCompanyInformation;

            $sheet->setCellValueByColumnAndRow($columnIndex + 0, $rowIndex, $company->getName())
                  ->setCellValueByColumnAndRow($columnIndex + 1, $rowIndex, $company->getCity())
                  ->setCellValueByColumnAndRow($columnIndex + 2, $rowIndex, $company->getDescription())
                  ->setCellValueByColumnAndRow($columnIndex + 3, $rowIndex, $company->getFunctionDescription())
                  ->setCellValueByColumnAndRow($columnIndex + 4, $rowIndex, $company->getUrl());
        }
    }


    /**
     * Add phone number fields to the work sheet line.
     *
     * @param Worksheet   $sheet       Work sheet to add the data to
     * @param int         $rowIndex    Row to write the data to
     * @param MemberEntry $memberEntry Data source
     */
    private function addPhoneNumberData(Worksheet $sheet, int $rowIndex, MemberEntry $memberEntry): void
    {
        $phoneNumbers = $memberEntry->getPhoneNumbersAsArray();
        if (count($phoneNumbers)) {
            $columnIndex = $this->indexOfPhoneNumbers;
            foreach ($phoneNumbers as $phoneNumber) {
                $sheet->setCellValueByColumnAndRow($columnIndex, $rowIndex, $phoneNumber['type'])
                    ->setCellValueExplicitByColumnAndRow(
                        $columnIndex + 1,
                        $rowIndex,
                        $phoneNumber['phone_number'],
                        DataType::TYPE_STRING
                    );
                $columnIndex += 2;
            }
        }
    }


    /**
     * Add address fields to the work sheet line.
     *
     * @param Worksheet   $sheet       Work sheet to add the data to
     * @param int         $rowIndex    Row to write the data to
     * @param MemberEntry $memberEntry Data source
     */
    private function addAddressData(Worksheet $sheet, int $rowIndex, MemberEntry $memberEntry): void
    {
        $addresses = $memberEntry->getAddressesAsArray();
        if (count($addresses)) {
            $columnIndex = $this->indexOfAddresses;
            foreach ($addresses as $address) {
                $sheet->setCellValueByColumnAndRow($columnIndex, $rowIndex, $address['is_main_address'] ? 'yes' : 'no')
                      ->setCellValueByColumnAndRow($columnIndex + 1, $rowIndex, $address['type'])
                      ->setCellValueByColumnAndRow($columnIndex + 2, $rowIndex, $address['address'])
                      ->setCellValueExplicitByColumnAndRow($columnIndex + 3, $rowIndex, $address['zip'], DataType::TYPE_STRING)
                      ->setCellValueByColumnAndRow($columnIndex + 4, $rowIndex, $address['city'])
                      ->setCellValueByColumnAndRow($columnIndex + 5, $rowIndex, $address['country']);
                $columnIndex += 6;
            }
        }
    }


    /**
     * Add committee function fields to the work sheet line.
     *
     * @param Worksheet   $sheet       Work sheet to add the data to
     * @param int         $rowIndex    Row to write the data to
     * @param MemberEntry $memberEntry Data source
     */
    private function addCommitteeFunctions(Worksheet $sheet, int $rowIndex, MemberEntry $memberEntry): void
    {
        $committeeFunctions = $memberEntry->getCommitteeFunctionMaps();
        if (count($committeeFunctions)) {
            $columnIndex = $this->indexOfCommitteeFunctions;
            /** @var CommitteeFunctionMap $committeeFunction */
            foreach ($committeeFunctions as $committeeFunction) {
                $sheet->setCellValueByColumnAndRow($columnIndex, $rowIndex, (string) $committeeFunction->getCommittee())
                      ->setCellValueByColumnAndRow($columnIndex + 1, $rowIndex, (string) $committeeFunction->getCommitteeFunction());
                $columnIndex += 2;
            }
        }
    }


    /**
     * Add group member fields to the work sheet line.
     *
     * @param Worksheet   $sheet       Work sheet to add the data to
     * @param int         $rowIndex    Row to write the data to
     * @param MemberEntry $memberEntry Data source
     */
    private function addGroupMembers(Worksheet $sheet, int $rowIndex, MemberEntry $memberEntry): void
    {
        $groupMembers = $memberEntry->getGroupMembers();
        if (count($groupMembers)) {
            $columnIndex = $this->indexOfGroupMembers;
            /** @var MemberEntry $groupMember */
            foreach ($groupMembers as $groupMember) {
                $sheet->setCellValueByColumnAndRow($columnIndex, $rowIndex, $groupMember->getName())
                      ->setCellValueByColumnAndRow($columnIndex + 1, $rowIndex, $groupMember->getFirstName())
                      ->setCellValueByColumnAndRow($columnIndex + 2, $rowIndex, $groupMember->getEmail());
                $columnIndex += 3;
            }
        }
    }


    /**
     * Add the headers for an entry which has a dynamic number of entries. For each block, there is a main header with a
     * number in row 1, which is merged over all of the entries fields. In line 2 al fields are written. Example:
     * | Phone number 1                   | Phone number 2                   |
     * | Phone number type | Phone number | Phone number type | Phone number |
     *
     * @param Spreadsheet $spreadsheet
     * @param int         $columnIndex     Column index where to start writing the fields.
     * @param int         $entryCounter    Number of entries for this header section.
     * @param array       $headerInfo      Part of ExcelDataFormats::DYNAMIC_HEADERS to use.
     * @param bool        $suppressCounter When true, the main header in row 1 is written without number.
     *
     * @return int Column index where to write the next header to.
     */
    private function addDynamicHeaderEntry(Spreadsheet $spreadsheet, int $columnIndex, int $entryCounter, array $headerInfo, bool $suppressCounter = false): int
    {
        if ($entryCounter) {
            $numberOfFields = count($headerInfo['fields']);
            for ($entryIndex = 1; $entryIndex <= $entryCounter; ++$entryIndex) {
                $header = $headerInfo['header'];
                if (!$suppressCounter) {
                    $header .= " $entryIndex";
                }
                $sheet = $spreadsheet->getActiveSheet()
                    ->setCellValueByColumnAndRow($columnIndex, 1, $header)
                    ->mergeCellsByColumnAndRow($columnIndex, 1, $columnIndex + $numberOfFields - 1, 1);
                foreach ($headerInfo['fields'] as $header) {
                    $sheet->setCellValueByColumnAndRow($columnIndex, 2, $header);
                    ++$columnIndex;
                }
            }
        }

        return $columnIndex;
    }
}
