<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Service;

use App\Entity\Language;
use App\Form\ValueFormType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\Mapping\MappingException;
use ReflectionException;
use Symfony\Component\Form\FormFactory;

/**
 * This class generates the form data for simple entities (those without references to other tables). With this form
 * existing objects can be removed and new objects can be added.
 *
 * Class AddRemoveFormGenerator
 */
class AddRemoveFormGenerator
{
    private FormFactory $formFactory;
    private EntityManagerInterface $entityManager;
    private string $defaultLocale;


    /**
     * Create AddRemoveFormGenerator object.
     *
     * @param FormFactory            $formFactory
     * @param EntityManagerInterface $entityManager
     * @param string                 $defaultLocale
     */
    public function __construct(FormFactory $formFactory, EntityManagerInterface $entityManager, string $defaultLocale)
    {
        $this->formFactory   = $formFactory;
        $this->entityManager = $entityManager;
        $this->defaultLocale = $defaultLocale;
    }


    /**
     * Create a widget (all items of a certain propel type and a form for a new object). This widget shows all available
     * objects of this type and offers a way to delete them and to create new ones.
     *
     * @param string     $type          Doctrine class type (e.g. Status)
     * @param string     $tableCaption  String containing the caption above the table with the found objects.
     * @param array|null $processedForm Array with the elements 'form' and 'type' containing the processed form if the
     *                                  method is called via the addAction.
     *
     * @return array Complete widget used by the twig template.
     */
    public function createShowAddDeleteWidget(string $type, string $tableCaption, ?array $processedForm): array
    {
        $locales = null;
        if ($this->isTranslatable($type)) {
            $locales = $this->entityManager->getRepository(Language::class)->findAllLocales($this->defaultLocale);
        }

        if ((!is_null($processedForm)) && ($processedForm['type'] === $type)) {
            /// @todo: this is probably needed when there are form errors. It is not clear, if these errors can occur at all.
            $form = $processedForm['form'];
        } else {
            $form = $this->formFactory->create(ValueFormType::class, null, [
                'entityType' => $type,
                'entityManager' => $this->entityManager,
                'locales' => $locales,
            ]);
        }

        $objectList = $this->entityManager->getRepository('App\\Entity\\'.$type)->findAll();
        $widget = [
            'articles'    => $objectList,
            'form'        => $form->createView(),
            'caption'     => $tableCaption,
            'object_type' => $type,
            'headers'     => $this->generateHeaders($type, $locales),
            'locales'     => $locales,
        ];


        return ($widget);
    }


    /**
     * Create the headers for the type objects columns. When the main column is not to be translated, then there will be
     * only the headers for the class fields, except "id" and "slug". When the main column has to be translated (i.e.
     * when the objects uses the Translatable behaviour, then the main field is generated for each language. To be able
     * to translate the headers, they are generated as string looking like "field value|locale".
     *
     * @param string        $type    Entity type (without namespace) for which the headers are to be generated.
     * @param string[]|null $locales List of locales for which the headers are to be generated, The default locale must
     *                               be in the first place.
     *
     * @return array Headers for the table
     *
     * @throws MappingException
     * @throws ReflectionException
     */
    private function generateHeaders(string $type, ?array $locales): array
    {
        $typeWithNamespace = 'App\\Entity\\'.$type;
        // Read all the headers for the given object and remove the headers "id" and "slug"
        $headers = $this->entityManager->getMetadataFactory()
                                       ->getMetadataFor($typeWithNamespace)
                                       ->getFieldNames();
        unset($headers[array_search('id', $headers)]);
        unset($headers[array_search('slug', $headers)]);
        // Unset left the array with non-continuous indexes. To fix this, create a new array.
        $headers = array_values($headers);

        // If entity value has to be translated, change the main header to be translated with the default locale and
        // add headers for all the other locales.
        if (!is_null($locales)) {
            $headerWithLocales = [];
            foreach ($locales as $locale) {
                $headerWithLocales[] = $this->addTranslatableLocale($headers[0], $locale);
            }
            array_splice($headers, 0, 1, $headerWithLocales);
        }

        return $headers;
    }


    /**
     * In the twig output for the header fields, there is a special handler for translating a field with several
     * locales. This methods writes the header in this special format.
     *
     * @param string $translateField
     * @param string $locale
     *
     * @return string
     */
    private function addTranslatableLocale(string $translateField, string $locale): string
    {
        return $translateField.'|'.$locale;
    }


    /**
     * Returns true if the entity type uses the Translatable behaviour.
     *
     * @param string $type
     *
     * @return bool
     */
    private function isTranslatable(string $type): bool
    {
        // To check if entity value must be translated, check if it used the Translatable behaviour by searching for one
        // of the properties defined there.
        return property_exists('Core\DataBundle\Entity\\'.$type, 'translations');
    }
}
