<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Security;

use App\Service\LogMessageCreator;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;

/**
 * Class LoginFormAuthenticator
 */
class LoginFormAuthenticator extends AbstractAuthenticator
{
    private RouterInterface        $router;
    private LogMessageCreator      $logMessageCreator;
    private EntityManagerInterface $entityManager;


    /**
     * LoginFormAuthenticator constructor.
     *
     * @param RouterInterface        $router
     * @param LogMessageCreator      $logMessageCreator
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(RouterInterface $router, LogMessageCreator $logMessageCreator, EntityManagerInterface $entityManager)
    {
        $this->router = $router;
        $this->logMessageCreator = $logMessageCreator;
        $this->entityManager = $entityManager;
    }


    /**
     * Does the authenticator support the given Request?
     *
     * If this returns false, the authenticator will be skipped.
     *
     * Returning null means authenticate() can be called lazily when accessing the token storage.
     *
     * @param Request $request
     *
     * @return bool|null
     */
    public function supports(Request $request): ?bool
    {
        return $request->get('_route') === 'security_login'
            && $request->isMethod('POST');
    }


    /**
     * Create a passport for the current request.
     *
     * The passport contains the user, credentials and any additional information
     * that has to be checked by the Symfony Security system. For example, a login
     * form authenticator will probably return a passport containing the user, the
     * presented password and the CSRF token value.
     *
     * You may throw any AuthenticationException in this method in case of error (e.g.
     * a UserNotFoundException when the user cannot be found).
     *
     * @param Request $request
     *
     * @return Passport
     */
    public function authenticate(Request $request): Passport
    {
        $userName = $request->request->all('login_form')['_username'];

        // Pass last given username to the login form in case the login failed. The username is stored in the session
        // and passed automatically to the username form field.
        $request->getSession()->set(Security::LAST_USERNAME, $userName);

        return new Passport(
            new UserBadge($userName),
            new PasswordCredentials($request->request->all('login_form')['_password']),
            [
                new CsrfTokenBadge('login_form', $request->request->all('login_form')['_token']),
            ]
        );
    }


    /**
     * Called when authentication executed and was successful!
     *
     * This should return the Response sent back to the user, like a
     * RedirectResponse to the last page they visited.
     *
     * If you return null, the current request will continue, and the user
     * will be authenticated. This makes sense, for example, with an API.
     *
     * @param Request                 $request
     * @param AuthenticationException $exception
     *
     * @return Response|null
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);

        try {
            $message = 'Login failure with message %message% and user name %user%|'.$exception->getMessage().','.$request->request->all('login_form')['_username'];
            $this->logMessageCreator->createLogEntryWithoutLoggedInUser('login error', $message);
            $this->entityManager->flush();
        } catch (Exception $e) {
            // Left intentionally blank: failing to log cannot be handled otherwise
        }

        return new RedirectResponse($this->router->generate('security_login'));
    }


    /**
     * Called when authentication executed, but failed (e.g. wrong username password).
     *
     * This should return the Response sent back to the user, like a
     * RedirectResponse to the login page or a 403 response.
     *
     * If you return null, the request will continue, but the user will
     * not be authenticated. This is probably not what you want to do.
     *
     * @param Request        $request
     * @param TokenInterface $token
     * @param string         $firewallName
     *
     * @return Response|null
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        // Read the preferred language from the current user and write it to the sesstion
        $user = $token->getUser();
        if (null !== $user->getPreferredLanguage()) {
            $request->getSession()->set('_locale', $user->getPreferredLanguage()->getLocale());
        }

        try {
            $this->logMessageCreator->setUser($user);
            $this->logMessageCreator->createLogEntry('login', 'Login succeeded', $user);
            $this->entityManager->flush();
        } catch (Exception $e) {
            // Left intentionally blank: failing to log cannot be handled otherwise
        }

        return new RedirectResponse($this->router->generate('dashboard_homepage'));
    }
}
