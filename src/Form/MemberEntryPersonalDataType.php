<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use App\Entity\MemberEntry;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\AtLeastOneOf;
use Symfony\Component\Validator\Constraints\Blank;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class MemberEntryPersonalDataType
 */
class MemberEntryPersonalDataType extends AbstractType
{
    /**
     * {@inheritdoc}
    */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('firstName', TextType::class, [
                'empty_data' => '-',
            ])
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    'Male' => 'm',
                    'Female' => 'f',
                    'Diverse' => 'd',
                    'None' => 'n',
                ],
                'empty_data'  => null,
                'expanded' => true,
                'multiple' => false,
            ])
        ->add('birthday', BirthdayType::class, array('required' => false, 'format' => 'dd-MM-yyyy'))
        ->add('title')
        ->add('plainPassword', RepeatedType::class, [
            'type' => PasswordType::class,
            'invalid_message' => 'The password fields must match.',
            'constraints' => [
                new AtLeastOneOf([
                    new Length(['min' => 10]), // This checks for unicode number of characters, which is what we want!
                    new Blank(),
                ]),
                new Callback([$this, 'validatePasswordComplexity']),
            ],
        ])
        ->add('preferredLanguage') // TODO This field is required, but can be set to null in the form.
        ;
    }


    /**
     * This method checks the password for several complexity rules:
     * 1. There have to be at least 4 different characters
     * 2. The username cannot be part of the password
     * 3. The email address cannot be part of the password
     * 4. The first or last name cannot be part of the password
     *
     * @param string|null               $newPassword
     * @param ExecutionContextInterface $context
     */
    public function validatePasswordComplexity(?string $newPassword, ExecutionContextInterface $context): void
    {
        if (!is_null($newPassword)) {
            // 1. Find the number of different characters and shown an error when this is not at least 4
            // preg_split also takes utf8-characters into account! Use flag PREG_SPLIT_NO_EMPTY to also count spaces
            $numberOfUniqueChars = count(array_unique(preg_split('//u', $newPassword, -1, PREG_SPLIT_NO_EMPTY)));
            if ($numberOfUniqueChars < 4) {
                $context
                    ->buildViolation('This value is not complex enough. It should have 4 different characters or more.')
                    ->addViolation();
            }

            /** @var MemberEntry $member */
            $member = $context->getRoot()->getData();
            // 2. Search for username. The username is generated as a lower case string at the moment. Just to be sure
            // for the future, convert it to lower case here.
            $this->validateStringInPassword($context, $newPassword, $member->getUsername(), 'This value should not contain the user name.');

            // 3. Search for users email address. To find all variants of upper and lower case addresses, convert it to
            // lower case here.
            $this->validateStringInPassword($context, $newPassword, $member->getEmail(), 'This value should not contain the users email address.');

            // 4. Search for users first name. To find all variants of upper and lower case names, convert it to lower
            // case here.
            $this->validateStringInPassword($context, $newPassword, $member->getFirstName(), 'This value should not contain the users first name.');

            // 4. Search for users last name. To find all variants of upper and lower case names, convert it to lower
            // case here.
            $this->validateStringInPassword($context, $newPassword, $member->getName(), 'This value should not contain the users last name.');
        }
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MemberEntry::class,
        ]);
    }


    /**
     * Search for a string in the new password and generate a validation error if it is found
     *
     * @param ExecutionContextInterface $context      Validation context
     * @param string                    $newPassword  New password to search in
     * @param string                    $stringToFind String to find in the password
     * @param string                    $errorMessage Error message to use when the string is found
     */
    private function validateStringInPassword(ExecutionContextInterface $context, string $newPassword, string $stringToFind, string $errorMessage): void
    {
        // Use to lower to make sure that a simple change to upper case doesn't circumvent the checks
        if (false !== strpos(strtolower($newPassword), strtolower($stringToFind))) {
            $context
                ->buildViolation($errorMessage)
                ->addViolation();
        }
    }
}
