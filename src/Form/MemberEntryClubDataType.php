<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use App\Entity\CommitteeFunctionMap;
use App\Entity\MemberEntry;
use App\Entity\MembershipType;
use App\Entity\Status;
use App\Entity\UserRole;
use App\Repository\MemberEntryRepository;
use App\Repository\MembershipTypeRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MemberEntryClubDataType
 */
class MemberEntryClubDataType extends AbstractType
{
    private ?MemberEntry $selectedMember = null;


    /**
     * {@inheritdoc}
    */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $this->selectedMember = $options['selected_member'];
        $hasGroupMembers = $this->selectedMember->getGroupMembers()->count() > 0;

        $builder
            ->add('memberSince', ChoiceType::class, [
                'choices' => array_combine(range(date('Y'), 1950), range(date('Y'), 1950)),
                'required' => false,
            ])
            ->add('membershipEndRequested', CheckboxType::class, [
                'required' => false,
            ])
            ->add('status', EntityType::class, [
                'class' => Status::class,
                'required' => true,
            ])
            ->add('membershipType', EntityType::class, [
                'class' => MembershipType::class,
                'required' => true,
                'query_builder' => function (MembershipTypeRepository $repository) use ($options, $hasGroupMembers) {
                    return ($repository->createFindByGroupMembershipQueryBuilder($options['current_locale'], $hasGroupMembers));
                },
            ])
            ->add('userRole', EntityType::class, [
                'class' => UserRole::class,
                'required' => true,
            ])
            ->add('committeeFunctionMaps', EntityType::class, [
                'class' => CommitteeFunctionMap::class,
                'expanded' => true,
                'multiple' => true,
                'choice_label' => 'committeeFunction',
                'by_reference' => false,
            ])
            ->add('groupMembers', EntityType::class, [
                'class' => MemberEntry::class,
                'expanded' => true,
                'multiple' => true,
                'by_reference' => false,
                'query_builder' => function (MemberEntryRepository $repository) {
                    return ($repository->createFindAllWithSameAddressesAndLastNameQueryBuilder($this->selectedMember));
                },
            ])
            ->add('membershipNumber', CheckboxType::class, [
                'label' => 'use direct debit',
                'required' => false,
            ])
        ;

        $membershipNumber = $builder->getData()->getMembershipNumber();
        $builder->get('membershipNumber')
            ->addModelTransformer(new CallbackTransformer(
                function ($membershipNumberAsEntity) {
                    // When this member has a membership number (i.e. this really is a member), then return the setting
                    // for direct debit.
                    if ($membershipNumberAsEntity) {
                        return $membershipNumberAsEntity->getUseDirectDebit();
                    }

                    return false;
                },
                function ($membershipNumberAsBool) use ($membershipNumber) {
                    // When this member has a membership number (i.e. this really is a member), then update the setting
                    // for direct debit.
                    if ($membershipNumber) {
                        $membershipNumber->setUseDirectDebit($membershipNumberAsBool);
                    }

                    // This can also be null
                    return $membershipNumber;
                }
            ))
        ;
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MemberEntry::class,
            'selected_member' => null,
            'current_locale' => null,
        ]);
    }
}
