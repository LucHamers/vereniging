<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class EmailTemplateType
 */
class EmailTemplateType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $template = $options['template'];
        $locales = $options['locales'];

        foreach ($locales as $locale) {
            $templateName = $template->translate($locale, false)->getTemplateNameTranslated();
            $senderAddress = $template->translate($locale, false)->getSenderAddressTranslated();

            $builder->add("templateName_$locale", TextType::class, [
                'required' => false,
                'data' => $templateName,
                'label' => false,
            ]);

            $builder->add("senderAddress_$locale", EmailType::class, [
                'required' => false,
                'data' => $senderAddress,
                'label' => false,
                'constraints' => [
                    new Callback([$this, 'validateEmailAddress']),
                ],
            ]);
        }
        $builder->add('templateContent', TextareaType::class, [
            'data' => $template->getContent(),
        ]);
    }


    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'template' => null,
            'locales' => null,
        ]);
    }


    /**
     * This checks if the email address ist correct. It can have two different forms:
     * test@email.com
     * Test User <test@email.com>
     *
     * @param string|null               $emailAddress
     * @param ExecutionContextInterface $context
     *
     * @return void
     */
    public function validateEmailAddress(?string $emailAddress, ExecutionContextInterface $context): void
    {
        if (!is_null($emailAddress)) {
            // If the email address matches "bla <test@email.com>", then filter out the email address in between < >
            if (preg_match('#\S+\s*<(.*)>#', $emailAddress, $matches)) {
                $emailAddress = $matches[1];
            }
            if (!filter_var($emailAddress, FILTER_VALIDATE_EMAIL)) {
                $context
                    ->buildViolation('Email validation error')
                    ->addViolation();
            }
        }
    }
}
