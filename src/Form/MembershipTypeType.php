<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Form;

use App\Entity\MembershipType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MembershipTypeType
 */
class MembershipTypeType extends AbstractType
{
    private array $locales;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $this->locales = $options['locales'];

        // Create a form field for each field in the entity. Use an event listener, because the translation fields must
        // be added directly after the type field AND must be initialised with the current translation values, which can
        // only be read from the entity.
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                $form = $event->getForm();
                $membershipType = $event->getData();
                $form->add('type', TextType::class, [
                    'label' => false,
                    'required' => true,
                ]);

                // Add translation fields for main value (if the entity type is translatable)
                $this->addTranslationFields($form, $membershipType);

                $form
                    ->add('membershipFee', MoneyType::class, [
                        'label' => false,
                    ])
                    ->add('groupMembership', CheckboxType::class, [
                        'label' => false,
                    ])
                    ->add('defaultGroupMembershipType', RadioType::class, [
                        'label' => false,
                        'attr' => [
                            'class' => 'toggle1',
                            'novalidate' => 'novalidate',
                        ],
                    ])
                    ->add('defaultNewMembershipType', RadioType::class, [
                        'label' => false,
                        'attr' => [
                            'class' => 'toggle2',
                            'novalidate' => 'novalidate',
                        ],
                    ])
                ;
            }
        );
    }


    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MembershipType::class,
            'locales' => null,
            // This is needed to add the extra fields with the translated values.
            'allow_extra_fields' => true,

        ]);
    }


    /**
     * Add translation fields to the field, when the entity type is translatable.
     *
     * @param FormInterface  $form           Form to which the fields are to be added.
     * @param MembershipType $membershipType Entity which is used to initialise the translation fields.
     */
    private function addTranslationFields(FormInterface $form, ?MembershipType $membershipType): void
    {
        // Use a copy of the locales property, to not change that. Remove default locale, since this is
        // already covered by adding the main field.
        $locales = $this->locales;
        unset($locales[0]);
        foreach ($locales as $locale) {
            // These additional translation fields are not mapped to the entity. They will be handled as
            // extra fields in the controller and added to the translation of the entity by hand, not by
            // the form system.
            $data = null;
            if ($membershipType) {
                $data = $membershipType->translate($locale, false)->getTypeTranslated();
            }
            $form->add('type_'.$locale, TextType::class, [
                'mapped' => false,
                'required' => false,
                'data' => $data,
                'label' => false,
            ]);
        }
    }
}
