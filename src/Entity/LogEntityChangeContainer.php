<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

/**
 * This class is used by any doctrine object to store all fields which are important to the user. After changing such an
 * object, a second log entity container object is created. These objects are then compared to each other to generate a
 * log message conaining all diffs for the transaction.
 *
 * Class LogEntityChangeContainer
 */
class LogEntityChangeContainer
{
    /** @var  string Name of the class for which this container holds the data */
    private $className;
    /** @var  int Database index number */
    private $index;
    /** @var  string Change type for the class for which this container holds the data */
    private $changeType;
    /** @var  string String to identify the object in the log file when the object contains more than one field */
    private $stringIdentifier;
    /** @var  int|null This contains the member objects index if this object is linked to a member object */
    private $changesOnMemberId;
    /** @var ChangeEntry[]|array List of object properties to compare */
    private $content;

    /**
     * LogEntityChangeContainer constructor.
     */
    public function __construct()
    {
        $this->changesOnMemberId = null;
        $this->stringIdentifier = null;
        $this->content = [];
    }

    /**
     * @return string
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * @param string $className
     */
    public function setClassName(string $className)
    {
        // Strip the namespace part of the class name.
        $this->className = strtolower(substr($className, strrpos($className, '\\') + 1));
    }

    /**
     * @return int
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @param int $index
     */
    public function setIndex(int $index = null)
    {
        $this->index = $index;
    }

    /**
     * @return array
     */
    public function getContent(): array
    {
        return $this->content;
    }

    /**
     * Short content is shown when entities are added or deleted.
     *
     * @param array $shortContent
     */
    public function setShortContent(array $shortContent)
    {
        foreach ($shortContent as $key => $value) {
            $this->addContentItem($key, false, $value);
        }
    }

    /**
     * Extended content is only shown when entities are changed, not when they are added or deleted.
     *
     * @param array $extendedContent
     */
    public function setExtendedContent(array $extendedContent)
    {
        foreach ($extendedContent as $key => $value) {
            $this->addContentItem($key, true, $value);
        }
    }

    /**
     * This adds an item to one of the content lists.
     *
     * @param string                       $key               Key unter which the property is stored
     * @param bool                         $isExtendedContent True when the item is to be added to the extended content
     *                                                        list. Extented content is only shown for entity changes,
     *                                                        not when an entity is added or deleted.
     * @param string|EntityLoggerInterface $content           Either a simple text or an EntityLoggerInterface object
     *
     * @return LogEntityChangeContainer This object to be used in a fluent interface
     */
    public function addContentItem(string $key, bool $isExtendedContent, $content)
    {
        $this->content[$key] = new ChangeEntry($isExtendedContent, $content);

        return $this;
    }

    /**
     * @return string
     */
    public function getChangeType(): ?string
    {
        return $this->changeType;
    }

    /**
     * @param string $changeType
     */
    public function setChangeType(string $changeType)
    {
        $this->changeType = $changeType;
    }

    /**
     * @return string
     */
    public function getStringIdentifier(): ?string
    {
        return $this->stringIdentifier;
    }

    /**
     * @param string $stringIdentifier
     */
    public function setStringIdentifier(string $stringIdentifier)
    {
        $this->stringIdentifier = $stringIdentifier;
    }

    /**
     * @return int|null
     */
    public function getChangesOnMemberId()
    {
        return $this->changesOnMemberId;
    }

    /**
     * @param int|null $changesOnMemberId
     */
    public function setChangesOnMemberId($changesOnMemberId)
    {
        $this->changesOnMemberId = $changesOnMemberId;
    }

    /**
     * @param LogEntityChangeContainer $other
     *
     * @return AbstractComplexLogEntryBase
     *
     * @throws \Exception
     */
    public function diffObjects(LogEntityChangeContainer $other)
    {
        // When the current object is null, then the other object contains all the data
        if (is_null($this->index)) {
            $diffObject = $this->diffWithEmptyThis($other);
            // When the other object is null, then the current object contains all the data
        } elseif (is_null($other->index)) {
            $diffObject = $this->diffWithEmptyOther();
            // Bot objects contain data and must be merged. The shortContent array will contain only the fields from
            // shortContent and extendedContent which differ
        } else {
            $diffObject = $this->diffWithBothContents($other);
        }

        return $diffObject;
    }

    /**
     * This method is called when an entity is created. It has no prior values, only current ones, which are stored in
     * $other.
     *
     * @param LogEntityChangeContainer $other
     *
     * @return ComplexLogEntrySingleDataField
     *
     * @throws \Exception
     */
    private function diffWithEmptyThis(LogEntityChangeContainer $other): ComplexLogEntrySingleDataField
    {
        $content = new ComplexLogEntrySingleDataField();
        /** @var ChangeEntry $value */
        foreach ($other->content as $key => $value) {
            if (false === $value->isExtendedContent()) {
                $content->addDataField($key, [$value->getContent()]);
            }
        }

        return $content;
    }

    /**
     * @return ComplexLogEntrySingleDataField
     *
     * @throws \Exception
     */
    private function diffWithEmptyOther()
    {
        $content = new ComplexLogEntrySingleDataField();
        /** @var ChangeEntry $value */
        foreach ($this->content as $key => $value) {
            if (false === $value->isExtendedContent()) {
                $content->addDataField($key, [$value->getContent()]);
            }
        }

        return $content;
    }

    /**
     * @param LogEntityChangeContainer $other
     *
     * @return ComplexLogEntryDoubleDataField
     *
     * @throws \Exception
     */
    private function diffWithBothContents(LogEntityChangeContainer $other)
    {
        $content = new ComplexLogEntryDoubleDataField();
        foreach ($this->content as $key => $value) {
            // phpcs:ignore
            if ($value != $other->content[$key]) {
                $content->addDataField($key, [$value->getContent(), $other->content[$key]->getContent()]);
            }
        }

        return $content;
    }
}
