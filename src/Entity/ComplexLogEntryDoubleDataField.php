<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

/**
 * Class for complex log entries where the value consists of two data fields. This type of log entry looks like this:
 * ["main message", ['name' => ['value1', 'value2'], 'first name' => ['value3', 'value4']]]
 *
* Class ComplexLogEntryDoubleDataField
 */
class ComplexLogEntryDoubleDataField extends AbstractComplexLogEntryBase
{
    /**
     * Returns the number of data fields
     *
     * @return int
     */
    public function getNumberOfDataFields()
    {
        return 2;
    }

    /**
     * This returns the data as an associative array, which looks like this:
     * ['name' => ['old name', 'new name'], 'first name' => ['old name', 'new name']]
     * When the translator is set, then keys and values are translated.
     *
     * @param bool $translateContent When true, the content is translated. When false, the raw data is returned.
     *
     * @return array When returning the raw data array to be changed, read it like this: $data = &$object->getDataAsArray(false);
     */
    public function &getDataAsArray(bool $translateContent = true)
    {
        if (!$translateContent) {
            return $this->dataArray;
        }

        $resultingArray = $this->translateArrayKeys();

        foreach ($resultingArray as $key => $value) {
            $resultingArray[$key] = [
                $this->translateLogEntryDataField($value[0]),
                $this->translateLogEntryDataField($value[1]),
                ];
        }

        return $resultingArray;
    }


    /**
     * The data array passed to the constructor must be deserialised for each field, which has to be done in the derived
     * classes. The derived method has to write it to the data array of the base class.
     *
     * @param array $dataArray
     *
     * @throws \Exception
     */
    protected function unserialiseDataFields(array $dataArray)
    {
        foreach ($dataArray as $key => $value) {
            $this->dataArray[$key] = $value;
            // When the content is an array, it is a serialised log entry data field and must be converted to an object
            if (is_array($value[0])) {
                $this->dataArray[$key] = [
                    new LogEntryDataField(json_encode($value[0])),
                    new LogEntryDataField(json_encode($value[1])),
                    ];
            }
        }
    }
}
