<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * App\Entity\SerialLetter
 *
 * @ORM\Entity(repositoryClass="App\Repository\SerialLetterRepository")
 * @ORM\Table(name="serial_letter", indexes={@ORM\Index(name="fk_serial_letter_member_entry1",
 * columns={"first_signature_member_id"}), @ORM\Index(name="fk_serial_letter_member_entry2",
 * columns={"second_signature_member_id"})})
 */
class SerialLetter implements EntityLoggerInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="smallint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="date")
     *
     * @Gedmo\Timestampable(on="create")
     */
    protected $createDate;

    /**
     * @ORM\Column(type="date")
     *
     * @Gedmo\Timestampable(on="update")
     */
    protected $changeDate;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $signatureUseTitle;

    /**
     * @ORM\OneToMany(targetEntity="SerialLetterContent", mappedBy="serialLetter", cascade={"remove"})
     * @ORM\JoinColumn(name="id", referencedColumnName="serial_letter_id")
     */
    protected $serialLetterContents;

    /**
     * @ORM\ManyToOne(targetEntity="MemberEntry")
     * @ORM\JoinColumn(name="first_signature_member_id", referencedColumnName="id", nullable=true)
     */
    protected $firstSignatureMember;

    /**
     * @ORM\ManyToOne(targetEntity="MemberEntry")
     * @ORM\JoinColumn(name="second_signature_member_id", referencedColumnName="id", nullable=true)
     */
    protected $secondSignatureMember;

    /**
     * @ORM\ManyToOne(targetEntity="CommitteeFunctionMap")
     * @ORM\JoinColumn(name="fist_signature_function_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $firstSignatureMembersFunction;

    /**
     * @ORM\ManyToOne(targetEntity="CommitteeFunctionMap")
     * @ORM\JoinColumn(name="second_signature_function", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $secondSignatureFunction;

    /**
     * @ORM\ManyToOne(targetEntity="MemberEntry")
     * @ORM\JoinColumn(name="last_updated_by_id", referencedColumnName="id", nullable=false)
     */
    protected $lastUpdatedBy;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->serialLetterContents = new ArrayCollection();
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of create_date.
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Get the value of change_date.
     *
     * @return \DateTime
     */
    public function getChangeDate()
    {
        return $this->changeDate;
    }

    /**
     * Set the value of signature_use_title.
     *
     * @param boolean $signatureUseTitle
     */
    public function setSignatureUseTitle($signatureUseTitle)
    {
        $this->signatureUseTitle = $signatureUseTitle;
    }

    /**
     * Get the value of signature_use_title.
     *
     * @return boolean
     */
    public function getSignatureUseTitle()
    {
        return $this->signatureUseTitle;
    }

    /**
     * Add SerialLetterContent entity to collection (one to many).
     *
     * @param SerialLetterContent $serialLetterContent
     */
    public function addSerialLetterContent(SerialLetterContent $serialLetterContent)
    {
        $this->serialLetterContents[] = $serialLetterContent;
        $serialLetterContent->setSerialLetter($this);
    }

    /**
     * Get SerialLetterContent entity collection (one to many).
     *
     * @return \Doctrine\Common\Collections\Collection|SerialLetterContent[]
     */
    public function getSerialLetterContents()
    {
        return $this->serialLetterContents;
    }

    /**
     * Set MemberEntry entity related by `first_signature_member_id` (many to one).
     *
     * @param MemberEntry $memberEntry
     */
    public function setFirstSignatureMember(MemberEntry $memberEntry = null)
    {
        $this->firstSignatureMember = $memberEntry;
    }

    /**
     * Get MemberEntry entity related by `first_signature_member_id` (many to one).
     *
     * @return MemberEntry
     */
    public function getFirstSignatureMember()
    {
        return $this->firstSignatureMember;
    }

    /**
     * Set MemberEntry entity related by `second_signature_member_id` (many to one).
     *
     * @param MemberEntry $memberEntry
     */
    public function setSecondSignatureMember(MemberEntry $memberEntry = null)
    {
        $this->secondSignatureMember = $memberEntry;
    }

    /**
     * Get MemberEntry entity related by `second_signature_member_id` (many to one).
     *
     * @return MemberEntry
     */
    public function getSecondSignatureMember()
    {
        return $this->secondSignatureMember;
    }

    /**
     * @return CommitteeFunctionMap|null
     */
    public function getFirstSignatureMembersFunction()
    {
        return $this->firstSignatureMembersFunction;
    }

    /**
     * @param CommitteeFunctionMap|null $function
     */
    public function setFirstSignatureMembersFunction($function = null)
    {
        $this->firstSignatureMembersFunction = $function;
    }

    /**
     * @return CommitteeFunctionMap|null
     */
    public function getSecondSignatureMembersFunction()
    {
        return $this->secondSignatureFunction;
    }

    /**
     * @param CommitteeFunctionMap|null $function
     */
    public function setSecondSignatureMembersFunction($function = null)
    {
        $this->secondSignatureFunction = $function;
    }

    /**
     * @return MemberEntry
     */
    public function getLastUpdatedBy()
    {
        return $this->lastUpdatedBy;
    }

    /**
     * @param MemberEntry $lastUpdatedBy
     */
    public function setLastUpdatedBy(MemberEntry $lastUpdatedBy)
    {
        $this->lastUpdatedBy = $lastUpdatedBy;
    }

    /**
     * @param \DateTime $changeDate
     */
    public function setChangeDate(\DateTime $changeDate)
    {
        $this->changeDate = $changeDate;
    }


    /**
     * Get the title for this serial letter for the selected language. If the language is not found, return the title of
     * the first language found. If no content is found, return '',
     *
     * @param Language|string $language
     *
     * @return string Title in selected language, '' if not found.
     */
    public function getTitle($language)
    {
        $content = $this->getContent($language);
        if (is_null($content)) {
            return '';
        }

        return $content->getLetterTitle();
    }

    /**
     * Return the titles for this letter for each language as a string.
     *
     * @return string
     */
    public function getTitles()
    {
        $titles = '';

        /** @var SerialLetterContent $content */
        foreach ($this->serialLetterContents as $content) {
            $titles .= $content->getLanguage()->getLocale().': '.$content->getLetterTitle().'; ';
        }

        // Remove last semicolon
        if ('' !== $titles) {
            $titles = substr($titles, 0, -2);
        }

        return $titles;
    }

    /**
     * Get the content for this serial letter for the selected language. If the language is not found, return the
     * content of the first language found. If no content is found, return '',
     *
     * @param Language|string $language
     *
     * @return SerialLetterContent|null Content in selected language, null if not found.
     */
    public function getContent($language)
    {
        // $language can be either a string or a language object. If it is a language object, read the locale as string.
        if ((is_object($language)) && ('Language' === substr(get_class($language), -strlen('Language')))) {
            $language = $language->getLocale();
        }

        /** @var SerialLetterContent $content */
        foreach ($this->serialLetterContents as $content) {
            if ($content->getLanguage()->getLocale() === $language) {
                return $content;
            }
        }

        if (count($this->serialLetterContents)) {
            return $this->serialLetterContents[0];
        }

        return null;
    }

    /**
     * This method must return an associative array with the following fields:
     * className:   name of the class for which the content is saved
     * index:       index of the current entity
     * content:     array in which the fields of the entity and their values are stored.
     * change_type: string containing the change type to be logged.
     *
     * @return LogEntityChangeContainer
     *
     * @throws \Exception
     */
    public function getEntityContent()
    {
        $contentContainer = new LogEntityChangeContainer();
        $contentContainer->setClassName(get_class($this));
        $contentContainer->setIndex($this->id);
        $shortContent = [];
        /** @var SerialLetterContent $content */
        foreach ($this->serialLetterContents as $content) {
            $locale = $content->getLanguage()->getLocale();
            $shortContent["title_$locale"] = $content->getLetterTitle();
        }
        $contentContainer->setShortContent($shortContent);

        $extendedContent = [
            'signatureUseTitle' => new LogEntryDataField($this->signatureUseTitle ? 'yes' : 'no', true),
            'firstSignatureMember' => $this->firstSignatureMember ? $this->firstSignatureMember->getCompleteName() : '-',
            'secondSignatureMember' => $this->secondSignatureMember ? $this->secondSignatureMember->getCompleteName() : '-',
        ];
        /** @var SerialLetterContent $content */
        foreach ($this->serialLetterContents as $content) {
            $locale = $content->getLanguage()->getLocale();
            $extendedContent["content_$locale"] = $this->optimiseHtmlContentForDiff($content->getContent());
        }
        $contentContainer->setExtendedContent($extendedContent);
        $contentContainer->setChangeType('serial letter change');

        return $contentContainer;
    }


    /**
     * When the content is in html (which is the case when using the editor in the gui), then the following changes are
     * done to make it easier for the diff viewer and the log file:
     * variables are replaced by {{ variable-name }}
     * html parameters are removed
     *
     * @param string $content
     *
     * @return string|null
     */
    private function optimiseHtmlContentForDiff(?string $content): ?string
    {
        // Replace variables like <span class="mceNonEditable" style="border: thin dotted grey;" data-variable="firstName">firstName</span>
        // by {{ firstName }}
        preg_match_all("#.*(<span class=\"mceNonEditable\".+data-variable=\"(\w+)\".*>.+</span>)#U", $content, $matches);
        foreach ($matches[1] as $key => $variableBlock) {
            $content = str_replace($variableBlock, '{{ '.$matches[2][$key].' }}', $content);
        }

        // Remove all the parameters from html tags (e.g. change <td style="width: 15cm;"> to <td>). This is needed so
        // <ins> or <del> are not written into the middle of an html tag with parameters.
        $content = preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/si", '<$1$2>', $content);

        return $content;
    }
}
