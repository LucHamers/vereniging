<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use Knp\DoctrineBehaviors\Model\Translatable\TranslatableTrait;

/**
 * App\Entity\SerialLetter
 *
 * @ORM\Entity(repositoryClass="App\Repository\EmailTemplateRepository")
 * @ORM\Table(name="email_template")
 */
class EmailTemplate implements EntityLoggerInterface, EntityTranslationInterface, TranslatableInterface
{
    use TranslatableTrait;

    // In each template, the following block must be present, since this is what will be replaced on generating the
    // email.
    const CONTENT_VARIABLE = 'email content';
    const HTML_CONTENT_VARIABLE = '<span class="mceNonEditable" style="border: thin dotted grey;" data-variable="email content">'.self::CONTENT_VARIABLE.'</span>';
    const ALLOWED_TEMPLATE_TYPES = ["serial email", "login data", "change message"];

    /**
     * @ORM\Id
     * @ORM\Column(type="smallint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;


    /**
     * @ORM\Column(type="text")
     */
    protected string $templateName = '';


    /**
     * This is the actual html template. Each template must contain the html block which is to be replaced by the
     * content of the serial email.
     *
     * @ORM\Column(type="text")
     */
    protected string $content = self::HTML_CONTENT_VARIABLE;


    /**
     * This can be either "login data", "change message" or "serial email"
     *
     * @ORM\Column(type="text")
     */
    protected string $templateType = self::ALLOWED_TEMPLATE_TYPES[0];


    /**
     * The email address from which this email templates emails are sent.
     *
     * @ORM\Column(type="text")
     */
    protected string $senderAddress = '';

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * Set the value of templateTitle.
     *
     * @param string $templateName
     */
    public function setTemplateName(string $templateName): void
    {
        // When the template title is set for the first time or when it is changed, then update or set the default
        // translation.
        if ($this->templateName !== $templateName) {
            $this->translate($this->getDefaultLocale())->setTemplateNameTranslated($templateName);
        }

        $this->templateName = $templateName;
    }


    /**
     * Get the value of templateTitle.
     *
     * @param string|null $locale
     * @param bool        $getUntranslated When true, the original, untranslated value is returned.
     *
     * @return string
     */
    public function getTemplateName(?string $locale = null, bool $getUntranslated = false): string
    {
        if ($getUntranslated) {
            return $this->templateName;
        }

        if ($locale) {
            $this->setCurrentLocale($locale);
        }

        return $this->proxyCurrentLocaleTranslation('getTemplateNameTranslated', []);
    }


    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }


    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }


    /**
     * @return string
     */
    public function getTemplateType(): string
    {
        return $this->templateType;
    }


    /**
     * @param string $templateType
     */
    public function setTemplateType(string $templateType): void
    {
        if (!in_array($templateType, self::ALLOWED_TEMPLATE_TYPES)) {
            throw new InvalidArgumentException(sprintf('Template type "%s" not allowed, only values %s are allowed!', $templateType, json_encode(self::ALLOWED_TEMPLATE_TYPES)));
        }

        $this->templateType = $templateType;
    }


    /**
     * This method must return an associative array with the following fields:
     * className:   name of the class for which the content is saved
     * index:       index of the current entity
     * content:     array in which the fields of the entity and their values are stored.
     * change_type: string containing the change type to be logged.
     *
     * @return LogEntityChangeContainer
     *
     * @throws \Exception
     */
    public function getEntityContent(): LogEntityChangeContainer
    {
        $contentContainer = new LogEntityChangeContainer();
        $contentContainer->setClassName(get_class($this));
        $contentContainer->setIndex($this->id);
        $contentContainer->setShortContent(['title' => $this->templateName]);
        $contentContainer->setExtendedContent(['content' => $this->optimiseHtmlContentForDiff($this->content)]);
        $contentContainer->setChangeType('system settings change');

        return $contentContainer;
    }


    /**
     * Use this method to add a translation for a certain locale. To store these to the database, a doctrine flush is
     * needed.
     *
     * @param string $locale
     * @param string $translation
     */
    public function addNewTranslation(string $locale, string $translation): void
    {
        $this->translate($locale, false)->setTemplateNameTranslated($translation);
        $this->mergeNewTranslations();
    }


    /**
     * Use this method to add a translation for a certain locale. To store these to the database, a doctrine flush is
     * needed.
     *
     * @param string $locale
     * @param string $translation
     */
    public function addNewTSenderAddressTranslation(string $locale, string $translation): void
    {
        $this->translate($locale, false)->setSenderAddressTranslated($translation);
        $this->mergeNewTranslations();
    }


    /**
     * When the entity is created for the first time, there will be translations present, either the translation for the
     * default locale  or translations added by hand. These must be saved in the database, which is done by the merge
     * new translations command. To ensure that this is called only once regardless of how the translations are added
     * (e.g. by constructor, setter or by hand), the merge is done before persisting the object.
     *
     * @ORM\PrePersist()
     */
    public function mergeNewTranslationsOnPersist(): void
    {
        $this->mergeNewTranslations();
    }


    /**
     * Return the template names for each language as a string, e.g. "de: Vorlage; en: template"
     *
     * @return string
     */
    public function getTemplateNames(): string
    {
        $names = '';

        /** @var EmailTemplateTranslation[] $translations */
        $translations = $this->getTranslations();

        foreach ($translations as $translation) {
            $names .= "{$translation->getLocale()}: {$translation->getTemplateNameTranslated()}; ";
        }

        // Remove last semicolon
        if ('' !== $names) {
            $names = substr($names, 0, -2);
        }

        return $names;
    }


    /**
     * Set the value of sender address.
     *
     * @param string $senderAddress
     */
    public function setSenderAddress(string $senderAddress): void
    {
        // When the sender address is set for the first time or when it is changed, then update or set the default
        // translation.
        if ($this->senderAddress !== $senderAddress) {
            $this->translate($this->getDefaultLocale())->setSenderAddressTranslated($senderAddress);
        }

        $this->senderAddress = $senderAddress;
    }


    /**
     * Get the value of sender address.
     *
     * @param string|null $locale
     * @param bool        $getUntranslated When true, the original, untranslated value is returned.
     *
     * @return string
     */
    public function getSenderAddress(?string $locale = null, bool $getUntranslated = false): string
    {
        if ($getUntranslated) {
            return $this->senderAddress;
        }

        if ($locale) {
            $this->setCurrentLocale($locale);
        }

        return $this->proxyCurrentLocaleTranslation('getSenderAddressTranslated', []);
    }

    /**
     * When the content is in html (which is the case when using the editor in the gui), then the following changes are
     * done to make it easier for the diff viewer and the log file:
     * variables are replaced by {{ variable-name }}
     * html parameters are removed
     *
     * @param string|null $content
     *
     * @return string|null
     */
    private function optimiseHtmlContentForDiff(?string $content): ?string
    {
        // Replace variables like <span class="mceNonEditable" style="border: thin dotted grey;" data-variable="firstName">firstName</span>
        // by {{ firstName }}
        preg_match_all("#.*(<span class=\"mceNonEditable\".+data-variable=\"(\w+)\".*>.+</span>)#U", $content, $matches);
        foreach ($matches[1] as $key => $variableBlock) {
            $content = str_replace($variableBlock, '{{ '.$matches[2][$key].' }}', $content);
        }

        // Remove all the parameters from html tags (e.g. change <td style="width: 15cm;"> to <td>). This is needed so
        // <ins> or <del> are not written into the middle of an html tag with parameters.
        $content = preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/si", '<$1$2>', $content);

        return $content;
    }
}
