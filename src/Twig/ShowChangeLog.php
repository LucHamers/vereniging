<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Twig;

use DateTime;
use Exception;
use Symfony\Component\Yaml\Yaml;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * This class reads the change log from the file change_log.yaml in the projects base directory and shows it as an html
 * table.
 *
 * Class ShowChangeLog
 */
class ShowChangeLog extends AbstractExtension
{
    private ?array $changeLogArray = [];


    /**
     * ShowChangeLog constructor.
     *
     * @param string $projectDir
     * @param string $changeLogFile Path to the yaml file with the change log.
     *
     * @throws Exception
     */
    public function __construct(string $projectDir, string $changeLogFile = '')
    {
        $filepath = "$projectDir/$changeLogFile";
        if (!is_readable($filepath)) {
            throw new Exception(sprintf('Cannot load change log yaml file %s!', "$projectDir/$changeLogFile"));
        }

        $this->changeLogArray = Yaml::parseFile($filepath);
    }


    /**
     * @inheritDoc
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('show_change_log', [$this, 'showChangeLog'], ['is_safe' => ['html']]),
        ];
    }


    /**
     * @return string
     */
    public function showChangeLog(): string
    {
        if (is_null($this->changeLogArray)) {
            return ('-');
        }

        $html = '';
        foreach ($this->changeLogArray as $version => $versionItems) {
            $migrationItems = $versionItems['migration'] ?? '';
            $html .= "<h4>Version $version.".$versionItems['data'][0][0]."</h4>\n";
            if ($migrationItems) {
                $html .= "<h6>Update information:</h6>\n";
                $html .= "<ul>\n";
                foreach ($migrationItems as $migrationItem) {
                    $html .= "  <li>$migrationItem</li>\n";
                }
                $html .= "</ul>\n";
            }
            $html .= "<table>\n";
            $html .= "  <tr><th>Buildnumber&nbsp;&nbsp;</th><th>Date</th><th>&nbsp;&nbsp;Issue</th><th>Change</th></tr>\n";
            foreach ($versionItems['data'] as $versionItem) {
                $date = new DateTime($versionItem[1]);
                $html .= "  <tr><td style=\"vertical-align: top;\">$versionItem[0]</td><td style=\"text-align: right; vertical-align: top;\">".$date->format('d.m.Y')."</td>";
                $html .= "<td style='text-align: right; vertical-align: top;'>".($versionItem[2] ? "(#$versionItem[2])" : '')."&nbsp;</td><td>$versionItem[3]</td></tr>\n";
            }
            $html .= "<tr><td>&nbsp;</td><td></td><td></td><td></td></tr></table>\n";
        }

        return $html;
    }
}
