<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Doctrine;

use App\Entity\MembershipType;
use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\UnitOfWork;
use Doctrine\Persistence\Event\LifecycleEventArgs;

/**
 * Class DefaultMembershipTypesListener
 *
 * This class listens to the change events for membership types and ensures that exactly one default new and group
 * membership type.
 *
 */
class DefaultMembershipTypesSubscriber implements EventSubscriberInterface
{
    /**
     * @param LifecycleEventArgs $args
     */
    public function preRemove(LifecycleEventArgs $args): void
    {
        /** @var MembershipType $entity */
        $entity = $args->getObject();
        $entityManager = $args->getObjectManager();

        if (!$entity instanceof MembershipType) {
            return;
        }

        // When none of the default flags is set, then no action is needed.
        if (!$entity->getDefaultGroupMembershipType() && !$entity->getDefaultNewMembershipType()) {
            return;
        }

        $membershipTypes = $entityManager->getRepository(MembershipType::class)->findAll();
        $this->ensureDefaultNewFlagSetOnDelete($entity, $membershipTypes);
        $this->ensureDefaultGroupFlagSetOnDelete($entity, $membershipTypes);
    }


    /**
     * This method ensures that when on an existing membership type one of the default flags is set, all other default
     * flags if the same type are removed.
     *
     * @param OnFlushEventArgs $eventArgs
     */
    public function onFlush(OnFlushEventArgs $eventArgs): void
    {
        $entityManager = $eventArgs->getEntityManager();
        $uow = $entityManager->getUnitOfWork();

        $this->processInserts($uow, $entityManager);
        $this->processUpdates($uow, $entityManager);
    }


    /**
     * @return array
     */
    public function getSubscribedEvents(): array
    {
        return [Events::onFlush, Events::preRemove];
    }


    /**
     * @param MembershipType         $entity
     * @param UnitOfWork             $uow
     * @param EntityManagerInterface $entityManager
     */
    private function updateChangeSet(MembershipType $entity, UnitOfWork $uow, EntityManagerInterface $entityManager): void
    {
        $meta = $entityManager->getClassMetadata(get_class($entity));
        $uow->recomputeSingleEntityChangeSet($meta, $entity);
    }


    /**
     * When the new to be inserted entities have one of the default flags set, remove the flags on all other membership
     * types. This handles the following cases:
     * 1. There are no types in the database, new ones with at least one default flag are added
     * 2. There are no types in the database, new ones without a default flag are added
     * 3. There are types in the database, new ones without flags are added
     * 4. There are types in the database, new ones without at least one default flag are added
     *
     * @param UnitOfWork             $uow
     * @param EntityManagerInterface $entityManager
     */
    private function processInserts(UnitOfWork $uow, EntityManagerInterface $entityManager): void
    {
        $defaultGroup = [
            'count' => 0,
            'entity' => null,
        ];
        $defaultNew = [
            'count' => 0,
            'entity' => null,
        ];
        $firstDefaultValue = null;

        /** @var MembershipType[] $membershipTypes */
        $membershipTypes = $entityManager->getRepository(MembershipType::class)->findAll();
        if (count($membershipTypes)) {
            $defaultGroup['count'] = 1;
            $defaultNew['count'] = 1;
        }

        $valuesInserted = false;

        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if ($entity instanceof MembershipType) {
                $valuesInserted = true;
                // Set a default entity in case no values are stored and none of the new values has set default flags.
                if (0 === count($membershipTypes)) {
                    $firstDefaultValue = $entity;
                }

                // When in the current insert, the group default flag is set, then remove this flag from all others
                $defaultGroup = $this->defaultGroupIsSet($entity, $defaultGroup, $membershipTypes, $uow, $entityManager);

                // When in the current insert, the new default flag is set, then remove this flag from all others
                $defaultNew = $this->defaultNewIsSet($entity, $defaultNew, $membershipTypes, $uow, $entityManager);
            }
        }

        if (0 === $defaultGroup['count'] + $defaultNew['count'] && $valuesInserted) {
            $firstDefaultValue->setDefaultNewMembershipType(true);
            $firstDefaultValue->setDefaultGroupMembershipType(true);
            $this->updateChangeSet($firstDefaultValue, $uow, $entityManager);
        }

        // Special handler when more than one entity is inserted at once with set default group flags
        if ($defaultGroup['count'] > 1) {
            $this->removeOtherFlags($uow->getScheduledEntityInsertions(), $defaultGroup['entity'], 'setDefaultGroupMembershipType', $uow, $entityManager);
        }

        // Special handler when more than one entity is inserted at once with set default new flags
        if ($defaultNew['count'] > 1) {
            $this->removeOtherFlags($uow->getScheduledEntityInsertions(), $defaultNew['entity'], 'setDefaultNewMembershipType', $uow, $entityManager);
        }
    }


    /**
     * One of the changed entities contains a set default flag, then remove the flags on all other membership types.
     *
     * @param UnitOfWork             $uow
     * @param EntityManagerInterface $entityManager
     */
    private function processUpdates(UnitOfWork $uow, EntityManagerInterface $entityManager): void
    {
        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if ($entity instanceof MembershipType) {
                /** @var MembershipType[] $membershipTypes */
                $membershipTypes = $entityManager->getRepository(MembershipType::class)->findAll();

                foreach ($uow->getEntityChangeSet($entity) as $keyField => $field) {
                    // When one of the boolean fields are changed from false to true (field[0] is the previous value,
                    // $field[1] is the new value), then there is a new default group or new membership type. In that
                    // case set all other flags fo the same type for all other membership types to false.
                    if ($field[1]) {
                        foreach ($membershipTypes as $membershipType) {
                            // Don't change the flags of the current membership type.
                            if ($membershipType->getId() !== $entity->getId()) {
                                // Set corresponding flag to false
                                if ('defaultGroupMembershipType' === $keyField) {
                                    $membershipType->setDefaultGroupMembershipType(false);
                                } else {
                                    if ('defaultNewMembershipType' === $keyField) {
                                        $membershipType->setDefaultNewMembershipType(false);
                                    }
                                }
                                // Store the changed entity
                                $this->updateChangeSet($membershipType, $uow, $entityManager);
                            }
                        }
                    }
                }
            }
        }
    }


    /**
     * Pass a list of membership type entities and remove the selected flag from all entities except the fixed one.
     *
     * @param mixed                  $entityList
     * @param MembershipType         $fixedEntity
     * @param string                 $function
     * @param UnitOfWork             $uow
     * @param EntityManagerInterface $entityManager
     */
    private function removeOtherFlags($entityList, MembershipType $fixedEntity, string $function, UnitOfWork $uow, EntityManagerInterface $entityManager): void
    {
        foreach ($entityList as $membershipType) {
            if ($membershipType instanceof MembershipType) {
                // Don't change the flags of the current membership type.
                if ($fixedEntity !== $membershipType) {
                    $membershipType->$function(false);
                    $this->updateChangeSet($membershipType, $uow, $entityManager);
                }
            }
        }
    }


    /**
     * When the group default flag is set, then this flag is removed from all other types in the database.
     *
     * @param MembershipType         $entity
     * @param array                  $defaultGroup
     * @param MembershipType[]       $membershipTypes
     * @param UnitOfWork             $uow
     * @param EntityManagerInterface $entityManager
     *
     * @return array
     */
    private function defaultGroupIsSet(MembershipType $entity, array $defaultGroup, $membershipTypes, UnitOfWork $uow, EntityManagerInterface $entityManager): array
    {
        if ($entity->getDefaultGroupMembershipType()) {
            ++$defaultGroup['count'];
            $defaultGroup['entity'] = $entity;
            // When the default group flag is set on the new entity, then the flag of all others is removed
            $this->removeOtherFlags($membershipTypes, $entity, 'setDefaultGroupMembershipType', $uow, $entityManager);
        }

        return $defaultGroup;
    }


    /**
     * When the new member default flag is set, then this flag is removed from all other types in the database.
     *
     * @param MembershipType         $entity
     * @param array                  $defaultNew
     * @param MembershipType[]       $membershipTypes
     * @param UnitOfWork             $uow
     * @param EntityManagerInterface $entityManager
     *
     * @return array
     */
    private function defaultNewIsSet(MembershipType $entity, array $defaultNew, $membershipTypes, UnitOfWork $uow, EntityManagerInterface $entityManager): array
    {
        if ($entity->getDefaultNewMembershipType()) {
            ++$defaultNew['count'];
            $defaultNew['entity'] = $entity;
            // When the default group flag is set on the new entity, then the flag of all others is removed
            $this->removeOtherFlags($membershipTypes, $entity, 'setDefaultNewMembershipType', $uow, $entityManager);
        }

        return $defaultNew;
    }


    /**
     * If the entity which is currently deleted, is the default membership type for new members, then ensure that
     * another type is set to be the default type.
     *
     * @param MembershipType $entity
     * @param array          $membershipTypes
     */
    private function ensureDefaultNewFlagSetOnDelete(MembershipType $entity, array $membershipTypes)
    {
        if ($entity->getDefaultNewMembershipType()) {
            foreach ($membershipTypes as $membershipType) {
                if ($membershipType->getId() !== $entity->getId()) {
                    $membershipType->setDefaultNewMembershipType(true);
                    break;
                }
            }
        }
    }


    /**
     * If the entity which is currently deleted, is the default membership type for groups, then ensure that another
     * type is set to be the default group type.
     *
     * @param MembershipType $entity
     * @param array          $membershipTypes
     */
    private function ensureDefaultGroupFlagSetOnDelete(MembershipType $entity, array $membershipTypes): void
    {
        if ($entity->getDefaultGroupMembershipType()) {
            /** @var MembershipType $membershipType */
            foreach ($membershipTypes as $membershipType) {
                // Ensure that the new default group type also is a group type.
                if (($membershipType->getId() !== $entity->getId()) && ($membershipType->getGroupMembership())) {
                    $membershipType->setDefaultGroupMembershipType(true);
                    break;
                }
            }
        }
    }
}
