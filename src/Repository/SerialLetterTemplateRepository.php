<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Repository;

use App\Entity\Language;
use App\Entity\SerialLetterTemplate;
use Doctrine\ORM\EntityRepository;

/**
 * Class SerialLetterTemplateRepository
 */
class SerialLetterTemplateRepository extends EntityRepository
{
    /**
     * @return array Return the serial letter templates as array with the following structure:
     * [
     *   locale => [name => content, name => content, ...]
     *   locale => [name => content, name => content, ...]
     * ]
     * The entries below locale are ordered by name
     */
    public function findAllAsArray(): array
    {
        $entities = $this->createQueryBuilder('template')
            ->leftJoin('template.language', 'lang')
            ->addOrderBy('lang.language', 'ASC')
            ->addOrderBy('template.templateName', 'ASC')
            ->getQuery()
            ->getResult()
        ;

        $templates = [];
        /** @var SerialLetterTemplate $entity */
        foreach ($entities as $entity) {
            $templates[$entity->getLanguage()->getLocale()][] = $entity;
        }

        return $templates;
    }
}
