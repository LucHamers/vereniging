<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Member;

use App\Entity\ChangeType;
use App\Entity\LogfileEntry;
use App\Entity\MemberEntry;
use App\Entity\MembershipType;
use App\Entity\UserRole;
use App\Form\NewMemberFormType;
use App\Service\ManageMembershipNumberInterface;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class NewMemberController
 *
 * @Security("is_granted('ROLE_CREATE_NEW_MEMBERS')")
 *
 */
class NewMemberController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private ManageMembershipNumberInterface $membershipNumberManager;


    /**
     * NewMemberController constructor.
     *
     * @param EntityManagerInterface          $entityManager
     * @param ManageMembershipNumberInterface $membershipNumberManager
     */
    public function __construct(EntityManagerInterface $entityManager, ManageMembershipNumberInterface $membershipNumberManager)
    {
        $this->entityManager = $entityManager;
        $this->membershipNumberManager = $membershipNumberManager;
    }

    /**
     * @Route("/create_new_member", name="create_new_member")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        $form = $this->createForm(NewMemberFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var MemberEntry $newMember */
            $newMember = $form->getData();
            $newMember->setMembershipEndRequested(false);
            $memberShipType = $this->entityManager->getRepository(MembershipType::class)->findOneBy(['defaultNewMembershipType' => true]);
            $newMember->setMembershipType($memberShipType);
            $userRole = $this->entityManager->getRepository(UserRole::class)->findOneBy(['defaultRole' => true]);
            $newMember->setUserRole($userRole);
            $newMember->setBirthday(null);
            $this->updateMembershipNumber($newMember);
            $this->entityManager->persist($newMember);
            $this->entityManager->flush();

            // Because the member gets its id after the log message is created, the changes on field of the log message
            // must be set manually.
            $logMessage = $this->entityManager->getRepository(LogfileEntry::class)->findOneBy([], ['id' => 'DESC']);
            $logMessage->setChangesOnMember($newMember);
            // Because the change type is wrong for new members, change is here
            $memberAddedType = $this->entityManager->getRepository(ChangeType::class)->findOneBy(['changeType' => 'member added']);
            $logMessage->setChangeType($memberAddedType);
            $this->entityManager->flush();

            return $this->redirectToRoute('member_data', ['userName' => $newMember->getUsername()]);
        }

        return $this->render('Member/create_new_member.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * When the membership has a membership number, then add a membership number
     *
     * @param MemberEntry $member
     */
    private function updateMembershipNumber(MemberEntry $member): void
    {
        // Check if the status has been changed in the form. If so, this might change the membership number.
        // If a status has changed in a way that it influences the membership number, then set or remove the
        // number.
        if ($member->getStatus()->getHasMembershipNumber()) {
            $this->membershipNumberManager->addMembershipNumber($member);
            // When adding the member ship number, then also set the member since year field
            $member->setMemberSince(date("Y"));
        }
    }
}
