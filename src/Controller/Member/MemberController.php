<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Member;

use App\Entity\AddressType;
use App\Entity\CommitteeFunctionMap;
use App\Entity\LogfileEntry;
use App\Entity\MemberEntry;
use App\Entity\MembershipFeeTransaction;
use App\Entity\PhoneNumberType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Controller for editing member entry data.
 *
 */
class MemberController extends AbstractController
{
    private EntityManagerInterface $entityManager;


    /**
     * MemberController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * Show all information about a member entry. This creates several sections, which then can be edited one by one
     * using ajax.
     *
     * @Route("/member/{userName}", name="member_data" )
     *
     * @param string              $userName   Username to search for the member entry to display.
     * @param TranslatorInterface $translator
     *
     * @return Response
     */
    public function indexAction(string $userName, TranslatorInterface $translator): Response
    {
        /** @var MemberEntry $member */
        $member = $this->entityManager->getRepository(MemberEntry::class)->findOneBy(['username' => $userName]);

        // When the user is only allowed to edit its own data and he tries to edit other users, throw an exception.
        if (!$this->isGranted('ROLE_EDIT_OTHER_USERS') && ($this->getUser()->getUsername() !== $member->getUsername())) {
            throw $this->createAccessDeniedException('You are not allowed to edit other users data!');
        }

        $addressTypes = $this->entityManager->getRepository(AddressType::class)->findAll();
        $phoneNumberTypes = $this->entityManager->getRepository(PhoneNumberType::class)->findAll();

        $logfileEntries = $this->entityManager->getRepository(LogfileEntry::class)
                                              ->findBy(array('changesOnMember' => $member), array('date' => 'DESC', 'id' => 'DESC'), 5);

        LogfileEntry::prepareTranslation($this->entityManager, $translator, $this->getUser()->getPreferredLanguage()->getLocale());

        $committees = $this->entityManager->getRepository(CommitteeFunctionMap::class)->getOrderedCommitteeFunctions($member->getCommitteeFunctionMaps());

        // This is needed for the rendering of the committee function table
        $maxNoFunctions = 0;
        foreach ($committees as $committee) {
            $maxNoFunctions = max($maxNoFunctions, count($committee));
        }

        return $this->render('Member/index.html.twig', [
            'member' => $member,
            'valid' => true,
            'logfile_entries' => $logfileEntries,
            'address_types' => $addressTypes,
            'phone_number_types' => $phoneNumberTypes,
            'committees' => $committees,
            'maxNumberOfFunctions' => $maxNoFunctions,
            'membershipFeeTransactions' => $this->entityManager->getRepository(MembershipFeeTransaction::class)->findAllByMember($member),
        ]);
    }
}
