<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Member;

use App\Entity\MemberEntry;
use App\Form\MemberEntryPersonalDataType;
use App\Service\LogMessageCreator;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller for editing member entry personal data.
 *
 */
class MemberPersonalDataController extends AbstractController
{
    /**
     * This method is called when the member entry has to be edited or when it is submitted after editing.
     *
     * @Route("/member/edit_personal_data/{id}", name="member_edit_personal_data" )
     *
     * @param MemberEntry            $member
     * @param Request                $request
     * @param EntityManagerInterface $entityManager
     * @param LogMessageCreator      $logMessageCreator
     *
     * @return Response
     *
     * @throws Exception
     */
    public function editAction(MemberEntry $member, Request $request, EntityManagerInterface $entityManager, LogMessageCreator $logMessageCreator): Response
    {
        // When the user is only allowed to edit its own data and he tries to edit other users, throw an exception.
        if (!$this->isGranted('ROLE_EDIT_OTHER_USERS') && ($this->getUser()->getUsername() !== $member->getUsername())) {
            throw $this->createAccessDeniedException('You are not allowed to edit other users data!');
        }

        // In case the password validation fails, the password field will still be empty because of setting the plain
        // password when handling the request. To work around this, store the password and write back in case of
        // validation errors
        $currentPassword = $member->getPassword();
        $originalLocale = $member->getPreferredLanguage()->getLocale();
        $form = $this->createForm(MemberEntryPersonalDataType::class, $member);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($member->getPlainPassword()) {
                $logMessageCreator->createLogEntry('member settings change', 'Updated password', $member);
            }

            // Prevent code injections. This will break the user login, so the admin will have to fix it. But at least
            // this is safe.
            $member->setName(str_replace('<', '&lt;', $member->getName()));
            $member->setName(str_replace('>', '&gt;', $member->getName()));
            $member->setFirstName(str_replace('<', '&lt;', $member->getFirstName()));
            $member->setFirstName(str_replace('>', '&gt;', $member->getFirstName()));

            $entityManager->flush();

            $reloadPage = false;
            // When the currently logged in user changes his language, then also change the gui language by changing the
            // locale in the session. This is otherwise only done on login.
            if ($this->getUser() === $member) {
                $request->getSession()->set('original_locale', $originalLocale);
                if ($originalLocale !== $member->getPreferredLanguage()->getLocale()) {
                    $request->getSession()->set('_locale', $member->getPreferredLanguage()->getLocale());
                    $reloadPage = true;
                }
            }

            return $this->render("Member/personal_data.html.twig", [
                'member' => $member,
                'edit' => false,
                'reloadPage' => $reloadPage,
            ]);
        }


        $member->setPassword($currentPassword);

        return $this->render("Member/personal_data.html.twig", [
            'member' => $member,
            'edit' => true,
            'form' => $form->createView(),
        ]);
    }


    /**
     * This method is called when the member entry is switched from editing to not-editing by pressing cancel.
     *
     * @Route("/member/cancel_personal_data/{id}", name="member_cancel_personal_data" )
     *
     * @param MemberEntry $member Member entry
     *
     * @return Response
     */
    public function cancelAction(MemberEntry $member): Response
    {
        return $this->render("Member/personal_data.html.twig", array(
            'member' => $member,
            'edit' => false,
        ));
    }
}
