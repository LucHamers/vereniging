<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Member;

use App\Entity\CommitteeFunction;
use App\Entity\CommitteeFunctionMap;
use App\Entity\ComplexLogEntrySingleDataField;
use App\Entity\LogEntryDataField;
use App\Entity\MemberEntry;
use App\Entity\MembershipType;
use App\Entity\Status;
use App\Form\MemberEntryClubDataType;
use App\Service\LogMessageCreator;
use App\Service\ManageMembershipNumberInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller for editing member entry personal data.
 *
 */
class MemberClubDataController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private LogMessageCreator $logMessageCreator;
    private ManageMembershipNumberInterface $membershipNumberManager;


    /**
     * MemberClubDataController constructor.
     *
     * @param EntityManagerInterface          $entityManager
     * @param LogMessageCreator               $logMessageCreator
     * @param ManageMembershipNumberInterface $membershipNumberManager
     */
    public function __construct(EntityManagerInterface $entityManager, LogMessageCreator $logMessageCreator, ManageMembershipNumberInterface $membershipNumberManager)
    {
        $this->entityManager = $entityManager;
        $this->logMessageCreator = $logMessageCreator;
        $this->membershipNumberManager = $membershipNumberManager;
    }


    /**
     * This method is called when the member entry has to be edited or when it is submitted after editing.
     *
     * @Route("/member/edit_club_data/{id}", name="member_edit_club_data")
     *
     * @Security("is_granted('ROLE_MANAGE_MEMBER_CLUB_SETTINGS')")
     *
     * @param MemberEntry $member  Member entry to edit.
     * @param Request     $request
     *
     * @return Response
     *
     * @throws Exception
     */
    public function editAction(MemberEntry $member, Request $request): Response
    {
        $originalMemberGroup = [];
        foreach ($member->getGroupMembers() as $groupMember) {
            $originalMemberGroup[] = $groupMember->getCompleteName();
        }
        sort($originalMemberGroup);


        $originalFunctions = [];
        foreach ($member->getCommitteeFunctionMaps() as $committeeFunctionMap) {
            $originalFunctions[] = $committeeFunctionMap;
        }

        $originalStatus = $member->getStatus();
        $originalDirectDebitSetting = $member->getMembershipNumber() ? $member->getMembershipNumber()->getUseDirectDebit() : false;

        $form = $this->createForm(MemberEntryClubDataType::class, $member, [
            'selected_member' => $member,
            'current_locale' => $request->getLocale(),
        ]);

        /** @var array | MemberEntry[] $possibleGroupMembers */
        $possibleGroupMembers = $this->entityManager->getRepository(MemberEntry::class)
                                                    ->createFindAllWithSameAddressesAndLastNameQueryBuilder($member)
                                                    ->getQuery()
                                                    ->execute();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->updateMembershipNumber($member, $originalStatus);
            $this->generateFunctionChangeLogMessages($member, $originalFunctions);
            $this->generateDirectDebitLogMessages($member, $originalDirectDebitSetting);
            $this->entityManager->flush();

            $committees = $this->entityManager->getRepository(CommitteeFunctionMap::class)->getOrderedCommitteeFunctions($member->getCommitteeFunctionMaps());

            return $this->render("Member/club_data.html.twig", array(
                'member' => $member,
                'edit' => false,
                'committees' => $committees,
                'maxNumberOfFunctions' => $this->getMaxNumberOfFunctions($committees),
                'possibleMembers' => $possibleGroupMembers,
            ));
        }

        $functions = $this->getOrderedCommitteeFunctionMapsIds();
        $allMembershipTypes = $this->entityManager->getRepository(MembershipType::class)
                                                  ->findByGroupMembershipAsJson($request->getLocale(), false);
        $groupMembershipTypes = $this->entityManager->getRepository(MembershipType::class)
                                                    ->findByGroupMembershipAsJson($request->getLocale(), true);

        return $this->render("Member/club_data.html.twig", array(
            'member' => $member,
            'edit' => true,
            'form' => $form->createView(),
            'committees' => $functions,
            'maxNumberOfFunctions' => $this->getMaxNumberOfFunctions($functions),
            'possibleMembers' => $possibleGroupMembers,
            'groupMembershipTypes' => $groupMembershipTypes,
            'allMembershipTypes' => $allMembershipTypes,
        ));
    }


    /**
     * This method is called when the member entry is switched from editing to not-editing by pressing cancel.
     *
     * @Route("/member/cancel_club_data/{id}", name="member_cancel_club_data")
     *
     * @param int $id User index to search for the member entry to edit.
     *
     * @return Response
     */
    public function cancelAction(int $id): Response
    {
        $member = $this->entityManager->getRepository(MemberEntry::class)->find($id);
        $committees = $this->entityManager->getRepository(CommitteeFunctionMap::class)->getOrderedCommitteeFunctions($member->getCommitteeFunctionMaps());

        return $this->render("Member/club_data.html.twig", array(
            'member' => $member,
            'edit' => false,
            'committees' => $committees,
            'maxNumberOfFunctions' => $this->getMaxNumberOfFunctions($committees),
        ));
    }


    /**
     * Get all committee function map ids. It returns an array where the committee is the key and for each committee
     * the map id is stored, ordered by function name.
     *
     * @return array
     */
    private function getOrderedCommitteeFunctionMapsIds(): array
    {
        $committeeFunctions = $this->entityManager->getRepository(CommitteeFunction::class)->findAll();

        $orderedFunctions = [];
        foreach ($committeeFunctions as $committeeFunction) {
            $orderedFunctions[] = $committeeFunction->getCommitteeFunctionName();
        }
        sort($orderedFunctions);

        // Add members committee functions to functions array, where the committee name is the array key.
        $functions = [];
        $committeeFunctionMaps = $this->entityManager->getRepository(CommitteeFunctionMap::class)->findAll();
        foreach ($orderedFunctions as $function) {
            foreach ($committeeFunctionMaps as $committeeFunctionMap) {
                if ($committeeFunctionMap->getCommitteeFunction()->getCommitteeFunctionName() === $function) {
                    $functions[$committeeFunctionMap->getCommittee()->getCommitteeName()][] = $committeeFunctionMap->getId();
                }
            }
        }

        // Sort the committee names
        ksort($functions);

        return $functions;
    }


    /**
     * @param MemberEntry $member
     * @param array       $originalFunctions
     *
     * @throws Exception
     */
    private function generateFunctionChangeLogMessages(MemberEntry $member, array $originalFunctions): void
    {
        $currentFunctions = [];
        foreach ($member->getCommitteeFunctionMaps() as $committeeFunctionMap) {
            $currentFunctions[] = $committeeFunctionMap;
        }

        // When the original committee-function is missing in the current list, then the function has been removed.
        foreach ($originalFunctions as $originalFunction) {
            if (!in_array($originalFunction, $currentFunctions)) {
                $complexEntry = new ComplexLogEntrySingleDataField();
                $complexEntry->addDataField('Committee', [new LogEntryDataField($originalFunction->getCommittee(), true)]);
                $complexEntry->addDataField('Function', [new LogEntryDataField($originalFunction->getCommitteeFunction(), true)]);
                $complexEntry->setMainMessage('Removed committee membership');

                $this->logMessageCreator->createLogEntry('member settings change', $complexEntry, $member);
            }
        }

        // When the current committee-function is missing in the original list, then the function has been added.
        foreach ($currentFunctions as $currentFunction) {
            if (!in_array($currentFunction, $originalFunctions)) {
                $complexEntry = new ComplexLogEntrySingleDataField();
                $complexEntry->addDataField('Committee', [new LogEntryDataField($currentFunction->getCommittee(), true)]);
                $complexEntry->addDataField('Function', [new LogEntryDataField($currentFunction->getCommitteeFunction(), true)]);
                $complexEntry->setMainMessage('Added committee membership');

                $this->logMessageCreator->createLogEntry('member settings change', $complexEntry, $member);
            }
        }
    }


    /**
     * Check if the direct debit settings have been changed and if so, generate a log message for each member of the
     * group.
     *
     * @param MemberEntry $memberEntry
     * @param bool        $originalDirectDebitSetting
     */
    private function generateDirectDebitLogMessages(MemberEntry $memberEntry, bool $originalDirectDebitSetting): void
    {
        if ($memberEntry->getMembershipNumber()) {
            $newDirectDebitSetting = $memberEntry->getMembershipNumber()->getUseDirectDebit();
            if ($originalDirectDebitSetting !== $newDirectDebitSetting) {
                try {
                    // Create direct debit log message for current member
                    $this->addDirectDebitLogMessage($memberEntry, $newDirectDebitSetting);

                    // Create same log message for each group member
                    foreach ($memberEntry->getGroupMembers() as $groupMember) {
                        $this->addDirectDebitLogMessage($groupMember, $newDirectDebitSetting);
                    }
                } catch (Exception $e) {
                    // left intentionally blank
                }
            }
        }
    }


    /**
     * @param MemberEntry $member
     * @param Status      $originalStatus
     *
     * @throws Exception
     */
    private function updateMembershipNumber(MemberEntry $member, Status $originalStatus): void
    {
        // Check if the status has been changed in the form. If so, this might change the membership number.
        // If a status has changed in a way that it influences the membership number, then set or remove the
        // number.
        if ($member->getStatus()->getHasMembershipNumber() !== $originalStatus->getHasMembershipNumber()) {
            if ($originalStatus->getHasMembershipNumber()) {
                $this->membershipNumberManager->removeMembershipNumber($member);
            } else {
                $this->membershipNumberManager->addMembershipNumber($member);
                // When adding the member ship number, then also set the member since year field
                $member->setMemberSince(date("Y"));
            }
        }
    }

    /**
     * Generate a log message with the following content depending on the $userDirectDebit variable:
     * 'Use direct debit for membership number 123'
     * 'Don't use direct debit for membership number 123'
     *
     * @param MemberEntry $changesOn
     * @param bool        $useDirectDebit
     *
     * @throws Exception
     */
    private function addDirectDebitLogMessage(MemberEntry $changesOn, bool $useDirectDebit): void
    {
        $message = $useDirectDebit ? 'Use ' : 'Don\'t use ';
        $message .= 'direct debit for membership number %membershipNumber%|'.$changesOn->getMembershipNumber()->getMembershipNumber();
        $this->logMessageCreator->createLogEntry('member settings change', $message, $changesOn);
    }


    /**
     * Count the number of functions per committee and find the highest number of functions for any committee. This is
     * needed when the function table is rendered.
     *
     * @param array $functions
     *
     * @return int
     */
    private function getMaxNumberOfFunctions(array $functions): int
    {
        $maxNoFunctions = 0;
        foreach ($functions as $function) {
            $maxNoFunctions = max($maxNoFunctions, count($function));
        }

        return $maxNoFunctions;
    }
}
