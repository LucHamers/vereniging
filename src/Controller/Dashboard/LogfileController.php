<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Dashboard;

use App\Entity\LogfileEntry;
use App\Form\LogfileEntrySelectionType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class LogfileController
 *
 * @Security("is_granted('ROLE_VIEW_LOG_FILE')")
 */
class LogfileController extends AbstractController
{
    /**
     * Show the mailing lists.
     *
     * @Route("/dashboard/logfile", name="dashboard_logfile")
     *
     * @param EntityManagerInterface $entityManager
     * @param TranslatorInterface    $translator
     *
     * @return Response
     */
    public function indexAction(EntityManagerInterface $entityManager, TranslatorInterface $translator): Response
    {

        $numberLimit = 10;
        $logfileEntries = $entityManager->getRepository(LogfileEntry::class)
                                        ->findBy([], ['date' => 'DESC', 'id' => 'DESC'], $numberLimit);
        $form = $this->createForm(LogfileEntrySelectionType::class, null, ['selectedNumber' => $numberLimit]);

        LogfileEntry::prepareTranslation($entityManager, $translator, $this->getUser()->getPreferredLanguage()->getLocale());

        return $this->render('Dashboard/logfile.html.twig', [
            'logfileEntries' => $logfileEntries,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Show main member lists page.
     *
     * @Route("/dashboard/logfile/show", name="core_dashboard_logfile_show")
     *
     * @param Request                $request
     * @param EntityManagerInterface $entityManager
     * @param TranslatorInterface    $translator
     *
     * @return Response
     *
     * @throws \Exception
     */
    public function showAction(Request $request, EntityManagerInterface $entityManager, TranslatorInterface $translator): Response
    {
        $form = $this->createForm(LogfileEntrySelectionType::class);

        $logfileEntries = [];
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $numberLimit = $form->getData()['numberLimit'];
            $logfileEntries = $entityManager->getRepository(LogfileEntry::class)
                                            ->findAllByParameters(
                                                $numberLimit,
                                                $form->getData()['changeType'],
                                                $form->getData()['dateRange'],
                                                $form->getData()['searchWhat'],
                                                $form->getData()['searchByWhom'],
                                                $form->getData()['searchOnWho']
                                            );

            LogfileEntry::prepareTranslation($entityManager, $translator, $this->getUser()->getPreferredLanguage()->getLocale());
        }

        return $this->render('Dashboard/logfile_entry_table.html.twig', [
            'logfileEntries' => $logfileEntries,
            'form' => $form->createView(),
        ]);
    }
}
