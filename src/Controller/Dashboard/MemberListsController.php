<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Dashboard;

use App\Entity\EmailTemplate;
use App\Entity\MemberEntry;
use App\Entity\SerialLetter;
use App\Entity\Status;
use App\Service\LogMessageCreator;
use App\Service\MemberListActionsForControllers;
use App\Service\MemberSelectionFormHandler;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as SecurityAnnotation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * Show member lists.
 *
 * @SecurityAnnotation("is_granted('ROLE_VIEW_MEMBER_LISTS')")
 */
class MemberListsController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private MemberSelectionFormHandler $selectionFormHandler;
    private LogMessageCreator $logMessageCreator;
    private MemberListActionsForControllers $memberListActions;


    /**
     * MemberListsController constructor.
     *
     * @param EntityManagerInterface          $entityManager
     * @param MemberSelectionFormHandler      $formHandler
     * @param LogMessageCreator               $logMessageCreator
     * @param Security                        $security
     * @param MemberListActionsForControllers $memberListActions
     *
     * @throws Exception
     */
    public function __construct(EntityManagerInterface $entityManager, MemberSelectionFormHandler $formHandler, LogMessageCreator $logMessageCreator, Security $security, MemberListActionsForControllers $memberListActions)
    {
        $this->entityManager = $entityManager;
        $this->selectionFormHandler = $formHandler;
        $this->logMessageCreator = $logMessageCreator;
        /** @var MemberEntry $currentUser */
        $currentUser = $security->getUser();
        $this->logMessageCreator->setUser($currentUser);
        $this->selectionFormHandler->setOption('currentLocale', $currentUser->getPreferredLanguage()->getLocale());
        $this->selectionFormHandler->setOption('omitFields', ['debt']);
        $this->memberListActions = $memberListActions;
    }


    /**
     * Show main member lists page.
     *
     * @Route("/dashboard/memberlists", name="dashboard_memberlist")
     *
     * @return Response
     */
    public function indexAction(): Response
    {
        // Create the member selection form
        $form = $this->selectionFormHandler->generateForm(true);

        // Read the data needed to show the complete page.
        $members = $this->entityManager->getRepository(MemberEntry::class)->findByStatus('member');
        $letters = $this->entityManager->getRepository(SerialLetter::class)->findAll();
        $statuses = $this->entityManager->getRepository(Status::class)->findAll();
        $emailTemplates = $this->entityManager->getRepository(EmailTemplate::class)->getEmailTemplates();

        return $this->render('Dashboard/member_list.html.twig', array(
            'selectionForm' => $form->createView(),
            'members' => $members,
            'serialLetters' => $letters,
            'statuses' => $statuses,
            'emailTemplates' => $emailTemplates,
        ));
    }


    /**
     * Show member list depending on the filter selection.
     *
     * @Route("/dashboard/memberlists/show", name="dashboard_show_memberlist")
     *
     * @SecurityAnnotation("is_granted('ROLE_SELECT_OTHER_USER_GROUPS')")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function showAction(Request $request): Response
    {
        $this->selectionFormHandler->generateForm();
        $members = $this->selectionFormHandler->handleFormRequest($request);

        $letters = $this->entityManager->getRepository(SerialLetter::class)->findAll();
        $statuses = $this->entityManager->getRepository(Status::class)->findAll();
        $emailTemplates = $this->entityManager->getRepository(EmailTemplate::class)->getEmailTemplates();

        return $this->render('Dashboard/member_table.html.twig', [
            'members' => $members,
            'serialLetters' => $letters,
            'statuses' => $statuses,
            'emailTemplates' => $emailTemplates,
        ]);
    }


    /**
     * With a selected number of members several actions can be done. The listAction distributes these requests to the
     * apropriate function.
     *
     * @Route("/dashboard/memberlists/action", name="dashboard_memberlist_actions" )
     *
     * @param Request $request

     * @return Response
     *
     * @throws Exception
     */
    public function listActions(Request $request): Response
    {
        // Read the member ids to do the action for
        $memberIds = $request->query->all('member');
        // Read the selected status id in case we will change the members status
        $statusId = $request->query->get('status_select', null);
        // Read the serial letter in case this is to be generated for the selected members
        $serialLetterId = $request->query->get('letter_select', null);
        // Read the email template needed for serial emails
        $emailTemplateId = $request->query->get('emailtemplate_select', null);


        switch ($request->query->get('action_select', null)) {
            case 'serial_letter':
                $this->denyAccessUnlessGranted('ROLE_CREATE_SERIAL_LETTERS', null, 'Unable to access this page!');

                return $this->memberListActions->createSerialLetterAction($memberIds, $serialLetterId);
            case 'serial_email':
                $this->denyAccessUnlessGranted('ROLE_SEND_SERIAL_EMAILS', null, 'Unable to access this page!');

                return $this->memberListActions->createSerialEmailAction($memberIds, $serialLetterId, $emailTemplateId);
            case 'change_status':
                $this->denyAccessUnlessGranted('ROLE_MANAGE_MEMBER_CLUB_SETTINGS', null, 'Unable to access this page!');

                return $this->memberListActions->changeMemberStatus($memberIds, $statusId);
        }

        return new Response(null, 204);
    }
}
