<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Dashboard;

use App\Entity\MemberEntry;
use App\Entity\MembershipFeeTransaction;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as SecurityAnnotation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller for showing statistics.
 *
 * @SecurityAnnotation("is_granted('ROLE_VIEW_STATISTICS')")
 */
class StatisticsController extends AbstractController
{
    private EntityManagerInterface $entityManager;


    /**
     * ClubStructureController constructor.
     *
     * @param EntityManagerInterface $entityManager
     *
     * @throws Exception
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * Display the statistics page.
     *
     * @Route("/dashboard/statistics", name="dashboard_statistics")
     *
     * @return Response
     */
    public function indexAction(): Response
    {
        $memberRepo = $this->entityManager->getRepository(MemberEntry::class);
        $feeRepo = $this->entityManager->getRepository(MembershipFeeTransaction::class);

        return $this->render('Dashboard/statistics.html.twig', [
            'countsPerStatus' => $memberRepo->getCountsPerStatus(),
            'countsPerLanguage' => $memberRepo->getCountsPerLanguage(),
            'countsPerMemberSince' => $memberRepo->getCountsPerMemberSince(),
            'countsPerUseDirectDebit' => $memberRepo->getCountsPerUseDirectDebit(),
            'membershipFeeStatistics' => $feeRepo->getMembershipFeeStatistics(),
        ]);
    }
}
