<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Dashboard;

use App\Entity\Language;
use App\Entity\SerialLetter;
use App\Entity\SerialLetterContent;
use App\Entity\SerialLetterTemplate;
use App\Form\SerialLetterType;
use App\Service\LogMessageCreator;
use App\Service\SerialLetterGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as SecurityAnnotation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class SerialLetterController
 *
 * @SecurityAnnotation("is_granted('ROLE_CREATE_SERIAL_LETTERS')")
 */
class SerialLetterController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private LogMessageCreator $logMessageCreator;
    private SerialLetterGenerator $serialLetterGenerator;
    private TranslatorInterface $translator;


    /**
     * SerialLetterController constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param LogMessageCreator      $logMessageCreator
     * @param SerialLetterGenerator  $serialLetterGenerator
     * @param TranslatorInterface    $translator
     * @param Security               $security
     */
    public function __construct(EntityManagerInterface $entityManager, LogMessageCreator $logMessageCreator, SerialLetterGenerator $serialLetterGenerator, TranslatorInterface $translator, Security $security)
    {
        $this->entityManager = $entityManager;
        $this->logMessageCreator = $logMessageCreator;
        $this->serialLetterGenerator = $serialLetterGenerator;
        $this->translator = $translator;
        $this->logMessageCreator->setUser($security->getUser());
    }


    /**
     * Controller for selecting serial letters
     *
     * @Route("/serial_letters/select", name="select_serial_letter")
     *
     * @return Response
     */
    public function indexAction(): Response
    {
        $letters = $this->entityManager->getRepository(SerialLetter::class)->findAll();

        $lang = $this->getUser()->getPreferredLanguage();

        return $this->render('Dashboard/select_serial_letter.html.twig', [
            'letters' => $letters,
            'lang' => $lang,
        ]);
    }


    /**
     * Edit the content of a selected serial letter.
     *
     * @param SerialLetter $letter
     * @param Request      $request
     *
     * @return Response
     *
     * @Route("/serial_letters/edit/{id}", name="edit_serial_letter")
     *
     * @throws Exception
     */
    public function editAction(SerialLetter $letter, Request $request): Response
    {
        // When the first signature member is empty, but the function is still set, then the form validation has a
        // problem. It then tries to set a function which is not valid for the empty member. So fix this by removing the
        // function.
        $serialLetterData = $request->request->all('serial_letter');
        if (!is_null($serialLetterData)) {
            if ((key_exists('firstSignatureMember', $serialLetterData)) &&
                ('' === $serialLetterData['firstSignatureMember'])) {
                $serialLetterData['firstSignatureMembersFunction'] = '';
            }
            if ((key_exists('secondSignatureMember', $serialLetterData)) &&
                ('' === $serialLetterData['secondSignatureMember'])) {
                $serialLetterData['secondSignatureMembersFunction'] = '';
            }
            $request->request->set('serial_letter', $serialLetterData);
        }

        $this->forwardTranslationOfVariables($letter);
        $form = $this->createForm(SerialLetterType::class, $letter);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $letter->setLastUpdatedBy($this->getUser());
            $this->reverseTranslationOfVariables($letter);
            $this->entityManager->flush();

            $previewKeys = $this->generatePreviewKeys($letter);

            /**
             * @var SerialLetterContent $content
             */
            foreach ($previewKeys as $previewKey => $content) {
                if ($request->request->has($previewKey)) {
                    $logContent = "Generated preview for serial letter \"%title%\"|".$content->getLetterTitle();
                    $this->logMessageCreator->createLogEntry('export action', $logContent);
                    $this->entityManager->flush();

                    return ($this->generatePdfPreview($letter, $content));
                }
            }
        }

        $templates = $this->entityManager->getRepository(SerialLetterTemplate::class)->findAllAsArray();

        return $this->render('Dashboard/edit_serial_letter.html.twig', [
            'letter' => $letter,
            'form' => $form->createView(),
            'templates' => $templates,
            'variables' => SerialLetterGenerator::getReplacementVariables(),
        ]);
    }


    /**
     * Create a new serial letter object.
     *
     * @return Response
     *
     * @throws Exception
     *
     * @Route("/serial_letters/new", name="new_serial_letter")
     */
    public function newAction(): Response
    {
        $letter = new SerialLetter();
        $letter->setSignatureUseTitle(false);
        $letter->setLastUpdatedBy($this->getUser());

        $languages = $this->entityManager->getRepository(Language::class)->findAllForSerialLetters();

        foreach ($languages as $language) {
            $content = new SerialLetterContent();
            $letter->addSerialLetterContent($content);
            $content->setLanguage($language);
            $content->setLetterTitle($this->translator->trans('New serial letter', [], null, $language->getLocale()));
            $this->entityManager->persist($content);
        }

        $this->entityManager->flush();

        return ($this->redirectToRoute('edit_serial_letter', ['id' => $letter->getId()]));
    }


    /**
     * In the view, there is a button for every content, which is the name is 'preview'.language. This method generates
     * a list if these name keys.
     *
     * @param SerialLetter $letter Serial letter for which the content list is searched for the language names.
     *
     * @return array
     */
    private function generatePreviewKeys(SerialLetter $letter): array
    {
        $previewKeys = [];
        foreach ($letter->getSerialLetterContents() as $content) {
            $previewKeys['preview'.$content->getLanguage()->getLanguage()] = $content;
        }

        return $previewKeys;
    }


    /**
     * Generate PDF preview file for the selected letter and the selected content. The content must belong to the
     * letter.
     *
     * @param SerialLetter        $letter  Letter for which the preview is to be generated.
     * @param SerialLetterContent $content Content from this letter for which the preview is to be generated.
     *
     * @return BinaryFileResponse
     *
     * @throws Exception
     */
    private function generatePdfPreview(SerialLetter $letter, SerialLetterContent $content): BinaryFileResponse
    {
        try {
            // generatePdfPreview generates an exception when the pdf creation fails.
            $filePath =  $this->serialLetterGenerator->generatePdfPreview($letter, $content->getLanguage(), $this->getUser());
        } catch (Exception $e) {
            // When there was a problem reported by pdflatex, return the log file which starts with the log file line
            // which contains the first error.
            $logFile = $this->serialLetterGenerator->getLogFile();

            return new BinaryFileResponse($logFile, 200, [
                'Cache-Control' => 'max-age=0',
                'Content-Disposition' => 'attachment;filename="letter.log"',
                'Content-Length' => filesize($logFile),
            ]);
        }

        return new BinaryFileResponse($filePath, 200, [
            'Cache-Control' => 'max-age=0',
            'Content-Disposition' => 'attachment;filename="letter.pdf"',
            'Content-Length' => filesize($filePath),
        ]);
    }


    /**
     * When the letter contains variables, they will contain text in the locale of the current user. Before storing
     * these in the database, replace the translated variable name by the untranslated one. A Variable in the text will
     * look like this:
     * <span class="mceNonEditable" style="border: thin dotted grey;" data-variable="birthday">Geburtstag</span>
     * or (when tinyMCE adds special attributes)
     * <span class="mceNonEditable" style="border: thin dotted grey;" data-variable="birthday" data-mce-style="border: thin dotted grey;" contenteditable="false">Geburtstag</span>
     * This has to be replaced by this:
     * <span class="mceNonEditable" style="border: thin dotted grey;" data-variable="birthday">birthday</span>
     * so the translated text "Geburtstag" is replaced by the value of the data-variable
     *
     * @param SerialLetter $letter
     */
    private function reverseTranslationOfVariables(SerialLetter $letter)
    {
        $contents = $letter->getSerialLetterContents();
        foreach ($contents as $content) {
            // Search for all variables. In the returned array, $matches[1] is a list of all data-variables, $matches[2] of
            // the translated texts
            $text = $content->getContent();
            preg_match_all("#.*<span class=\"mceNonEditable\".+data-variable=\"(\w+)\".*>(.+)</span>#U", $text, $matches);

            for ($i = 0; $i < count($matches[0]); $i++) {
                $variableName = $matches[1][$i];
                $translatedName = $matches[2][$i];
                $text = str_replace(">$translatedName</span>", ">$variableName</span>", $text);
            }
            $content->setContent($text);
        }
    }


    /**
     * Variables in the letter content have to be translated to the current locale before they are shown on the edit
     * page. In the database, they will look like this:
     * <span class="mceNonEditable" style="border: thin dotted grey;" data-variable="birthday">birthday</span>
     * This has to be replaced by this:
     * <span class="mceNonEditable" style="border: thin dotted grey;" data-variable="birthday">Geburtstag</span>
     * so the displayed part of the variable is translated
     *
     * @param SerialLetter $letter
     */
    private function forwardTranslationOfVariables(SerialLetter $letter)
    {
        $contents = $letter->getSerialLetterContents();
        foreach ($contents as $content) {
            // Search for all variables. In the returned array, $matches[1] is a list of all the variable texts to be
            // translated.
            $text = $content->getContent();
            preg_match_all("#.*<span class=\"mceNonEditable\".+data-variable=\"(\w+)\".*>.+</span>#U", $text, $matches);
            foreach ($matches[1] as $variableName) {
                $text = str_replace(">$variableName</span>", '>'.$this->translator->trans($variableName).'</span>', $text);
            }
            $content->setContent($text);
        }
    }
}
