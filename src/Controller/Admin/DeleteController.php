<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Admin;

use App\Entity\MemberEntry;
use App\Service\MemberListActionsForControllers;
use App\Service\MemberSelectionFormHandler;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as SecurityAnnotation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * Class DeleteController
 *
 * @SecurityAnnotation("is_granted('ROLE_DELETE_MEMBERS')")
 */
class DeleteController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private MemberSelectionFormHandler $selectionFormHandler;
    private MemberListActionsForControllers $actionsForControllers;
    private MemberEntry $currentUser;


    /**
     * DeleteController constructor.
     *
     * @param EntityManagerInterface          $entityManager
     * @param MemberSelectionFormHandler      $selectionFormHandler
     * @param Security                        $security
     * @param MemberListActionsForControllers $actionsForControllers
     *
     * @throws Exception
     */
    public function __construct(EntityManagerInterface $entityManager, MemberSelectionFormHandler $selectionFormHandler, Security $security, MemberListActionsForControllers $actionsForControllers)
    {

        $this->entityManager = $entityManager;
        $this->selectionFormHandler = $selectionFormHandler;
        $this->currentUser = $security->getUser();
        $this->selectionFormHandler->setOption('currentLocale', $this->currentUser->getPreferredLanguage()->getLocale());
        $this->selectionFormHandler->setOption('omitFields', ['debt']);
        $this->actionsForControllers = $actionsForControllers;
    }


    /**
     * Show the main page to delete users.
     *
     * @Route("/administration/delete_users", name="administration_delete_users")
     *
     * @return Response
     */
    public function indexAction(): Response
    {
        $selectionForm = $this->selectionFormHandler->generateForm(true);

        // Load all members with the status "member", which are shown in the default view.
        $memberEntries = $this->entityManager->getRepository(MemberEntry::class)->findByStatus('member');

        return $this->render('Admin/delete_users.html.twig', [
            'selectionForm' => $selectionForm->createView(),
            'members' => $memberEntries,
        ]);
    }


    /**
     * Update the member list table after a filter settings change.
     *
     * @Route("/administration/delete_users/show", name="administration_delete_users_show")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function showAction(Request $request): Response
    {
        $this->selectionFormHandler->generateForm(false);
        $members = $this->selectionFormHandler->handleFormRequest($request);

        return $this->render('Admin/delete_users_member_table.html.twig', [
            'members' => $members,
        ]);
    }


    /**
     * Open a confirm window to show the user which has to be deleted.
     *
     * @Route("/administration/delete_users/delete_confirm/{id}", name="administration_delete_users_delete_confirm")
     *
     * @param MemberEntry $memberEntry Member entry which shall be deleted
     *
     * @return Response
     */
    public function deleteConfirmAction(MemberEntry $memberEntry): Response
    {
        return $this->render('Admin/delete_users_confirm.html.twig', [
            'member' => $memberEntry,
        ]);
    }


    /**
     * Really delete the member from the database
     *
     * @Route("/administration/delete_users/delete/{id}", name="administration_delete_users_delete")
     *
     * @param MemberEntry $memberEntry Member entry which shall be deleted
     *
     * @return Response
     *
     * @throws Exception
     */
    public function deleteAction(MemberEntry $memberEntry): Response
    {
        $memberId = $memberEntry->getId();
        $this->actionsForControllers->deleteMember($memberEntry, $this->currentUser);

        return new Response($memberId, 200);
    }
}
