<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Admin;

use App\Entity\MemberEntry;
use App\Service\GenerateExcelDataFile;
use App\Service\LogMessageCreator;
use App\Service\MemberSelectionFormHandler;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security as SecurityAnnotation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * Class ExportToExcelController
 *
 * @SecurityAnnotation("is_granted('ROLE_EXPORT_EXCEL')")
 */
class ExportToExcelController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private MemberSelectionFormHandler $selectionFormHandler;
    private LogMessageCreator $logMessageCreator;
    private GenerateExcelDataFile $excelDataFileGenerator;


    /**
     * ExportToExcelController constructor.
     *
     * @param EntityManagerInterface     $entityManager
     * @param MemberSelectionFormHandler $selectionFormHandler
     * @param LogMessageCreator          $logMessageCreator
     * @param GenerateExcelDataFile      $excelDataFileGenerator
     * @param Security                   $security
     *
     * @throws Exception
     */
    public function __construct(EntityManagerInterface $entityManager, MemberSelectionFormHandler $selectionFormHandler, LogMessageCreator $logMessageCreator, GenerateExcelDataFile $excelDataFileGenerator, Security $security)
    {
        $this->entityManager = $entityManager;
        $this->selectionFormHandler = $selectionFormHandler;
        $this->logMessageCreator = $logMessageCreator;
        $this->excelDataFileGenerator = $excelDataFileGenerator;
        /** @var MemberEntry $currentUser */
        $currentUser = $security->getUser();
        $this->selectionFormHandler->setOption('currentLocale', $currentUser->getPreferredLanguage()->getLocale());
        $this->selectionFormHandler->setOption('omitFields', ['debt']);
    }


    /**
     * Select data to be exported.
     *
     * @Route("/administration/export_excel", name="administration_export_to_excel")
     *
     * @return Response
     */
    public function indexAction(): Response
    {
        // Create the member selection form
        $form = $this->selectionFormHandler->generateForm(true);

        // Read the data needed to show the complete page.
        $members = $this->entityManager->getRepository(MemberEntry::class)->findByStatus('member');

        return $this->render('Admin/export_excel.html.twig', array(
            'selectionForm' => $form->createView(),
            'file_selection_form' => $form->createView(),
            'members' => $members,
        ));
    }


    /**
     * Show main member lists page.
     *
     * @Route("/administration/export_excel_show", name="administration_export_to_excel_show")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function showAction(Request $request): Response
    {
        $form = $this->selectionFormHandler->generateForm(false);
        $members = $this->selectionFormHandler->handleFormRequest($request);

        return $this->render('Admin/export_excel_file_download.html.twig', [
            'file_selection_form' => $form->createView(),
            'members' => $members,
        ]);
    }


    /**
     * This method is called with selection criteria for selecting member entries. These members are then exported to an
     * excel file and send to the user as file download.
     *
     * @Route("/administration/export_excel_download", name="administration_export_to_excel_download")
     *
     * @param Request $request
     *
     * @throws Exception Exceptions when logged in user is not set or the changed type is not found.
     *
     * @return Response
     */
    public function downloadExcelFileAction(Request $request): Response
    {
        $this->selectionFormHandler->generateForm();
        $members = $this->selectionFormHandler->handleFormRequest($request);
        /** @var MemberEntry $currentUser */
        $currentUser = $this->getUser();
        $this->excelDataFileGenerator->setLoggedInUser($currentUser);

        try {
            $excelFile = $this->excelDataFileGenerator->createExcelFile($members);
        } catch (Exception $e) {
            $this->logMessageCreator->createLogEntry('export action', 'Export to excel file failed');
            $this->entityManager->flush();

            return new Response('Exporting excel file failed!');
        }

        $response = new StreamedResponse();
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->headers->set('Content-Disposition', 'attachment;filename="vereniging_datafile.xlsx"');
        $response->headers->set('Cache-Control', 'max-age=0');
        $response->setCallback(function () use ($excelFile) {
            $writer = IOFactory::createWriter($excelFile, 'Xlsx');
            $writer->save('php://output');
            flush();
        });
        $this->logMessageCreator->createLogEntry('export action', 'Exported excel file');
        $this->entityManager->flush();

        return ($response);
    }
}
