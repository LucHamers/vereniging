<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use App\Entity\PhoneNumberType;

/**
 * Fixture data for the PhoneNumberType class
 */
class PhoneNumberTypeData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $phoneNumberType1 = new PhoneNumberType("home");
        $phoneNumberType2 = new PhoneNumberType("mobile");
        $phoneNumberType3 = new PhoneNumberType("company");
        $phoneNumberType4 = new PhoneNumberType("company mobile");

        $phoneNumberType1->addNewTranslation('en', 'Home');
        $phoneNumberType1->addNewTranslation('nl', 'Thuis');
        $phoneNumberType1->addNewTranslation('de', 'Zuhause');
        $phoneNumberType2->addNewTranslation('en', 'Mobile');
        $phoneNumberType2->addNewTranslation('nl', 'Mobiel');
        $phoneNumberType2->addNewTranslation('de', 'Mobil');
        $phoneNumberType3->addNewTranslation('en', 'Company');
        $phoneNumberType3->addNewTranslation('nl', 'Werk');
        $phoneNumberType3->addNewTranslation('de', 'Firma');
        $phoneNumberType4->addNewTranslation('en', 'Company mobile');
        $phoneNumberType4->addNewTranslation('nl', 'Werk mobiel');
        $phoneNumberType4->addNewTranslation('de', 'Firma Mobil');

        // Order is reversed to test the findAll method in the repository class.
        $manager->persist($phoneNumberType2);
        $manager->persist($phoneNumberType1);
        $manager->persist($phoneNumberType3);
        $manager->persist($phoneNumberType4);
        $manager->flush();

        $this->addReference('phone_number_type_home', $phoneNumberType1);
        $this->addReference('phone_number_type_mobile', $phoneNumberType2);
        $this->addReference('phone_number_type_company', $phoneNumberType3);
        $this->addReference('phone_number_type_company_mobile', $phoneNumberType4);
    }


    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [DisableLogListener::class];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default'];
    }
}
