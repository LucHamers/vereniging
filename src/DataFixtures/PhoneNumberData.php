<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use App\Entity\PhoneNumber;

/**
 * Fixture data for the PhoneNumber class
 */
class PhoneNumberData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $phoneNumber1 = new PhoneNumber();
        $phoneNumber1->setPhoneNumberType($this->getReference("phone_number_type_home"));
        $phoneNumber1->setMemberEntry($this->getReference("member_entry_last_name1"));
        $phoneNumber1->setPhoneNumber("0123456789");

        $phoneNumber2 = new PhoneNumber();
        $phoneNumber2->setPhoneNumberType($this->getReference("phone_number_type_company"));
        $phoneNumber2->setMemberEntry($this->getReference("member_entry_last_name1"));
        $phoneNumber2->setPhoneNumber("9876543210");

        $phoneNumber3 = new PhoneNumber();
        $phoneNumber3->setPhoneNumberType($this->getReference("phone_number_type_mobile"));
        $phoneNumber3->setMemberEntry($this->getReference("member_entry_last_name1"));
        $phoneNumber3->setPhoneNumber("121212");

        $phoneNumber4 = new PhoneNumber();
        $phoneNumber4->setPhoneNumberType($this->getReference("phone_number_type_company"));
        $phoneNumber4->setMemberEntry($this->getReference("member_entry_last_name1"));
        $phoneNumber4->setPhoneNumber("2876543210");

        $phoneNumber5 = new PhoneNumber();
        $phoneNumber5->setPhoneNumberType($this->getReference("phone_number_type_company"));
        $phoneNumber5->setMemberEntry($this->getReference("member_entry_last_name2"));
        $phoneNumber5->setPhoneNumber("2876543210");

        $manager->persist($phoneNumber1);
        $manager->persist($phoneNumber2);
        $manager->persist($phoneNumber3);
        $manager->persist($phoneNumber4);
        $manager->persist($phoneNumber5);
        $manager->flush();
    }


    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [DisableLogListener::class, PhoneNumberTypeData::class, MemberEntryData::class];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default'];
    }
}
