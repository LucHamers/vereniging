<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Language;

/**
 * Fixture data for the Language class
 */
class LanguageData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $language1 = new Language();
        $language1->setLanguage("German");
        $language1->setLocale("de");
        $language1->setFlagImage("de.png");
        $language1->setUseForSerialLetters(false);
        $language1->setBabelName('ngerman');

        $language2 = new Language();
        $language2->setLanguage("Dutch");
        $language2->setLocale("nl");
        $language2->setFlagImage("nl.png");
        $language2->setUseForSerialLetters(true);

        $language3 = new Language();
        $language3->setLanguage("English");
        $language3->setLocale("en");
        $language3->setFlagImage("en.png");
        $language3->setUseForSerialLetters(true);

        $language4 = new Language();
        $language4->setLanguage("Spanish");
        $language4->setLocale("es");
        $language4->setFlagImage("es.png");
        $language4->setUseForSerialLetters(false);

        $language1->addNewTranslation('en', 'German');
        $language1->addNewTranslation('nl', 'Duits');
        $language1->addNewTranslation('de', 'Deutsch');
        $language2->addNewTranslation('en', 'Dutch');
        $language2->addNewTranslation('nl', 'Nederlands');
        $language2->addNewTranslation('de', 'Niederländisch');
        $language3->addNewTranslation('en', 'English');
        $language3->addNewTranslation('nl', 'Engels');
        $language3->addNewTranslation('de', 'Englisch');
        $language4->addNewTranslation('en', 'Spanish');
        $language4->addNewTranslation('nl', 'Spaans');
        $language4->addNewTranslation('de', 'Spanisch');

        $manager->persist($language1);
        $manager->persist($language2);
        $manager->persist($language3);
        $manager->persist($language4);
        $manager->flush();

        $this->addReference('language_german', $language1);
        $this->addReference('language_dutch', $language2);
        $this->addReference('language_english', $language3);
        $this->addReference('language_spanish', $language4);
    }


    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [DisableLogListener::class];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default'];
    }
}
