<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use App\Entity\EmailTemplate;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Fixture data for the Title class
 */
class EmailTemplateData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $emailTemplate1 = new EmailTemplate();
        $emailTemplate2 = new EmailTemplate();
        $emailTemplate3 = new EmailTemplate();
        $emailTemplate4 = new EmailTemplate();
        $emailTemplate1->setTemplateName('change message');
        $emailTemplate1->setTemplateType('change message');
        $emailTemplate1->setContent('change content: '.$emailTemplate1->getContent());
        $emailTemplate1->getSenderAddress('sender-change@email.com');
        $emailTemplate2->setTemplateName('login data');
        $emailTemplate2->setTemplateType('login data');
        $emailTemplate2->setContent('<p>login content: </p>'.$emailTemplate2->getContent());
        $emailTemplate2->getSenderAddress('sender-login@email.com');
        $emailTemplate3->setTemplateName('serial email');
        $emailTemplate3->setTemplateType('serial email');
        $emailTemplate3->setContent('serial email 1 content: '.$emailTemplate3->getContent());
        $emailTemplate3->getSenderAddress('sender-serial1@email.com');
        $emailTemplate4->setTemplateName('new email');
        $emailTemplate4->setTemplateType('serial email');
        $emailTemplate4->setContent('serial email 2 content: '.$emailTemplate4->getContent());
        $emailTemplate4->getSenderAddress('sender-serial2@email.com');
        $emailTemplate1->addNewTranslation('en', 'Change message');
        $emailTemplate1->addNewTranslation('nl', 'Veranderingsbericht');
        $emailTemplate1->addNewTranslation('de', 'Änderungsnachricht');
        $emailTemplate1->addNewTSenderAddressTranslation('en', 'sender-change-en@email.com');
        $emailTemplate1->addNewTSenderAddressTranslation('nl', 'sender-change-nl@email.com');
        $emailTemplate1->addNewTSenderAddressTranslation('de', 'sender-change-de@email.com');
        $emailTemplate2->addNewTranslation('en', 'Login data');
        $emailTemplate2->addNewTranslation('nl', 'Aanmeldgegevens');
        $emailTemplate2->addNewTranslation('de', 'Anmeldedaten');
        $emailTemplate2->addNewTSenderAddressTranslation('en', 'sender-login-en@email.com');
        $emailTemplate2->addNewTSenderAddressTranslation('nl', 'sender-login-nl@email.com');
        $emailTemplate2->addNewTSenderAddressTranslation('de', 'sender-login-de@email.com');
        $emailTemplate3->addNewTranslation('en', 'Serial email');
        $emailTemplate3->addNewTranslation('nl', 'Serie-email');
        $emailTemplate3->addNewTranslation('de', 'Serien-E-Mail');
        $emailTemplate3->addNewTSenderAddressTranslation('en', 'sender-serial1-en@email.com');
        $emailTemplate3->addNewTSenderAddressTranslation('nl', 'sender-serial1-nl@email.com');
        $emailTemplate3->addNewTSenderAddressTranslation('de', 'sender-serial1-de@email.com');
        $emailTemplate4->addNewTranslation('en', 'New email');
        $emailTemplate4->addNewTranslation('nl', 'Nieuwe email');
        $emailTemplate4->addNewTranslation('de', 'Neue E-Mail');
        $emailTemplate4->addNewTSenderAddressTranslation('en', 'sender-serial2-en@email.com');
        $emailTemplate4->addNewTSenderAddressTranslation('nl', 'sender-serial2-nl@email.com');
        $emailTemplate4->addNewTSenderAddressTranslation('de', 'sender-serial2-de@email.com');

        $manager->persist($emailTemplate1);
        $manager->persist($emailTemplate2);
        $manager->persist($emailTemplate3);
        $manager->persist($emailTemplate4);
        $manager->flush();

        $this->addReference('email_template1', $emailTemplate1);
        $this->addReference('email_template2', $emailTemplate2);
        $this->addReference('email_template3', $emailTemplate3);
        $this->addReference('email_template4', $emailTemplate4);
    }


    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [DisableLogListener::class];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default'];
    }
}
