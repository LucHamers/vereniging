<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use App\Entity\SerialLetterTemplate;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Fixture data for the SerialLetterTemplate class
 */
class SerialLetterTemplateData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $template11 = new SerialLetterTemplate();
        $template11->setLanguage($this->getReference('language_english'));
        $template11->setTemplateName('Day program');
        $template11->setTemplateContent("<table border=\"0\"><tbody><tr><td>19:00</td><td style=\"width: 15cm;\">bla</td></tr><tr><td>20:00</td><td>bla</td></tr></tbody></table>");

        $template12 = new SerialLetterTemplate();
        $template12->setLanguage($this->getReference('language_german'));
        $template12->setTemplateName('Tagesprogramm');
        $template12->setTemplateContent("<table border=\"0\"><tbody><tr><td>19:00h</td><td style=\"width: 15cm;\">bla</td></tr><tr><td>20:00h</td><td>bla</td></tr></tbody></table>");

        $template13 = new SerialLetterTemplate();
        $template13->setLanguage($this->getReference('language_dutch'));
        $template13->setTemplateName('Dagprogramma');
        $template13->setTemplateContent("<table border=\"0\"><tbody><tr><td>19.00u</td><td style=\"width: 15cm;\">bla</td></tr><tr><td>20.00u</td><td>bla</td></tr></tbody></table>");

        $template21 = new SerialLetterTemplate();
        $template21->setLanguage($this->getReference('language_english'));
        $template21->setTemplateName('Activity list');
        $template21->setTemplateContent("<table border=\"0\"><tbody><tr><td>26/7/2020</td><td style=\"width: 15cm;\">bla</td></tr><tr><td>26/8/2020</td><td>bla</td></tr></tbody></table>");

        $template22 = new SerialLetterTemplate();
        $template22->setLanguage($this->getReference('language_german'));
        $template22->setTemplateName('Veranstaltungsliste');
        $template22->setTemplateContent("<table border=\"0\"><tbody><tr><td>26.7.2020</td><td style=\"width: 15cm;\">bla</td></tr><tr><td>26.8.2020</td><td>bla</td></tr></tbody></table>");

        $template23 = new SerialLetterTemplate();
        $template23->setLanguage($this->getReference('language_dutch'));
        $template23->setTemplateName('Activiteitenlijst');
        $template23->setTemplateContent("<table border=\"0\"><tbody><tr><td>26-8-2020</td><td style=\"width: 15cm;\">bla</td></tr><tr><td>26-8-2020</td><td>bla</td></tr></tbody></table>");

        $template31 = new SerialLetterTemplate();
        $template31->setLanguage($this->getReference('language_english'));
        $template31->setTemplateName('Invitation');
        $content = '<p><strong>Program:</strong></p>
            <table border="0">
            <tbody>
            <tr>
            <td>19:00</td>
            <td style="width: 15cm;">bla</td>
            </tr>
            <tr>
            <td>20:00</td>
            <td>bla</td>
            </tr>
            </tbody>
            </table>
            <p><strong>Registration:</strong></p>
            <p><strong>Please transfer xxx &euro; per person until xxx</strong> on the account on the bottom of this page. As purpose please enter &bdquo;xxx&ldquo;. Guests are welcome.</p>
            <p><strong>Location and parking:</strong></p>
            <p>&nbsp;</p>
            <p><strong>Next activities: </strong></p>
            <table border="0">
            <tbody>
            <tr>
            <td>26/7/2020</td>
            <td style="width: 14cm;">bla</td>
            </tr>
            <tr>
            <td>26/8/2020</td>
            <td>bla</td>
            </tr>
            </tbody>
            </table>';
        $template31->setTemplateContent($content);

        $template32 = new SerialLetterTemplate();
        $template32->setLanguage($this->getReference('language_german'));
        $template32->setTemplateName('Einladung');
        $content = '<p><strong>Programm:</strong></p>
            <table border="0">
            <tbody>
            <tr>
            <td>19:00h</td>
            <td style="width: 15cm;">bla</td>
            </tr>
            <tr>
            <td>20:00h</td>
            <td>bla</td>
            </tr>
            </tbody>
            </table>
            <p><strong>Anmeldung:</strong></p>
            <p><strong>Bitte überweisen Sie xxx &euro; pro Person bis zum xxx</strong> auf das unten auf dieser Seite angegebene Konto. Geben Sie als Verwendungszweck &bdquo;xxx&ldquo; an. Gäste sind willkommen.</p>
            <p><strong>Anfahrt und Parken:</strong></p>
            <p>&nbsp;</p>
            <p><strong>Nächste Veranstaltungen: </strong></p>
            <table border="0">
            <tbody>
            <tr>
            <td>26.7.2020</td>
            <td style="width: 14cm;">bla</td>
            </tr>
            <tr>
            <td>26.8.2020</td>
            <td>bla</td>
            </tr>
            </tbody>
            </table>';
        $template32->setTemplateContent($content);

        $template33 = new SerialLetterTemplate();
        $template33->setLanguage($this->getReference('language_dutch'));
        $template33->setTemplateName('Uitnodiging');
        $content = '<p><strong>Programma:</strong></p>
            <table border="0">
            <tbody>
            <tr>
            <td>19.00u</td>
            <td style="width: 15cm;">bla</td>
            </tr>
            <tr>
            <td>20.00u</td>
            <td>bla</td>
            </tr>
            </tbody>
            </table>
            <p><strong>Aanmelding:</strong></p>
            <p><strong>Maak vóór xxx &euro; per persoon over</strong> over op het rekeningnummer, dat u onder op deze pagina vindt, o.v.v. &bdquo;xxx&ldquo;. Ook introducées zijn welkom.</p>
            <p><strong>Bereikbaarheid en parkeren:</strong></p>
            <p>&nbsp;</p>
            <p><strong>Verdere geplande activiteiten: </strong></p>
            <table border="0">
            <tbody>
            <tr>
            <td>26-7-2020</td>
            <td style="width: 14cm;">bla</td>
            </tr>
            <tr>
            <td>26-8-2020</td>
            <td>bla</td>
            </tr>
            </tbody>
            </table>';
        $template33->setTemplateContent($content);

        $manager->persist($template11);
        $manager->persist($template12);
        $manager->persist($template13);
        $manager->persist($template21);
        $manager->persist($template22);
        $manager->persist($template23);
        $manager->persist($template31);
        $manager->persist($template32);
        $manager->persist($template33);
        $manager->flush();

        $this->addReference('serial_letter_template11', $template11);
        $this->addReference('serial_letter_template12', $template12);
        $this->addReference('serial_letter_template13', $template13);
        $this->addReference('serial_letter_template21', $template21);
        $this->addReference('serial_letter_template22', $template22);
        $this->addReference('serial_letter_template23', $template23);
        $this->addReference('serial_letter_template31', $template31);
        $this->addReference('serial_letter_template32', $template32);
        $this->addReference('serial_letter_template33', $template33);
    }


    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [DisableLogListener::class, LanguageData::class];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default'];
    }
}
