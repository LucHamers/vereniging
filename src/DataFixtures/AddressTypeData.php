<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use App\Entity\AddressType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Fixture data for the AddressType class
 */
class AddressTypeData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    public const ADDRESS_TYPE_PRIVATE = "address_type_private";
    public const ADDRESS_TYPE_COMPANY = "address_type_company";


    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $addressType1 = new AddressType("Home address", false);
        $addressType2 = new AddressType("Company address", true);

        $addressType1->addNewTranslation('en', 'Home address');
        $addressType1->addNewTranslation('nl', 'Thuisadres');
        $addressType1->addNewTranslation('de', 'Privatadresse');
        $addressType2->addNewTranslation('en', 'Company address');
        $addressType2->addNewTranslation('nl', 'Firma-adres');
        $addressType2->addNewTranslation('de', 'Firmenadresse');

        $manager->persist($addressType1);
        $manager->persist($addressType2);
        $manager->flush();

        $this->addReference(self::ADDRESS_TYPE_PRIVATE, $addressType1);
        $this->addReference(self::ADDRESS_TYPE_COMPANY, $addressType2);
    }


    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [DisableLogListener::class];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default'];
    }
}
