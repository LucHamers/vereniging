<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use App\Service\LogEventsDisabler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Disable
 */
class EnableLogListener extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    private LogEventsDisabler $logEventsDisabler;


    /**
     * EnableLogListener constructor.
     *
     * @param LogEventsDisabler $logEventsDisabler
     */
    public function __construct(LogEventsDisabler $logEventsDisabler)
    {
        $this->logEventsDisabler = $logEventsDisabler;
    }


    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $this->logEventsDisabler->enableLifecycleEvents();
    }


    /**
     * @inheritDoc
    *
     * @return array
     */
    public function getDependencies(): array
    {
        return [
            AddressData::class,
            AddressTypeData::class,
            ChangeTypeData::class,
            CommitteeData::class,
            CommitteeFunctionData::class,
            CommitteeFunctionMapData::class,
            CompanyData::class,
            CountryData::class,
            DisableLogListener::class,
            LanguageData::class,
            LogfileData::class,
            MailingListData::class,
            MemberAddressMapData::class,
            MemberEntryData::class,
            MembershipNumberData::class,
            MembershipTypeData::class,
            PhoneNumberData::class,
            PhoneNumberData::class,
            SerialLetterContentData::class,
            SerialLetterData::class,
            StatusData::class,
            TitleData::class,
            UserRoleData::class,
        ];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default'];
    }
}
