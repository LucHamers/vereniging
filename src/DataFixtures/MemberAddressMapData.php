<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use App\Entity\MemberAddressMap;

/**
 * Fixture data for the MemberAddressMap class
 */
class MemberAddressMapData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $memberAddressMap1 = new MemberAddressMap();
        $memberAddressMap1->setAddress($this->getReference("address1"));
        $memberAddressMap1->setMemberEntry($this->getReference("member_entry_last_name1"));
        // The first address for any member should be automatically set to its main address, even when it is set so
        // false here.
        $memberAddressMap1->setIsMainAddress(false);
        $manager->persist($memberAddressMap1);
        $manager->flush();

        $memberAddressMap2 = new MemberAddressMap();
        $memberAddressMap2->setAddress($this->getReference("address2"));
        $memberAddressMap2->setMemberEntry($this->getReference("member_entry_last_name1"));
        $memberAddressMap2->setIsMainAddress(false);

        $memberAddressMap3 = new MemberAddressMap();
        $memberAddressMap3->setAddress($this->getReference("address2"));
        $memberAddressMap3->setMemberEntry($this->getReference("member_entry_last_name11"));
        $memberAddressMap3->setIsMainAddress(false);

        $memberAddressMap4 = new MemberAddressMap();
        $memberAddressMap4->setAddress($this->getReference("address3"));
        $memberAddressMap4->setMemberEntry($this->getReference("member_entry_last_name12"));
        $memberAddressMap4->setIsMainAddress(true);

        $memberAddressMap5 = new MemberAddressMap();
        $memberAddressMap5->setAddress($this->getReference("address4"));
        $memberAddressMap5->setMemberEntry($this->getReference("member_entry_last_name10"));
        $memberAddressMap5->setIsMainAddress(true);

        $memberAddressMap6 = new MemberAddressMap();
        $memberAddressMap6->setAddress($this->getReference("address3"));
        $memberAddressMap6->setMemberEntry($this->getReference("member_entry_last_name9"));
        $memberAddressMap6->setIsMainAddress(true);

        $memberAddressMap7 = new MemberAddressMap();
        $memberAddressMap7->setAddress($this->getReference("address5"));
        $memberAddressMap7->setMemberEntry($this->getReference("member_entry_last_name7"));
        $memberAddressMap7->setIsMainAddress(true);

        $memberAddressMap8 = new MemberAddressMap();
        $memberAddressMap8->setAddress($this->getReference("address6"));
        $memberAddressMap8->setMemberEntry($this->getReference("member_entry_last_name8"));
        $memberAddressMap8->setIsMainAddress(true);

        $manager->persist($memberAddressMap2);
        $manager->persist($memberAddressMap3);
        $manager->persist($memberAddressMap4);
        $manager->persist($memberAddressMap5);
        $manager->persist($memberAddressMap6);
        $manager->persist($memberAddressMap7);
        $manager->persist($memberAddressMap8);
        $manager->flush();
    }


    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [DisableLogListener::class, AddressData::class, MemberEntryData::class];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default'];
    }
}
