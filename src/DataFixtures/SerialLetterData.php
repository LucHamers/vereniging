<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use App\Entity\SerialLetter;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Fixture data for the SerialLetter class
 */
class SerialLetterData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $serialLetter1 = new SerialLetter();
        $serialLetter1->setFirstSignatureMember($this->getReference("member_entry_last_name1"));
        $serialLetter1->setFirstSignatureMembersFunction($this->getReference('committee_function_map_board_chairman'));
        $serialLetter1->setSecondSignatureMember($this->getReference("member_entry_last_name5"));
        $serialLetter1->setSignatureUseTitle(true);
        $serialLetter1->setLastUpdatedBy($this->getReference("member_entry_last_name1"));

        $serialLetter2 = new SerialLetter();
        $serialLetter2->setFirstSignatureMember($this->getReference("member_entry_last_name2"));
        $serialLetter2->setSignatureUseTitle(false);
        $serialLetter2->setLastUpdatedBy($this->getReference("member_entry_last_name2"));

        $serialLetter3 = new SerialLetter();
        $serialLetter3->setFirstSignatureMember($this->getReference("member_entry_last_name1"));
        $serialLetter3->setSignatureUseTitle(false);
        $serialLetter3->setLastUpdatedBy($this->getReference("member_entry_last_name1"));
        $serialLetter3->setSecondSignatureMembersFunction($this->getReference('committee_function_map_board_chairman'));

        $manager->persist($serialLetter1);
        $manager->persist($serialLetter2);
        $manager->persist($serialLetter3);
        $manager->flush();

        $this->addReference('serial_letter1', $serialLetter1);
        $this->addReference('serial_letter2', $serialLetter2);
        $this->addReference('serial_letter3', $serialLetter3);
    }


    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [DisableLogListener::class, MemberEntryData::class, CommitteeFunctionMapData::class];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default'];
    }
}
