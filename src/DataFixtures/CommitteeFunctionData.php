<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use App\Entity\CommitteeFunction;

/**
 * Fixture data for the CommitteeFunction class
 */
class CommitteeFunctionData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $committeeFunction1 = new CommitteeFunction("chairman");
        $committeeFunction2 = new CommitteeFunction("vice chairman");
        $committeeFunction3 = new CommitteeFunction("member");

        $committeeFunction1->translate('en')->setCommitteeFunctionNameTranslated('Chairman');
        $committeeFunction1->translate('nl')->setCommitteeFunctionNameTranslated('Voorzitter');
        $committeeFunction1->translate('de')->setCommitteeFunctionNameTranslated('Vorsitzender');
        $committeeFunction2->translate('en')->setCommitteeFunctionNameTranslated('Vice chairman');
        $committeeFunction2->translate('nl')->setCommitteeFunctionNameTranslated('Vicevoorzitter');
        $committeeFunction2->translate('de')->setCommitteeFunctionNameTranslated('Stellvertretender Vorsitzender');
        $committeeFunction3->translate('en')->setCommitteeFunctionNameTranslated('Member');
        $committeeFunction3->translate('nl')->setCommitteeFunctionNameTranslated('Lid');
        $committeeFunction3->translate('de')->setCommitteeFunctionNameTranslated('Mitglied');

        $manager->persist($committeeFunction1);
        $manager->persist($committeeFunction2);
        $manager->persist($committeeFunction3);
        $manager->flush();

        $this->addReference('committee_function_chairman', $committeeFunction1);
        $this->addReference('committee_function_vice_chairman', $committeeFunction2);
        $this->addReference('committee_function_member', $committeeFunction3);
    }


    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [DisableLogListener::class];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default'];
    }
}
