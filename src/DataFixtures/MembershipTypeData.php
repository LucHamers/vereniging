<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use App\Entity\MembershipType;

/**
 * Fixture data for the MembershipType class
 */
class MembershipTypeData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $membershipType1 = new MembershipType();
        $membershipType1->setType("regular member");
        $membershipType1->setMembershipFee(24.00);
        $membershipType1->setGroupMembership(false);
        $membershipType1->setDefaultGroupMembershipType(false);
        $membershipType1->setDefaultNewMembershipType(true);

        $membershipType2 = new MembershipType();
        $membershipType2->setType("student member");
        $membershipType2->setMembershipFee(12.00);
        $membershipType2->setGroupMembership(false);
        $membershipType2->setDefaultGroupMembershipType(false);
        $membershipType2->setDefaultNewMembershipType(false);

        $membershipType3 = new MembershipType();
        $membershipType3->setType("family member");
        $membershipType3->setMembershipFee(36.00);
        $membershipType3->setGroupMembership(true);
        $membershipType3->setDefaultGroupMembershipType(true);
        $membershipType3->setDefaultNewMembershipType(false);

        $membershipType4 = new MembershipType();
        $membershipType4->setType("group member");
        $membershipType4->setMembershipFee(36.00);
        $membershipType4->setGroupMembership(true);
        $membershipType4->setDefaultGroupMembershipType(false);
        $membershipType4->setDefaultNewMembershipType(false);

        $membershipType1->translate('en')->setTypeTranslated('Regular member');
        $membershipType1->translate('nl')->setTypeTranslated('Regulier lid');
        $membershipType1->translate('de')->setTypeTranslated('Reguläres Mitglied');
        $membershipType2->translate('en')->setTypeTranslated('Student member');
        $membershipType2->translate('nl')->setTypeTranslated('Student lid');
        $membershipType2->translate('de')->setTypeTranslated('Studentisches Mitglied');
        $membershipType3->translate('en')->setTypeTranslated('Family member');
        $membershipType3->translate('nl')->setTypeTranslated('Familielid');
        $membershipType3->translate('de')->setTypeTranslated('Familienmitglied');
        $membershipType4->translate('en')->setTypeTranslated('Group member');
        $membershipType4->translate('nl')->setTypeTranslated('Groepslid');
        $membershipType4->translate('de')->setTypeTranslated('Gruppenmitglied');

        $manager->persist($membershipType1);
        $manager->persist($membershipType2);
        $manager->persist($membershipType3);
        $manager->persist($membershipType4);
        $manager->flush();

        $this->addReference('membership_type_regular', $membershipType1);
        $this->addReference('membership_type_student', $membershipType2);
        $this->addReference('membership_type_family', $membershipType3);
        $this->addReference('membership_type_group', $membershipType4);
    }


    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [DisableLogListener::class];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default'];
    }
}
