<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use App\Service\LogEventsDisabler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Disable
 */
class DisableLogListener extends Fixture implements FixtureGroupInterface
{
    private LogEventsDisabler $logEventsDisabler;


    /**
     * DisableLogListener constructor.
     *
     * @param LogEventsDisabler $logEventsDisabler
     */
    public function __construct(LogEventsDisabler $logEventsDisabler)
    {
        $this->logEventsDisabler = $logEventsDisabler;
    }


    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $this->logEventsDisabler->disableLifecycleEvents();
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default', 'installerFixtures'];
    }
}
