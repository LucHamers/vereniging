<?php

/*
 * Copyright (C) 2022 by Luc Hamers <vereniging@hamers.de>
 *
 * This file is part of Vereniging.
 *
 * Vereniging is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Vereniging is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Vereniging.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use App\Entity\MemberEntry;

/**
 * Fixture data for the MemberEntry class
 */
class MemberEntryData extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $memberEntry1 = $this->generateMember(1);
        $memberEntry1->setTitle($this->getReference("title1"));
        $memberEntry1->setBirthday(new \DateTime("1974-02-07"));
        $memberEntry1->setPreferredLanguage($this->getReference("language_english"));
        $memberEntry1->setUserRole($this->getReference("user_role_role_system_administrator"));
        $memberEntry1->setCompanyInformation($this->getReference('company1'));
        $memberEntry1->setMemberSince(2016);
        $memberEntry1->setPlainPassword("secret1");
        $memberEntry1->setMembershipNumber($this->getReference('membership_number_1'));
        $memberEntry1->setMembershipType($this->getReference("membership_type_family"));

        $memberEntry2 = $this->generateMember(2);
        $memberEntry2->setGender("f");
        $memberEntry2->setEmail(null);
        $memberEntry2->setTitle($this->getReference("title2"));
        $memberEntry2->setPreferredLanguage($this->getReference("language_german"));
        $memberEntry2->setPlainPassword("secret2");
        $memberEntry2->setMembershipNumber($this->getReference('membership_number_2'));

        $memberEntry3 = $this->generateMember(3);
        $memberEntry3->setUserRole($this->getReference("user_role_role_member_administrator"));
        $memberEntry3->setPlainPassword("secret3");
        $memberEntry3->setMembershipNumber($this->getReference('membership_number_3'));
        $memberEntry3->setMembershipEndRequested(true);

        $memberEntry4 = $this->generateMember(4);
        $memberEntry4->setStatus($this->getReference("status_future_member"));
        $memberEntry4->setMemberSince(null);

        $memberEntry5 = $this->generateMember(5);
        $memberEntry5->setMembershipNumber($this->getReference('membership_number_4'));
        $memberEntry5->setMemberSince(null);
        $memberEntry6 = $this->generateMember(6);
        $memberEntry6->setMembershipNumber($this->getReference('membership_number_5'));
        $memberEntry7 = $this->generateMember(7);
        $memberEntry7->setMembershipNumber($this->getReference('membership_number_6'));
        $memberEntry8 = $this->generateMember(8);
        $memberEntry8->setGender("f");
        // This is needed to check ordering in some views
        $memberEntry8->setMembershipNumber($this->getReference('membership_number_20'));
        $memberEntry9 = $this->generateMember(9);
        $memberEntry9->setMembershipNumber($this->getReference('membership_number_8'));
        $memberEntry10 = $this->generateMember(10);
        $memberEntry10->setMembershipNumber($this->getReference('membership_number_1'));
        $memberEntry10->setMembershipType($this->getReference("membership_type_family"));

        // Special members for testing group functions, one with the same last name and one with the same address.
        $memberEntry11 = $this->generateMember(11);
        $memberEntry11->setName('Lastname1');
        $memberEntry11->setMembershipNumber($this->getReference('membership_number_1'));
        $memberEntry11->setCompanyInformation($this->getReference('company2'));
        $memberEntry11->setMembershipType($this->getReference("membership_type_family"));
        $memberEntry12 = $this->generateMember(12);
        $memberEntry12->setMembershipNumber($this->getReference('membership_number_1'));
        $memberEntry12->setMembershipType($this->getReference("membership_type_family"));

        $memberEntry1->addGroupMember($memberEntry11);
        $memberEntry1->addGroupMember($memberEntry10);
        $memberEntry1->addGroupMember($memberEntry12);
        $manager->persist($memberEntry1);
        $manager->persist($memberEntry2);
        $manager->persist($memberEntry3);
        $manager->persist($memberEntry4);
        $manager->persist($memberEntry5);
        $manager->persist($memberEntry6);
        $manager->persist($memberEntry7);
        $manager->persist($memberEntry8);
        $manager->persist($memberEntry9);
        $manager->persist($memberEntry10);
        $manager->persist($memberEntry11);
        $manager->persist($memberEntry12);
        $manager->flush();

        $this->addReference('member_entry_last_name1', $memberEntry1);
        $this->addReference('member_entry_last_name2', $memberEntry2);
        $this->addReference('member_entry_last_name3', $memberEntry3);
        $this->addReference('member_entry_last_name4', $memberEntry4);
        $this->addReference('member_entry_last_name5', $memberEntry5);
        $this->addReference('member_entry_last_name6', $memberEntry6);
        $this->addReference('member_entry_last_name7', $memberEntry7);
        $this->addReference('member_entry_last_name8', $memberEntry8);
        $this->addReference('member_entry_last_name9', $memberEntry9);
        $this->addReference('member_entry_last_name10', $memberEntry10);
        $this->addReference('member_entry_last_name11', $memberEntry11);
        $this->addReference('member_entry_last_name12', $memberEntry12);
    }


    /**
     * @inheritDoc
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [
            DisableLogListener::class,
            TitleData::class,
            LanguageData::class,
            UserRoleData::class,
            CompanyData::class,
            MembershipNumberData::class,
            StatusData::class,
            MembershipTypeData::class,
        ];
    }


    /**
     * @inheritDoc
     *
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['default'];
    }


    /**
     * Create a member entry with basic information set.
     *
     * @param int $index Index of the member which is used for naming.
     *
     * @return MemberEntry
     */
    private function generateMember(int $index): MemberEntry
    {
        $memberEntry = new MemberEntry();
        $memberEntry->setGender("m");
        $memberEntry->setName("Lastname$index");
        $memberEntry->setFirstName("Firstname$index");
        $memberEntry->setMembershipEndRequested(false);
        $memberEntry->setPreferredLanguage($this->getReference("language_dutch"));
        $memberEntry->setStatus($this->getReference("status_member"));
        $memberEntry->setMembershipType($this->getReference("membership_type_regular"));
        $memberEntry->setUserRole($this->getReference("user_role_role_user"));
        $memberEntry->setEmail("firstname$index@email.com");
        $memberEntry->setMemberSince(2015);

        return ($memberEntry);
    }
}
