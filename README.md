Vereniging
==============

Vereniging is a management software for clubs and communities. It is installed on a web server using php, mysql and the
Symfony2 framework. More information on the project can be found on http://vereniging.hamers.de.


# Project installation

## Using git

Download source code and install dependencies:

        mkdir vereniging
        cd vereniging
        git clone https://gitlab.com/LucHamers/vereniging.git .
        COMPOSER_MEMORY_LIMIT=-1 composer install

## Using tar-ball
Unpack the tar.gz file into a directory or download the project via a git clone. Then switch into the project directory
and call

        COMPOSER_MEMORY_LIMIT=-1 composer install

When composer is not yet installed, you find it at getcomposer.org.

Running composer install not only downloads all third party packages, but also asks for needed configuration settings
like the database connection, its credentials and the database name.

## Site settings

There is a special file which contains all the "personalised" settings for your site. Copy the file
.env to .env.local and edit all your settings.

## Set correct rights

The web server needs to be able to write to the directories var/cache and var/logs. Set write rights for the web server
accordingly.

# Deployment

Deploying a vereniging installation is identical to installing it, when you have an ssh console to work with. If not,
there is a deployment script in this directory which supports in deploying remotely via FTP to simple web space
providers. To use this, do a local installation in a separate directory, This is then mirrored to the remote web space
using the deploy.php script. This automates all steps to update the web server to the current version of vereniging. For
this to work, the programs lftp and zip must be installed.

# Create database

To create a database, make sure that your database settings have been set correctly in the file .env.local. Then call 
the following commands to create the database, create all the needed tables and fill it with default data.

        ./bin/console doctrine:database:create
        ./bin/console doctrine:schema:update --force
        ./bin/console doctrine:fixtures:load

# Login

When using the default data, to log in, use the following credentials:

    user: firstname1.lastname1
    password: secret1

# Quality checking

Vereniging uses a lot of the standard php tools to ensure the quality of the software, among others phpunit, phpcs and
phpmd. Running all of these tools is automated by using ant in combination with the jenkins continuous build server. The
tools can also be run by hand by calling

        ant build

This runs all the tools. When using this in Jenkins after every checkin, it might take too long because of the check of
the vendor directory. To speed things up, there is a faster build target which omits this check:

        ant build-fast

# Coding standard

In this project, the Symfony2 coding standard is used. To check this automatically (and to be able to run the build
targets without errors), the Symfony2 CodeSniffer coding standard has to be installed. It can be downloaded from
https://github.com/escapestudios/Symfony2-coding-standard and is copied into the CodeSniffer standards directory
(e.g. /usr/share/php5/PEAR/PHP/CodeSniffer/Standards).


# Tools installation (OpenSuse Leap 42.3)

For quality checking, a lot of tools are used when calling the build command. The following sections explains how the
needed tools are to be installed on an OpenSuse 13.2 installation. These tools are installed globally, so they can also
be used by other projects. Tools can also be installed under the project root using composer, but this is not used here.
All tools can be and should be tested with the --version argument, e.g. composer --version.

## composer (used to install Symfony and other tools)

        curl -sS https://getcomposer.org/installer | php
        chmod +x composer.phar
        mv composer.phar /usr/local/bin/composer

## phploc (tool which calculates several statistics, among others the lines of code)

        wget https://phar.phpunit.de/phploc.phar
        chmod +x phploc.phar
        mv phploc.phar /usr/local/bin/phploc

## pdepend (tool which checks the dependencies of the components)

        wget http://static.pdepend.org/php/latest/pdepend.phar
        chmod +x pdepend.phar
        mv pdepend.phar /usr/local/bin/pdepend

## phpmd (php mess detector, which checks for bad php constructs)

        wget -c http://static.phpmd.org/php/latest/phpmd.phar
        chmod +x phpmd.phar
        mv phpmd.phar /usr/local/bin/phpmd

## phpcs (php code style, which checks the used coding style, here Symfony coding style)

        curl -OL https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar
        chmod +x phpcs.phar
        mv phpcs.phar /usr/local/bin/phpcs
        git clone git://github.com/djoos/Symfony-coding-standard.git
        mkdir -p /usr/share/php7/PEAR/PHP/CodeSniffer/Standards
        cp -r Symfony-coding-standard/Symfony/ /usr/share/php7/PEAR/PHP/CodeSniffer/Standards
        phpcs --config-set installed_paths /usr/share/php7/PEAR/PHP/CodeSniffer/Standards/Symfony/
        rm Symfony-coding-standard/ -r

## phpcpd (php copy detector, which checks for code doubles)

        wget https://phar.phpunit.de/phpcpd.phar
        chmod +x phpcpd.phar
        mv phpcpd.phar /usr/local/bin/phpcpd

## phpcb (php code browser, which generates browsable code for the jenkins CI server)

        wget https://github.com/mayflower/PHP_CodeBrowser/releases/download/1.1.1/phpcb-1.1.1.phar
        chmod +x phpcb-1.1.1.phar
        mv phpcb-1.1.1.phar /usr/local/bin/phpcb

## phpdox (php doxygen, which creates the documentation for all classes and methods)

        wget http://phpdox.de/releases/phpdox.phar
        chmod +x phpdox-0.8.0.phar
        mv phpdox-0.8.0.phar /usr/bin/phpdox

## phpunit (tools which is needed to run unit and integration tests)

        wget https://phar.phpunit.de/phpunit.phar
        chmod +x phpunit.phar
        mv phpunit.phar /usr/bin/phpunit

# Login and security

The software uses a form login system for normal operation. Users can login using a form on the page /. For unit tests, 
http-login is added in the config/test/security.yaml file, because this is a lot easier to handle than form-login. 
http-login uses the same users as the form login.
